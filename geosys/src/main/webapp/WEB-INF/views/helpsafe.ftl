<#escape x as (x)!?html>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "help.guide"></@spring.message></title>
</head>
<body>
<#include "header.ftl" parse=true/>
<div class="container">
<br>
<div class="list-group">
   <a href="#" class="list-group-item active">
      <h4 class="list-group-item-heading">
         <center>采集安全指南</center>
      </h4>
   </a>
   <a href="#" class="list-group-item">
      <h4 class="list-group-item-heading">
         ＊法律法规
      </h4>
      <p class="list-group-item-text">
      地理空间信息是一个国家建设的基础，部分信息或高精度信息属于国家机密，因此志愿者在采集数据时，不要采集相关国家禁止的地理信息，如：指挥机关、军用机场、港口、码头,营区、训练场、试验场,武器弹药、爆炸物品、剧毒物品、危险 等与公共安全相关的设施及标注信息….<br>
      您可以百度查阅我国公开发行的刊物：<strong>《公开地图内容表示的若干规定》</strong>、<strong>《遥感影像公开使用管理规定(试行)国家测绘局2011》</strong>
      </p>
   </a>
   <a href="#" class="list-group-item">
      <h4 class="list-group-item-heading">＊编辑提醒</h4>
      <p class="list-group-item-text">
      不使用高精度的测绘设备或GPS设备<br>
     不随意上传公司持有或测绘的境外数据（境外建筑、水利水电、地质等行业）<br>
    不表现出可以的获取数据意图<br>
    网络传输采用特殊的加密方式，避免明码传输，带来不必要的安全隐患<br>    
      </p>
   </a>
</div>
<div class="geocopyright"><center><@spring.message "geocopyright"></center></@spring.message>
</div>
</body>
</html>
</#escape>
<#escape x as (x)!?html>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "login"></@spring.message> </title>
</head>
<body>
<#include "header.ftl" parse=true/>

<!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong><@spring.message "login.welcome"></@spring.message> </strong></h1>
                            <div class="description">
                            	<p>
	                            	<@spring.message "login.r"></@spring.message>  <a href="register"><strong><@spring.message "register"></@spring.message> </strong></a>, 加入我们的Family!
                            	</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                            		<p><@spring.message "login.tip"></@spring.message> </p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="loginValidate" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="username" placeholder="Username..." value="yzz" class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="password" placeholder="Password..." value="" class="form-password form-control" id="form-password">
			                        </div>
                         
			                        <button type="submit" class="btn"><@spring.message "login"></@spring.message> </button>
			                        <span><font color="red"><center>${msg}</center></font></span>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

  
</form>
</body>
</html>
</#escape>
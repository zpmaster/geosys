<#escape x as (x)!?html>
<!DOCTYPE html>
<html style="height: 100%">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/geo/resources/css/easyui.css">
    <link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">

    <script type="text/javascript" src="/geo/resources/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/geo/resources/js/backinfo.js"></script>
    <script type="text/javascript" src="/geo/resources/js/backhome.js"></script>
    <script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/geo/resources/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/geo/resources/js/fixeasyuidatagrid.js"></script>
    <script type="text/javascript" src="/geo/resources/js/mypaging.js"></script>
    <link href="/geo/resources/css/backhome.css" rel="stylesheet">
<#--<link rel="stylesheet" type="text/css" href="/geo/resources/css/demo.css">-->
<#-- <link rel="stylesheet" type="text/css" href="/geo/resources/css/easyui.css">-->
    <link rel="stylesheet" type="text/css" href="/geo/resources/css/icon.css">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>首页</title>
</head>
<body style="width: 100%;height:100%">
<!-- BEGIN SECTION -->
<section id="container">
    <!-- BEGIN HEADER -->
    <header class="header white-bg" style="height:80px;">
        
        <!-- START HEADER  NAV -->

        <!-- START USER LOGIN DROPDOWN  -->

        <div class="top-nav ">
            <ul class="nav pull-right top-menu">
                 <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <img alt="" src="/geo/resources/images/avatar1_small.jpg">
                        <span class="username">
                  ${adminuser}
                </span>
                        <b class="caret">
                        </b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <li class="log-arrow-up">
                        </li>
                        <li>
                            <a href="back.ftl">
                                </i>
                                退出
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- END USER LOGIN DROPDOWN  -->
    </header>
    <!-- END HEADER -->
    <!-- BEGIN SIDEBAR -->
    <aside>
        <div id="sidebar" class="nav-collapse">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="javascript:;" id="abackhome">
                        <i class="fa fa-dashboard">
                        </i>
                        <span>
                  主&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;页
                </span>
                    </a>
                </li>
                <li class="sub-menu dcjq-parent-li">
                    <a id="backuser" href="javascript:;" class="dcjq-parent">
                        <i class="fa fa-laptop">
                        </i>
                        <span>
                  编辑者管理
                </span>
                        <span class="label label-success span-sidebar">
                  2
                </span>
                        <span class="dcjq-icon"></span>
                    </a>
                    <ul id="usersubul" class="sub" style="overflow: hidden; display: none;">
                        <li>
                            <a id="auserfind" href="javascript:;">
                                用户查询
                            </a>
                        </li>
                        <li>
                            <a id="auseradd" href="javascript:;">
                                用户新建
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a id="backcheck" href="javascript:;">
                        <i class="fa fa-book">
                        </i>
                        <span>
                  审核员管理
                </span>
                        <span class="label label-info span-sidebar">
                  2
                </span>
                        <span class="dcjq-icon"></span>
                    </a>
                    <ul id="checksubul" class="sub">
                    
                        <li>
                            <a id="acheckfind" href="javascript:;">
                                   用户查询
                            </a>
                        </li>
                            <li>
                            <a id="acheckadd" href="javascript:;">
                                   用户新建
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="sub-menu">
                    <a id="backlog" href="javascript:;">
                        <i class="fa fa-cogs">
                        </i>
                        <span>
                  日志管理
                </span>
                        <span class="label label-primary span-sidebar">
                  2
                </span>
                        <span class="dcjq-icon"></span>
                    </a>
                    <ul id="logsubul" class="sub">
                        <li>
                            <a id="alogfind" href="javascript:;">
                                日志查询
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="sub-menu">
                    <a id="backperson" href="javascript:;">
                        <i class="fa fa-tasks">
                        </i>
                        <span>
                  个人中心
                </span>
                        <span class="label label-info span-sidebar">
                  1
                </span>
                        <span class="dcjq-icon"></span>
                    </a>
                    <ul id="personsubul" class="sub">
                        <li>
                            <a id="abackperson" href="javascript:;">
                                个人信息
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            </li>
            </ul>
        </div>
    </aside>
    <!-- END SIDEBAR -->
    <!-- BEGIN MAIN CONTENT -->
    <section id="main-content" style="height:90%;">
        <!-- BEGIN backhome WRAPPER  -->
        <section id="backhomediv" class="wrapper">
            <br>
            <div class="row state-overview">
                <div class="col-lg-3 col-sm-6">
                    <section class="panel">
                        <div class="symbol">
                            <i class="fa fa-tags blue">
                            </i>
                        </div>
                        <div class="value">
                            <h1 class="count" id="xhyc">
                                0
                            </h1>
                            <p>
                                <font color="#5ab6df">审核员</font>
                            </p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <section class="panel">
                        <div class="symbol">
                            <i class="fa fa-money red">
                            </i>
                        </div>
                        <div class="value">
                            <h1 class=" count2" id="bjyc">
                                0
                            </h1>
                            <p>
                                <font color="#5ab6df">编辑员</font>
                            </p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <section class="panel">
                        <div class="symbol">
                            <i class="fa fa-user yellow">
                            </i>
                        </div>
                        <div class="value">
                            <h1 class=" count3" id="bjc">
                                0
                            </h1>
                            <p>
                                <font color="#5ab6df">编辑量</font>
                            </p>
                        </div>
                    </section>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <section class="panel">
                        <div class="symbol">
                            <i class="fa fa-shopping-cart purple">
                            </i>
                        </div>
                        <div class="value">
                            <h1 class=" count4" id="hglc">
                                0%
                            </h1>
                            <p>
                                <font color="#5ab6df">合格率</font>
                            </p>
                        </div>
                    </section>
                </div>
            </div>
            <div>
                <br>
                <div>
                <span style="font-size:14px;float:left;margin-left:40px;width:500px;height:336px;border:1px;"><!--审核员首页-->
                    <div style="margin:0px auto;width:100%;height:100%;border-color: #1c94c4;border: 1px;">
                        <table id='tadminresult' class="easyui-datagrid" title="审核员"
                               style="width:100%;height:100%;border-color: #ee0d77;border: 1px;"
                               data-options="rownumbers:true,singleSelect:true,method:'get',toolbar:'',pagination:true,pageSize:10">
                    <thead>
                    <tr>
                        <th data-options="field:'fname',width:108,align:'center'">审核员</th>
                  <th data-options="field:'femail',width:140,align:'center'">邮箱</th>
				   <th data-options="field:'ftel',width:108,align:'center'">电话</th>
				<th data-options="field:'fstatus',width:108,align:'center'">权限</th>
                    </tr>
                    </thead>
                </table>

                        </div>
	    </span>
                    <span style="font-size:14px;float:left;margin-left:40px;width:500px;height:336px;border:1px;">
          <table id='tagresult' class="easyui-datagrid" title="编辑员"
                 style="width:100%;height:100%;border-color: #ee0d77;border: 1px;"
                 data-options="rownumbers:true,singleSelect:true,method:'get',toolbar:'',pagination:true,pageSize:10">
                    <thead>
                    <tr>
                        <th data-options="field:'fname',width:108,align:'center'">编辑员</th>
                  <th data-options="field:'femail',width:140,align:'center'">邮箱</th>
				   <th data-options="field:'ftel',width:108,align:'center'">电话</th>
				<th data-options="field:'fstatus',width:108,align:'center'">状态</th>
                    </tr>
                    </thead>
                </table>
	    </span>
                </div>
            </div>
        </section>
        <!-- BEGIN userfind WRAPPER  --> <!--编辑员-->

        <section id="userfindiv" class="wrapper"
                 style="position:relative;margin-top:82px;height:93%; auto;padding:0px;width:100%;display: none">
            <div style="margin:0px auto;width:100%;height:100%;">
                <table id='taguerq' class="easyui-datagrid" title=""
                       style="width:100%;height:100%;border-color: #ee0d77;border: 1px;"
                       data-options="rownumbers:true,singleSelect:true,method:'get',toolbar:'#tbtager',pagination:true,pageSize:10">
                    <thead>
                    <tr>
                        <th data-options="field:'fname',width:fixWidth(0.121),align:'center'">编辑员</th>
                        <th data-options="field:'femail',width:fixWidth(0.121),align:'center'">邮箱</th>
                        <th data-options="field:'ftel',width:fixWidth(0.121),align:'center'">电话</th>
                        <th data-options="field:'fac',width:fixWidth(0.121),align:'center'">学历</th>
                        <th data-options="field:'fprof',width:fixWidth(0.121),align:'center'">专业</th>
                        <th data-options="field:'fstatus',width:fixWidth(0.121),align:'center'">状态</th>
                        <th data-options="field:'fedit',width:fixWidth(0.121),align:'center'">启用/停用</th>
                        <th data-options="field:'fdbtn',width:fixWidth(0.121),align:'center'">删除</th>
                    </tr>
                    </thead>
                </table>
            </div>
            <div id="tbtager" style="padding:3px 50px;">
                &nbsp;&nbsp;
                <span>姓名:</span>
                <input id="tagernb" class="easyui-textbox" style="width:100px;height:20px;">

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;启用状态：
                <select id='tagers' class="textbox-icon combo-arrow" panelHeight="auto" style="width:100px;">
                    <option value="0">---请选择---</option>
                    <option value="2">已禁用</option>
                    <option value="1">已启用</option>

                </select>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a id='queryTagerBtn' class="easyui-linkbutton" iconCls="icon-search" style="width:90px;height:20px;">查询</a>
            </div>
        </section>


        <!-- BEGIN checkfind WRAPPER 审核员表格 -->
        <section id="checkfindiv" class="wrapper"
                 style="position:relative;margin-top: 82px;height:93%; auto;padding:0px;width:100%;display: none">

            <div style="margin: 0;width:100%;height:100%;">
                <table id='tagshq' class="easyui-datagrid" title=""
                       style="width:100%;height:100%;border-color: #ee0d77;border: 1px;"
                       data-options="rownumbers:true,singleSelect:true,method:'get',toolbar:'#tbchecker',pagination:true,pageSize:30">
                    <thead>
                    <tr>
                        <th data-options="field:'fname',width:fixWidth(0.107),align:'center'">审核员</th>
                        <th data-options="field:'fprof',width:fixWidth(0.107),align:'center'">职称</th>
                        <th data-options="field:'facm',width:fixWidth(0.107),align:'center'">学历</th>
                        <th data-options="field:'femail',width:fixWidth(0.107),align:'center'">邮箱</th>
                        <th data-options="field:'ftel',width:fixWidth(0.107),align:'center'">电话</th>
                        <th data-options="field:'fstatus',width:fixWidth(0.107),align:'center'">权限</th>
                        <th data-options="field:'feditup',width:fixWidth(0.107),align:'center'">升级</th>
                        <th data-options="field:'feditdown',width:fixWidth(0.107),align:'center'">降级</th>
                        <th data-options="field:'fdbtn',width:fixWidth(0.107),align:'center'">删除</th>
                    </tr>
                    </thead>
                </table>

            </div>


            <div id="tbchecker" style="padding:3px 50px;">
                &nbsp;&nbsp;
                <span>姓名:</span>
                <input id="checknb" class="easyui-textbox" style="width:100px;height:20px;">

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;权限：
                <select id='checkers' class="textbox-icon combo-arrow" panelHeight="auto" style="width:100px;">
                    <option value="0">---请选择---</option>
                    <option value="2">审核员</option>
                    <option value="1">管理员</option>

                </select>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a id='queryCheckerBtn' class="easyui-linkbutton" iconCls="icon-search" style="width:90px;height:20px;">查询</a>
            </div>

        </section>

        <!-- BEGIN logfind WRAPPER  -->
        <!--日志管理-->
        <section id="logfindiv" class="wrapper"
                 style="position:relative;margin-top:82px;height:93%; auto;padding:0px;width:100%;display: none">

            <div style="margin:0px;width:100%;height:100%;">
                <table id='logtable' class="easyui-datagrid" title=""
                       style="width:100%;height:100%;border-color: #ee0d77;border: 1px;"
                       data-options="rownumbers:true,singleSelect:true,method:'get',toolbar:'',pagination:true,pageSize:30">
                    <thead>
                    <tr>
                        <th data-options="field:'fname',width:fixWidth(0.121) ,align:'center'">要素名</th>
                        <th data-options="field:'ftype',width:fixWidth(0.121),align:'center'">要素类型</th>
                        <th data-options="field:'fstatus',width:fixWidth(0.121),align:'center'">审核状态</th>
                        <th data-options="field:'fchecker',width:fixWidth(0.121),align:'center'">审核员</th>
                        <th data-options="field:'faemail',width:fixWidth(0.121),align:'center'">审核员邮箱</th>
                        <th data-options="field:'ftager',width:fixWidth(0.121),align:'center'">编辑者</th>
                        <th data-options="field:'fteamil',width:fixWidth(0.121),align:'center'">编辑者邮箱</th>
                        <th data-options="field:'fdes',width:fixWidth(0.121),align:'center'">描述</th>
                    </tr>
                    </thead>
                </table>
            </div>

            <div id="logq" style="padding:3px 50px;">
                &nbsp;&nbsp;
                <span>要素类型:</span>
            <#-- <input id="logqtype" class="easyui-textbox" style="width:100px;height:26px;">-->
                <select id='gtype' class="textbox-icon combo-arrow" panelHeight="auto" style="width:100px;">
                    <option value="0">---请选择---</option>
                    <option value="1">点要素</option>
                    <option value="2">线要素</option>
                    <option value="3">面要素</option>
                </select>

                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;审核状态：
                <select id='pon' class="textbox-icon combo-arrow" panelHeight="auto" style="width:100px;">
                    <option value="0">---请选择---</option>
                    <option value="2">通过</option>
                    <option value="1">未通过</option>
                </select>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a id='queryCheckerBtn' class="easyui-linkbutton" iconCls="icon-search" style="width:90px;height:20px;">查询</a>
            </div>
        </section>

        <!-- BEGIN logfind WRAPPER  --><!--新建用户-->
       <section id="useradddiv" class="wrapper" style="display:none;">
              <form>
            <div style="margin:0px;width:40%;height:100%;float:left">
                <div class="easyui-panel" title=""
                     style="margin:40px;width:480px;padding:30px 60px;border:0px solid;border-color:#99bce8;">
                    <div style="margin-bottom:20px">
                        <div>用&nbsp;户&nbsp;名:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cusernamet" placeholder="请输入用户名.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>密&nbsp;&nbsp;&nbsp;&nbsp;码:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cnewpt" placeholder="请输入密码.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    
                    <div style="margin-bottom:20px">
                        <div>邮&nbsp;&nbsp;&nbsp;&nbsp;箱:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cemailt" placeholder="请输入邮箱.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>电&nbsp;&nbsp;&nbsp;&nbsp;话:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="ctelt" placeholder="请输入电话.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    
                    <div style="margin-bottom:20px">
                        <div>专&nbsp;&nbsp;&nbsp;&nbsp;业:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cproft" placeholder="请输入专业.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>学&nbsp;&nbsp;&nbsp;&nbsp;历:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cacmt" placeholder="请输入学历.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    
                    <div style="margin-bottom:20px">
                        <div>地&nbsp;&nbsp;&nbsp;&nbsp;址:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cadresst" placeholder="请输入地址.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>

                    <div style="width:200px;">
                        <a class="easyui-linkbutton" id="newtagbtn" iconCls="icon-ok"
                           style="width:80px;height:32px;margin: 20px 60px;">添加</a>
                             <span>
                        <label id="htrslt" style="color: #b92c28;margin-left:10px;"></label></span>
                    </div>
                     
                </div>
            </div>

            <div style="margin-bottom:0px;margin-top:0px;margin-right:200px;width:40%;height:100%;float:right">
                <div class="easyui-panel" title=""
                     style="margin:40px;width:480px;padding:30px 60px;border:0px solid;border-color:#99bce8;">
                    <p style="font-size:18px"><label style="color: orangered;">提&nbsp;&nbsp;&nbsp;&nbsp;示:</label></p>
                    <ul style="font-family: Arial 20px;">
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">当前登录用户为超级管理员；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">超级管理员可对审核用户的权限进行控制；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">具有添加审核用户的权限；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">具有添加下级管理员的权限；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">审核用户有权对编辑者的编辑内容就行审核；</li>
                       
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">审核登录地址:<a style="color: orangered">http://localhost:8081/geo/</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- BEGIN logfind WRAPPER 新增审核员 -->


        <section id="checkadddiv" class="wrapper" style="display:none;">
        
            <div style="margin:0px;width:40%;height:100%;float:left">
                <div class="easyui-panel" title=""
                     style="margin:40px;width:480px;padding:30px 60px;border:0px solid;border-color:#99bce8;">
                    <div style="margin-bottom:20px">
                        <div>用&nbsp;户&nbsp;名:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cusername" placeholder="请输入用户名.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>密&nbsp;&nbsp;&nbsp;&nbsp;码:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cnewp" placeholder="请输入密码.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>权&nbsp;&nbsp;&nbsp;&nbsp;限:</div>
                        <span style="width:200px;border-color:transparent">
                        <select id='newpqx' placeholder="请选择.." panelHeight="auto" style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                               <option value="0"></option>
                               <option value="1">审核员</option>
                               <option value="2">管理员</option>
                </select>
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>邮&nbsp;&nbsp;&nbsp;&nbsp;箱:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cemail" placeholder="请输入邮箱.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>电&nbsp;&nbsp;&nbsp;&nbsp;话:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="ctel" placeholder="请输入电话.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                     <div style="margin-bottom:20px">
                        <div>职&nbsp;&nbsp;&nbsp;&nbsp;称:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cprofa" placeholder="请输入职称.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>学&nbsp;&nbsp;&nbsp;&nbsp;历:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="cacma" placeholder="请输入学历.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>

                    <div style="width:200px;">
                        <a class="easyui-linkbutton" id="savenpbtm" iconCls="icon-ok"
                           style="width:80px;height:32px;margin: 20px 60px;">添加</a>
                           <span>
                        <label id="hcrslt" style="color: #b92c28;margin-left:10px;"></label></span>
                    </div>
                    
                </div>
            </div>

            <div style="margin-bottom:0px;margin-top:0px;margin-right:200px;width:40%;height:100%;float:right">
                <div class="easyui-panel" title=""
                     style="margin:40px;width:480px;padding:30px 60px;border:0px solid;border-color:#99bce8;">
                    <p style="font-size:18px"><label style="color: orangered;">提&nbsp;&nbsp;&nbsp;&nbsp;示:</label></p>
                    <ul style="font-family: Arial 20px;">
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">当前登录用户为超级管理员；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">超级管理员可对审核用户的权限进行控制；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">具有添加审核用户的权限；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">具有添加下级管理员的权限；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">审核用户有权对编辑者的编辑内容就行审核；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">审核登录地址:<a style="color: orangered">http://localhost:8081/geo/back</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <!-- BEGIN logfind WRAPPER  个人中心-->
        <section id="personinfodiv" class="wrapper" style="display:none;">
            <div style="margin:0px;width:40%;height:100%;float:left">
                <div class="easyui-panel" title=""
                     style="margin:40px;width:480px;padding:30px 60px;border:0px solid;border-color:#99bce8;">
                    <div style="margin-bottom:20px">
                        <div>用&nbsp;户&nbsp;名:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="superusername" placeholder="请输入用户名.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>密&nbsp;&nbsp;&nbsp;&nbsp;码:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="supernewp" placeholder="请输入密码.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    
                    <div style="margin-bottom:20px">
                        <div>邮&nbsp;&nbsp;&nbsp;&nbsp;箱:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="superemail" placeholder="请输入邮箱.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>电&nbsp;&nbsp;&nbsp;&nbsp;话:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="supertel" placeholder="请输入电话.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                     <div style="margin-bottom:20px">
                        <div>职&nbsp;&nbsp;&nbsp;&nbsp;称:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="superprofa" placeholder="请输入职称.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>
                    <div style="margin-bottom:20px">
                        <div>学&nbsp;&nbsp;&nbsp;&nbsp;历:</div>
                        <span style="width:200px;border-color:transparent">
                    <input type="text" id="superacma" placeholder="请输入学历.."
                           style="border-color:#99bce8;width:70%;height:32px;border-bottom-width:2px; border-left-width:0px; border-top-width:0px; border-right-width:0px">
                    </span>
                    </div>

                    <div style="width:200px;">
                        <span><a class="easyui-linkbutton" id="savesuperuser" iconCls="icon-ok"
                           style="width:80px;height:32px;margin:5px 0px 5px 30px;">修改</a></span>
                         <span> <label id="hrslt2" style="color: #b92c28;margin-left:10px;">ninn</label></span>
                    </div>
                </div>
            </div>

            <div style="margin-bottom:0px;margin-top:0px;margin-right:200px;width:40%;height:100%;float:right">
                <div class="easyui-panel" title=""
                     style="margin:40px;width:480px;padding:30px 60px;border:0px solid;border-color:#99bce8;">
                    <p style="font-size:18px"><label style="color: orangered;">提&nbsp;&nbsp;&nbsp;&nbsp;示:</label></p>
                    <ul style="font-family: Arial 20px;">
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">当前登录用户为超级管理员；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">超级管理员可对审核用户的权限进行控制；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">具有添加审核用户的权限；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">具有添加下级管理员的权限；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">审核用户有权对编辑者的编辑内容就行审核；</li>
                        <li style="line-height: 40px;font-size: 16px;color: #2da5c8">审核登录地址:<a style="color: orangered">http://localhost:8081/geo/back</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- BEGIN logfind WRAPPER  -->
        <section id="logexportdiv" class="wrapper" style="display:none;">
            <br>
            <br>
            </div>
            </div>
        </section>
    </section>
</section>

</html>
</#escape>

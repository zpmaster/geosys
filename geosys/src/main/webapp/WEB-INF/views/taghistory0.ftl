<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="/geo/resources/openlayers/ol.css" rel="stylesheet">
	 <link rel="stylesheet" href="/geo/resources/css/layout.css" type="text/css">
	  <link rel="stylesheet" href="/geo/resources/css/taghistory.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/openlayers/bootstrap-responsive.min.css" type="text/css">	
    <link rel="stylesheet" href="/geo/resources/css/jquery.dataTables.min.css" type="text/css">	
<script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>
<script type="text/javascript" src="/geo/resources/js/jquery.dataTables.min.js"></script>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>历史列表</title>
</head>
<body style="overflow:hidden;">
 <#include "header.ftl" parse=true/>
 <div id="tagcontent">
 <div id="tagmap" class="taghistorymap">
 </div>
 <div id="taglist" class="taglist">
<table id="example" class="display" >
        <thead class="tagthread">
            <tr class="tagtr">
                <th>名称</th>
                <th>时间</th>
                <th>状态</th>
                <th>查看</th>
                <th>删除</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011/04/25</td>
                
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>63</td>
                <td>2011/07/25</td>
                
            </tr>
            <tr>
                <td>Ashton Cox</td>
                <td>Junior Technical Author</td>
                <td>San Francisco</td>
                <td>66</td>
                <td>2009/01/12</td>
                
            </tr>
            <tr>
                <td>Cedric Kelly</td>
                <td>Senior Javascript Developer</td>
                <td>Edinburgh</td>
                <td>22</td>
                <td>2012/03/29</td>
                
            </tr>
            <tr>
                <td>Airi Satou</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>33</td>
                <td>2008/11/28</td>
                
            </tr>
            
            <tr>
                <td>Hermione Butler</td>
                <td>Regional Director</td>
                <td>London</td>
                <td>47</td>
                <td>2011/03/21</td>
                
            </tr>
            <tr>
                <td>Lael Greer</td>
                <td>Systems Administrator</td>
                <td>London</td>
                <td>21</td>
                <td>2009/02/27</td>
                
            </tr>
            <tr>
                <td>Jonas Alexander</td>
                <td>Developer</td>
                <td>San Francisco</td>
                <td>30</td>
                <td>2010/07/14</td>
                
            </tr>
            <tr>
                <td>Shad Decker</td>
                <td>Regional Director</td>
                <td>Edinburgh</td>
                <td>51</td>
                <td>2008/11/13</td>
                
            </tr>
            <tr>
                <td>Michael Bruce</td>
                <td>Javascript Developer</td>
                <td>Singapore</td>
                <td>29</td>
                <td>2011/06/27</td>
                
            </tr>
            <tr>
                <td>Donna Snider</td>
                <td>Customer Support</td>
                <td>New York</td>
                <td>27</td>
                <td>2011/01/25</td>
                
            </tr>
        </tbody>
    </table>
 </div>
 <div>
<script type="text/javascript" src="/geo/resources/js/taghistory.js"></script>
<div class="geocopyright"><@spring.message "geocopyright"></@spring.message>  					                  
</div>
</body>
</html>
</#escape>

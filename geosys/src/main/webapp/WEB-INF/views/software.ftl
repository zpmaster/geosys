<#escape x as (x)!?html>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "help.guide"></@spring.message></title>
</head>
<body>
<#include "header.ftl" parse=true/>
<div class="container">
<br>
<div class="list-group">
 <a href="#" class="list-group-item active">
      <h4 class="list-group-item-heading">
         <center>APP下载方式</center>
      </h4>
   </a>
   
    <a href="#" class="list-group-item">
      <h4 class="list-group-item-heading"><center>浏览器端</center></h4>
      <a href="#"> <h3><center><font color="#F7464A">点击下载</font></center></h3></>      
   </a>
   <a href="#" class="list-group-item">
   <br>
      <h4 class="list-group-item-heading">
         <center>扫描下载</center>
      </h4>
      <center><img src="/geo/resources/images/biaobao.jpg" class="img-responsive" alt="Cinque Terre" width="400" height="400"></center>
   </a>
</div>

</div>
<div class="geocopyright"><center><@spring.message "geocopyright"></center></@spring.message>
</body>
</html>
</#escape>
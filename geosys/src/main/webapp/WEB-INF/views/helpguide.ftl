<#escape x as (x)!?html>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "help.guide"></@spring.message></title>
</head>
<body>
<#include "header.ftl" parse=true/>
<div class="container">
<br>
<div class="list-group">
   <a href="#" class="list-group-item active">
      <h4 class="list-group-item-heading">
         <center>数据采集</center>
      </h4>
   </a>
   <a href="#" class="list-group-item">
      <h4 class="list-group-item-heading">
         ＊在线编辑
      </h4>
      <p class="list-group-item-text">
         您可以点击导航来中标报菜单，对照网站提供的影像数据，绘制出您熟悉的区域地物信息，您可以根据您的理解将地物抽象为点线面等矢量要素，您依据需求点击网站提供的编辑工具进行绘制。
      </p>
      <center><img src="/geo/resources/images/123.jpg" class="img-responsive" alt="Cinque Terre" width="400" height="300"></center>
   </a>
   <!--<a href="#" class="list-group-item">
      <h4 class="list-group-item-heading">＊轨迹数据</h4>
      <p class="list-group-item-text">
      您可以下载本网站提供的GPS轨迹采集APP，安装到您的智能设备，然后在您出游或者出门时，将软件打开同时开启GPS轨迹记录功能，您可以在软件设置中设置相关参数，建议您将采集时间周期定义在10妙以下。
      </p>
   </a>-->
</div>
<div class="list-group">
   <a href="#" class="list-group-item active">
      <h4 class="list-group-item-heading">
         <center>数据上传</center>
      </h4>
   </a>
   <a href="#" class="list-group-item">
      <h4 class="list-group-item-heading">
         ＊Web浏览器端
      </h4>
      <p class="list-group-item-text">
         您通过在线编辑一个地理要素后，需要上传到服务器，您编辑完成后双击页面，会弹出一个对话框，您可以填写相应的属性信息，您也可以点击上边的添加按钮，自定义要素的属性信息，所有信息添加完成后点击提交，即完成一个地理的要素创建工作。
      </p>
      <center><img src="/geo/resources/images/123.jpg" class="img-responsive" alt="Cinque Terre" width="400" height="300"></center>
   </a>
   <!--<a href="#" class="list-group-item">
      <h4 class="list-group-item-heading">＊APP移动端</h4>
      <p class="list-group-item-text">
      您通过移动APP记录的轨迹数据和多媒体数据，如图片、视频、录音等，可以在有Wi-Fi的情况下上传到服务端，GPS轨迹数据上传之前，您需要将其生成为GPX文件，轨迹上传时您可以给其定义一定的标签，同时附上一段描述信息，使得其他用户能够更好的了解您分享的轨迹信息。
      </p>
   </a>-->
</div>
<div class="list-group">
   <a href="#" class="list-group-item active">
      <h4 class="list-group-item-heading">
        <center> 影像数据识别</center>
      </h4>
   </a>
   <a href="#" class="list-group-item">
      <h4 class="list-group-item-heading">
         ＊影像识别要点
      </h4>
      <p class="list-group-item-text">
      1、形状(Shape)：形状是指地物外部轮廓的形状在影像上的反映。不同类型的地面目标有其特定的形状，因此地物影像的形状是目标识别的重要依据。<br>
      2、大小(Size)：大小是指地物在像片上的尺寸，如长、宽、面积、体积等。地物的大小特征主要取决于影像比例尺。有了影像的比例尺，就能够建立物体和影像的大小联系。<br>
      3、色调(Tone)和色彩(Color)：色调是物体的电磁波特性在图像上的反映，在黑白像片上指黑白深浅程度。地物的形状、大小都要通过色调显示出来，所以色调特征是最基本的解译标志。<br>
      4、水系(River System)：水系标志在地质解译中应用最广泛，这里所讲的水系是水流作用所形成的水流形迹，即地面流水的渠道。它可以是大的江河，也可以是小的沟谷，包括冲沟、主流、支流、湖泊以至海洋等。<br>
      5、纹理(Texture)：很小的物体，在图像上是很难个别地详细表达的,但是一群很小的物体可以给图像上的影像色调造成有规律的重复，即影像的纹理特征。<br>
      6、位置(Location)：是指地物的环境位置以及地物间的空间位置关系在像片中的反映。也称为相关特征。它是重要的间接判读特征。<br>
      </p>
      <center><img src="/geo/resources/images/123.jpg" class="img-responsive" alt="Cinque Terre" width="400" height="300"></center>
   </a>
   <!--<a href="#" class="list-group-item">
      <h4 class="list-group-item-heading">＊影像地物编辑建议</h4>
      <p class="list-group-item-text">
      您可以下载本网站提供的GPS轨迹采集APP，安装到您的智能设备，然后在您出游或者出门时，将软件打开同时开启GPS轨迹记录功能，您可以在软件设置中设置相关参数，建议您将采集时间周期定义在10妙以下。
      </p>
   </a>-->
</div>
<div class="geocopyright"><center><@spring.message "geocopyright"></@spring.message></center></div>
</div>
</body>
</html>
</#escape>
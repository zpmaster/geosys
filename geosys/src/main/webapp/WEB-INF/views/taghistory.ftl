<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/geo/resources/js/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="/geo/resources/openlayers/ol.css" rel="stylesheet">
	 <link rel="stylesheet" href="/geo/resources/css/layout.css" type="text/css">
	  <link rel="stylesheet" href="/geo/resources/css/taghistory.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/openlayers/bootstrap-responsive.min.css" type="text/css">	
    <link rel="stylesheet" href="/geo/resources/css/jquery.dataTables.min.css" type="text/css">
	<!--easyui 布局-->
        <link rel="stylesheet" type="text/css" href="/geo/resources/css/icon.css">
        <link rel="stylesheet" type="text/css" href="/geo/resources/css/easyui.css">
<script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>




<script type="text/javascript">
</script>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>编辑历史</title>
</head>
<body style="overflow:hidden;">
 <#include "header.ftl" parse=true/>
 <div id="tagcontent">
 <div id="hmap" class="taghistorymap">
 </div>
 <div id="jqmui"  style='float:right;padding-right：200px; '>
  <table id='hresult' class="easyui-datagrid" title="编辑结果" style="width:100%;height:100%;padding-right:2px;"
			data-options="rownumbers:true,singleSelect:true,method:'get',toolbar:'#tb',footer:'#ft'">
		<thead>
			<tr>
				<th data-options="field:'fid',width:108,align:'center'">要素ID</th>
				<th data-options="field:'fname',width:108,align:'center'">要素名称</th>
				<th data-options="field:'ftype',width:108,align:'center'">地物类型</th>
				<th data-options="field:'fstatus',width:108,align:'center'">审核状态</th>
				
			</tr>
		</thead>
	</table>
	<div id="tb" style="padding:2px 5px;">
	      &nbsp;&nbsp;
	       <span>类型:</span> 
		<select id='selectFtype' class="textbox-icon combo-arrow"  panelHeight="auto" style="width:100px">
		    <option value="all">---请选择---</option>
			<option value="Point">点要素</option>
			<option value="Line">线要素</option>
			<option value="Polygon">面要素</option>
			
		</select>
		
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;审核状态：
		<select id='PNtype' class="textbox-icon combo-arrow"  panelHeight="auto" style="width:100px;">
		    <option value="0">---请选择---</option>
			<option value="1">通过</option>
			<option value="2">未通过</option>
			<option value="3">未审核</option>
		</select>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a id='queryBtn' class="easyui-linkbutton" iconCls="icon-search" >查询</a>
	</div>
 </div>
 <div>
<script type="text/javascript" src="/geo/resources/js/taghistory.js"></script>
<!--<div class="geocopyright"><@spring.message "geocopyright"></@spring.message>  					                  
</div>-->
</body>
</html>
</#escape>

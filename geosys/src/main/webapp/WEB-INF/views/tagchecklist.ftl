<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/geo/resources/js/jquery.easyui.min.js"></script>
    <script type="text/javascript"
            src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
    <link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/geo/resources/openlayers/ol.css" rel="stylesheet">
    <link rel="stylesheet" href="/geo/resources/css/layout.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/css/taghistory.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/openlayers/bootstrap-responsive.min.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/css/jquery.dataTables.min.css" type="text/css">

    <link rel="stylesheet" type="text/css" href="/geo/resources/css/easyui.css">
    <link rel="stylesheet" type="text/css" href="/geo/resources/css/icon.css">
    <script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>
    <!--<script type="text/javascript" src="/geo/resources/js/jquery.dataTables.min.js"></script>-->
    <script type="text/javascript" src="/geo/resources/openlayers/ol3-layerswitcher.js"></script>

    <script type="text/javascript">
    </script>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>在线检核</title>
</head>
<body style="overflow:hidden;">
    <#include "cheader.ftl" parse=true/>

<div id="hmap" class="taghistorymap" style="width:55%;">
</div>
<div id="rightPanel">
<div id="checkerTaskListPanel">
    <table id='checkerTaskList' class="easyui-datagrid" title="任务审核" style="width:100%;padding-right:2px;"
           data-options="rownumbers:true,singleSelect:true,method:'get',footer:'#ft'">
        <thead>
        <tr>
            <th data-options="field:'ctaskid',width:110,align:'center'">任务编号</th>
            <th data-options="field:'cdldtask',width:140,align:'center'">加载任务</th>
            <th data-options="field:'cpstsk',width:108,align:'center'">通过任务</th>
            <th data-options="field:'cnpastsk',width:108,align:'center'">不通过</th>
            <th data-options="field:'creason',width:110,align:'center'">审核状态</th>
            
        </tr>
        </thead>
    </table>
</div>
<div id="cjqmui" style='float:right;'>
    <table id='auditfeature' class="easyui-datagrid" title="要素审核" style="width:100%;height:100%;padding-right:2px;"
           data-options="rownumbers:true,singleSelect:true,method:'get',footer:'#ft'">
        <thead>
        <tr>
            <th data-options="field:'fname',width:110,align:'center'">要素名称</th>
            <th data-options="field:'ftype',width:110,align:'center'">地物类型</th>
            <th data-options="field:'febtn',width:108,align:'center'">通过</th>
            <th data-options="field:'fdbtn',width:108,align:'center'">不通过</th>
            <th data-options="field:'fstatus',width:140,align:'center'">原因描述</th>
        </tr>
        </thead>
    </table>
</div>
</div>

    <script type="text/javascript" src="/geo/resources/js/ctaghistory.js"></script>
    <!--<div class="geocopyright"><@spring.message "geocopyright"></@spring.message>
</div>-->
</body>
</html>
</#escape>

<#escape x as (x)!?html>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
	<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/js/jquery.validate.js"></script>
	<style type="text/css">
em { font-weight: bold; padding-right: 1em; vertical-align: top; }
em.error {
 padding-left: 16px;
 color:#DC143C;
 
}
</style>

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "nav.questionnaire"></@spring.message> </title>
</head>
<body>
	<#include "header.ftl" parse=true/>
	<!-- Top content -->
	<div class="top-content">

		<div class="inner-bg">
			<div class="container">
				<div class="form-bottom">
					
					<form  id="signupForm"  action="qnaireup" method="post" class="login-form"
					>
					<h2><center><font color="#337ab7"><@spring.message "personal"></@spring.message></font></center></h2>
					<label><@spring.message "personal.age"></@spring.message></label>
					<table class="table table-striped">
					<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="age" id="age1" 
								value="5" > <@spring.message "age.1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="age" id="age2"
								value="4"> <@spring.message "age.2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="age" id="age3"
								value="3"><@spring.message "age.3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="age" id="age4"
								value="2"><@spring.message "age.4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="age" id="age5"
								value="1"><@spring.message "age.5"></@spring.message>
							</label>
						</div>
						</td>
						<tr>
					</table>
					<label><@spring.message "personal.diploma"></@spring.message></label>
					<table class="table table-striped">
					<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="diploma" id="diploma1" 
								value="5" > <@spring.message "diploma.1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="diploma" id="diploma2"
								value="4"> <@spring.message "diploma.2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="diploma" id="diploma3"
								value="3"><@spring.message "diploma.3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="diploma" id="diploma4"
								value="2"><@spring.message "diploma.4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="diploma" id="diploma5"
								value="1"><@spring.message "diploma.5"></@spring.message>
							</label>
						</div>
						</td>
						<tr>
					</table>
					
					<label><@spring.message "personal.gender"></@spring.message></label>
					<table class="table table-striped">
					<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="gender" id="gender1"
								value="1" > <@spring.message "gender.1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="gender" id="gender2"
								value="2"> <@spring.message "gender.2"></@spring.message>
							</label>
						</div>
						</td>		
						<tr>
					</table>
					<label><@spring.message "personal.industry"></@spring.message></label>
					<table class="table table-striped">
					<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="industry" id="industry1"
								value="1" > <@spring.message "industry.1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="industry" id="industry2"
								value="2"> <@spring.message "industry.2"></@spring.message>
							</label>
						</div>
						</td>		
						<tr>
					</table>
					<h2><center><font color="#337ab7"><@spring.message "questionnaire.tip"></@spring.message></font></center></h2>
						<label><@spring.message "V1"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v1" id="v11"
								value="5" > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v1" id="v12"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v1" id="v13"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v1" id="v14"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v1" id="v15"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						<tr>
						</table>
						
						<label><@spring.message "V2"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v2" id="v21"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v2" id="v22"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v2" id="v23"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v2" id="v24"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v2" id="v25"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						<tr>
						</table>
						
						<label><@spring.message "V3"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v3" id="v31"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v3" id="v32"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v3" id="v33"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v3" id="v34"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v3" id="v35"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						
						<label><@spring.message "V4"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v4" id="v41"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v4" id="v42"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v4" id="v43"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v4" id="v44"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v4" id="v45"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
					  </table>
					  
					  
						<label><@spring.message "V5"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v5" id="v51"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v5" id="v52"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v5" id="v53"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v5" id="v54"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v5" id="v55"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						
						<label><@spring.message "V6"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v6" id="v61"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v6" id="v62"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v6" id="v63"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v6" id="v64"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v6" id="v65"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						
						<label><@spring.message "V7"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v7" id="v71"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v7" id="v72"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v7" id="v73"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v7" id="v74"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v7" id="v75"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						
						<label><@spring.message "V8"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v8" id="v81"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v8" id="v82"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v8" id="v83"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v8" id="v84"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v8" id="v85"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						<label><@spring.message "V9"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v9" id="v91"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v9" id="v92"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v9" id="v93"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v9" id="v94"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v9" id="v95"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						
						<label><@spring.message "V10"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v10" id="v101"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v10" id="v102"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v10" id="v103"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v10" id="v104"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v10" id="v105"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						<label><@spring.message "V11"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v11" id="v111"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v11" id="v112"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v11" id="v113"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v11" id="v114"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v11" id="v115"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						
						<label><@spring.message "V12"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v12" id="v121"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v12" id="v122"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v12" id="v123"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v12" id="v124"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v12" id="v125"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td></tr></table>
						<label><@spring.message "V13"></@spring.message></label>
						<table class="table table-striped">
						<tr>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v13" id="v131"
								value="5"  > <@spring.message "q1"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v13" id="v132"
								value="4"> <@spring.message "q2"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v13" id="v133"
								value="3"><@spring.message "q3"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v13" id="v134"
								value="2"><@spring.message "q4"></@spring.message>
							</label>
						</div>
						</td>
						<td>
						<div class="radio">
							<label> <input type="radio" name="v13" id="v135"
								value="1"><@spring.message "q5"></@spring.message>
							</label>
						</div>
						</td>
						</tr>
						</table>
						<button type="submit" class="btn"><@spring.message "submit"></@spring.message></button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
$.validator.setDefaults({
    submitHandler: function() {
      form.submit();
    }
});
		$().ready( function () {
			$( "#signupForm" ).validate( {
				rules: {
					age: "required",
					gender: "required",
					diploma: "required",
					industry:"required",
					v1:"required",
					v2:"required",
					v3:"required",
					v4:"required",
					v5:"required",
					v6:"required",
					v7:"required",
					v8:"required",
					v9:"required",
					v10:"required",
					v11:"required",
					v12:"required",
					v13:"required"
					
				},
				messages: {
					age: "必选项",
					gender: "必选项",
					diploma: "必选项",
					industry:"必选项",
					v1:"必选项",
					v2:"必选项",
					v3:"必选项",
					v4:"必选项",
					v5:"必选项",
					v6:"必选项",
					v7:"必选项",
					v8:"必选项",
					v9:"必选项",
					v10:"必选项",
					v11:"必选项",
					v12:"必选项",
					v13:"必选项"
				},
				errorElement: "em",
				errorPlacement: function ( error, element ) {
					// Add the `help-block` class to the error element
					error.addClass( "help-block" );

					if ( element.prop( "type" ) === "checkbox" ) {
						error.insertAfter( element.parent( "label" ) );
					} else {
						error.insertAfter( element.parent( "label"));
					}
				}
			} );
		} );
		</script>
</body>

</html>
</#escape>
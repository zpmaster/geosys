<#escape x as (x)!?html>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "gpsup"></@spring.message></title>
</head>
<body>
	<#include "header.ftl" parse=true/>

	<!-- Top content -->
	<div class="top-content">

            <div class="inner-bg">
                <div class="container">
		<div class="form-bottom">
			<form role="form" action="gpssave" method="post"enctype="multipart/form-data"
				class="login-form">
				<div class="form-group">
					<label ><@spring.message "up.tag"></@spring.message></label> <input
						type="text" name="tag" placeholder="Tag..."
						class="form-username form-control" id="form-username">
				</div>
				<div class="form-group">
					<label ><@spring.message "up.describe"></@spring.message></label> <input
						type="text" name="describe" placeholder="详细说明..."
						class="form-password form-control" id="form-password">
				</div>
				<div class="form-group">
					<label ><@spring.message "up.file"></@spring.message></label> <input
						type="file" name="path"
						>
				</div>
				<button type="submit" class="btn"><@spring.message "submit"></@spring.message></button>
			</form>
		</div>
	</div>
	</div>
	<div class="geocopyright"><center><@spring.message "geocopyright"></center></@spring.message>
	</div>
</body>
</html>
</#escape>
<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript"
	src="/geo/resources/bootstrap/js/Chart.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="/geo/resources/openlayers/ol.css" rel="stylesheet">
	 <link rel="stylesheet" href="/geo/resources/css/layout.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/openlayers/bootstrap-responsive.min.css" type="text/css">	
<script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Home</title>
</head>
<body style="overflow:hidden;">
 <#include "header.ftl" parse=true/>
 <div class="container">
 <h2 style="text-align: center">您的地理认知主导因子出结果啦，快快点击下图查看<h2/>
 <br>
<div id="canvas-holder" style="text-align: center">
			<canvas id="chart-area" width="300" height="300"/>
		</div>
	<script>
		var pieData = ${pieData};
			window.onload = function(){
				var ctx = document.getElementById("chart-area").getContext("2d");
				window.myPie = new Chart(ctx).Pie(pieData);
			};
	</script>	
	<h3 style="text-align: center">结果解析<h3/>	
	<h4 style="text-align: center"><font color="#A8B3C5">生活主导型：日常的生活行为主导您去认识周边地理环境，如平时上下班等</font><h4/>
	<h4 style="text-align: center"><font color="#A8B3C5">娱乐主导型：生活中旅游、购物、景点等因子主导您认识周边地理空间信息</font><h4/>
	<h4 style="text-align: center"><font color="#A8B3C5">知识主导型：地理知识的渴望和专业、行背景在您认知地理要素其主导作用</font><h4/>				                  
</div>

<div class="geocopyright"><@spring.message "geocopyright"></@spring.message>  
</div>
</body>
</html>
</#escape>

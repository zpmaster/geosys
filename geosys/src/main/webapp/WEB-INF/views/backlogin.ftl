<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="/geo/resources/css/layout.css" rel="stylesheet">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>后台登陆</title>
</head>
<body class="login-screen">
    <!-- BEGIN SECTION -->
    <div class="container">
      <form class="form-signin" action="backlogin" method="post">
        <h2 class="form-signin-heading">
          卫星测图信息在线检核系统
        </h2>
		<!-- LOGIN WRAPPER  -->
        <div class="login-wrap">
          
          <input type="text" class="form-control" name="username" value="yzz"placeholder="用户名" autofocus>
          <input type="password" class="form-control"  name="password"  value="yzz" placeholder="密码">
          <br>
          <button class="btn btn-lg btn-login btn-block" type="submit">
          
            登&nbsp;&nbsp;&nbsp;&nbsp;陆
          </button>
          <span><font color="red" ><center>${msg}</center></font></span>
        </div>
      </form>
    </div>
  </body>

</html>
</#escape>
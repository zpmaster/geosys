<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/geo/resources/js/jquery.easyui.min.js"></script>
    <script type="text/javascript"
            src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
    <link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/geo/resources/jqueryui/css/base/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/geo/resources/openlayers/ol.css" rel="stylesheet">
    <link rel="stylesheet" href="/geo/resources/css/layout.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/css/taghistory.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/openlayers/bootstrap-responsive.min.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/css/jquery.dataTables.min.css" type="text/css">

    <link rel="stylesheet" type="text/css" href="/geo/resources/css/easyui.css">
    <link rel="stylesheet" type="text/css" href="/geo/resources/css/icon.css">
    <script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>
    <!--<script type="text/javascript" src="/geo/resources/js/jquery.dataTables.min.js"></script>-->
    <script type="text/javascript" src="/geo/resources/openlayers/ol3-layerswitcher.js"></script>
   <script type="text/javascript" src="/geo/resources/jqueryui/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript">
    </script>
     <style type="text/css">
        #dialog {
            display: none;
        }
        </style>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>审核任务分配</title>
</head>
<body style="overflow:hidden;">
    <#include "aheader.ftl" parse=true/>
<div id="tagcontent">
    <div id="adminpanel" style="width:80px;height: 300px;position: absolute;
       left: 10px; top:250px;z-index: 120;">
        <div style='padding-top:4px;margin:3px;z-index:12;'>
            <button class="btn btn-default" type="submit" style='' onClick="adminLoaddata()">加载数据</button>
        </div>

        <div style='padding-top:4px;margin:3px;'>
            <button class="btn btn-default" type="submit" onClick="adminTaskLoadData()">加载任务</button>
        </div>
         <div style='padding-top:4px;margin:3px;'>
            <button class="btn btn-default" type="submit" onClick="adminAllocateTask()">分配任务</button>
        </div>
    </div>

    
    <div id="dialog" title="审核任务分配&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; ">
        <form role="form" class="login-form">
            <br>
            <div class="container-fluid">
                <div class="input-group">
                    <span class="input-group-addon">审核员</span>
                    <select id='editors' name="editors" class="form-control" >
                    </select>
                </div>
            </div>

            <hr>
            <div class="container-fluid" style="text-align:center;">
                <button type="button" id="taskup" class="btn"><font color="#337ab7">提交</font></button>
            </div>
        </form>
    </div>

    <div id="admintaskmap" class="taghistorymap" style="width:100%;">
    </div>

    <div>


        <script type="text/javascript" src="/geo/resources/js/admintaskallocate.js"></script>
</body>
</html>
</#escape>

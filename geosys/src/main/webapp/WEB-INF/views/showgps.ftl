<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="/geo/resources/openlayers/ol.css" rel="stylesheet">
	 <link rel="stylesheet" href="/geo/resources/css/layout.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/openlayers/bootstrap-responsive.min.css" type="text/css">	
<script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "showgps"></@spring.message> </title>
</head>
<body style="overflow:hidden;">
 <#include "header.ftl" parse=true/>
<div id="map1" class="map">
</div>
<script type="text/javascript">
var raster = new ol.layer.Tile({
  source: new ol.source.BingMaps({
    imagerySet: 'Aerial',
    key: 'AkGbxXx6tDWf1swIhPJyoAVp06H0s0gDTYslNWWHZ6RoPqMpB9ld5FY1WutX8UoF'
  })
});
var style = {
        'Point': new ol.style.Style({
          image: new ol.style.Circle({
            fill: new ol.style.Fill({
              color: 'rgba(255,255,0,0.4)'
            }),
            radius: 5,
            stroke: new ol.style.Stroke({
              color: '#ff0',
              width: 1
            })
          })
        }),
        'LineString': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: '#f00',
            width: 3
          })
        }),
        'MultiLineString': new ol.style.Style({
          stroke: new ol.style.Stroke({
            color: '#0f0',
            width: 3
          })
        })
      };

      var vector = new ol.layer.Vector({
        source: new ol.source.Vector({
          url: 'http://localhost:8082/geo/resources/images/123.gpx',
          format: new ol.format.GPX()
        }),
        style: function(feature) {
          return style[feature.getGeometry().getType()];
        }
      });

      var map = new ol.Map({
        layers: [raster, vector],
        target: 'map1',
        view: new ol.View({
          center: [-7916041.528716288, 5228379.045749711],
          zoom: 12
        })
      });

     
</script>
<div class="geocopyright"><@spring.message "geocopyright"></@spring.message>  					                  
</div>
</body>
</html>
</#escape>

<#escape x as (x)!?html>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
    src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
    src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css"
    rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "gpslist"></@spring.message></title>
</head>
<body>
    <#include "header.ftl" parse=true/>
    <div class="top-content">
    <div class="inner-bg">
                <div class="container">
    <@gpsList count=4; gps>
    <div class="form-bottom">
    <br>
    <#list gps as info>
    <label ><@spring.message "gpslist.username"></@spring.message><strong>${info.userName!}</strong></label><br>
    <label ><@spring.message "gpslist.tag"></@spring.message><strong>${info.tag!}</strong></label><br>
    <label ><@spring.message "gpslist.describe"></@spring.message><strong>${info.describe!}</strong></label><br>
    <label ><@spring.message "gpslist.createtime"></@spring.message> <strong>${info.createTime!}</strong></label><br>
    
    <a href="showgps?gpsID=${info.id!}"><@spring.message "gpslist.show"></@spring.message></a><br></br> 
    </#list>
    </div>
    </@gpsList>
    </div>
    </div>
    </div>
</body>
</html>
</#escape>
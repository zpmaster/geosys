<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
    <script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/geo/resources/js/jquery.easyui.min.js"></script>
    <script type="text/javascript"
            src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
    <link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="/geo/resources/jqueryui/css/base/jquery-ui-1.9.2.custom.css" rel="stylesheet">
    <link href="/geo/resources/openlayers/ol.css" rel="stylesheet">
    <link rel="stylesheet" href="/geo/resources/css/layout.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/openlayers/bootstrap-responsive.min.css" type="text/css">
    <link rel="stylesheet" href="/geo/resources/css/ol3-layerswitcher.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="/geo/resources/css/icon.css">
    <link rel="stylesheet" type="text/css" href="/geo/resources/css/easyui.css">
    <script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>
    <script type="text/javascript" src="/geo/resources/jqueryui/js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="/geo/resources/js/query.js"></script>
    <script type="text/javascript" src="/geo/resources/js/mytools.js"></script>
    <script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>
    <script type="text/javascript" src="/geo/resources/openlayers/ol3-layerswitcher.js"></script>
    
    <style type="text/css">
        #dialog {
            display: none;
        }

        #menu {
            position: absolute;
            top: 70px;
            right: 10px;

            padding: 10px;
            color: #cccccc;
            z-index: 5555;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home</title>
</head>
<body style="overflow:hidden;">
    <#include "header.ftl" parse=true/>
<div id="menu" style='postiton:realative;top:25%;left:1%;width:100px;height:370px;z-index:10'>
    <div class="container-fluid" style='z-index:11'>
       
        <div style='padding-top:4px;margin:3px;z-index:12;'>
            <button class="btn btn-default" type="submit" style='' onClick="addDrawPointInteraction()">绘点</button>
        </div>
        <div style='padding-top:4px;margin:3px;'>
            <button class="btn btn-default" type="submit" onClick="addDrawLineInteraction()">绘线</button>
        </div>
        <div style='padding-top:4px;margin:3px;'>
            <button class="btn btn-default" type="submit" onClick="addDrawPolygonInteraction()">绘面</button>
        </div>
        <div style='padding-top:4px;margin:3px;'>
            <button id='desbtn' class="btn btn-default" type="submit" onClick="addDescribtion()">属性</button>
        </div>
        <div style='padding-top:4px;margin:3px;position:relative;margin-top:0px;'>
            <a class="btn btn-default" data-toggle="dropdown">
                修改
            </a>
            <ul class="dropdown-menu" style="min-width:60px;height:73px;text-align:center;left:50px">
                <li style="width:60px;text-decoration:underline;font-size:inherit;color:#333;cursor:pointer;type="
                    submit
                " onClick="addModifyPoint()">点</li>
                <li style="width:60px;text-decoration:underline;font-size:inherit;color:#333;cursor:pointer;type="
                    submit
                " onClick="addModifyLine()">线</li>
                <li style="width:60px;text-decoration:underline;font-size:inherit;color:#333;cursor:pointer;type="
                    submit
                " onClick="addModifyPgon()">面</li>
            </ul>
        </div>
        
        <!-- <div style='padding-top:3px;margin:3px;'>
         <button id='deletebtn' class="btn btn-default" type="submit" >删除</button>
         </div>-->
        <!-- <div style='padding-top:3px;margin:3px;'>
         <button class="btn btn-default" type="submit" onClick="pan()">移动</button>
         </div>-->
        <!--<div style='padding-top:3px;margin:3px;'>
        <button class="btn btn-default" type="submit" onClick="save()">保存</button>
        </div>-->
    </div>

</div>
<div id="edittaskslist" style="display:block;width: 205px;height:200px;position: absolute;top:250px;right: 10px;z-index:2000;background-color: #ffffff">

        <table  id="taskslist"  class="easyui-datagrid" title="编辑任务" style="width: 100%;height:198px;"
                data-options="singleSelect:false,collapsible:true, fitColumns:false">
            <thead>
            <tr>
                <th data-options="field:'taskid',width:60,align:'center'">任务</th>
                <th data-options="field:'startbtn',width:70,align:'center'">开始</th>
                <th data-options="field:'endbtn',width:60,align:'center'">完成</th>
            </tr>
            </thead>
        </table>
</div>
<div id="doubledittaskslist" style="display:block;width: 205px;height:200px;position: absolute;top:450px;right: 10px;z-index:2000;background-color: #ffffff">
    <table  id="doubletaskslist"  class="easyui-datagrid" title="审核未通过" style="width: 100%;height:198px;"
            data-options="singleSelect:false,collapsible:true, fitColumns:false">
        <thead>
        <tr>
            <th data-options="field:'doubletaskid',width:60,align:'center'">任务</th>
            <th data-options="field:'doublestartbtn',width:70,align:'center'">开始</th>
            <th data-options="field:'doublendbtn',width:60,align:'center'">完成</th>
        </tr>
        </thead>
    </table>
</div>
<div id="map" class="map">
</div>

<div id="dialog" title="属性提交&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; ">
    <form role="form" class="login-form">
        <input type="hidden" id="tagshp" name="shp" value="">
        <br>
        <div class="container-fluid">
            <div class="input-group">

                <span class="input-group-addon">类&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型:</span>
                <select id='oldtype' name="oldType" class="form-control">
                    <option>---请选择---</option>
                    <option value="公交枢纽站">公交枢纽站</option>
                    <option value="自行车停放点">自行车停放点</option>
                    <option value="停车场">停车场</option>
                    <option value="建筑物">建筑物</option>
                    <option value="水体">水体</option>
                </select>
            </div>
        </div>
        <hr>

        <div class="container-fluid">
            <div class="input-group">
                <span class="input-group-addon">地物名称:</span>
                <input type="text" id='fname' class="form-control" name="name" placeholder="例如测绘大厦">
            </div>
        </div>
        <hr>
        <div class="container-fluid">
            <div class="input-group">
                <span class="input-group-addon">描&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;述:</span>
                <input type="text" id='fdes' class="form-control" name="describe" placeholder="无">
            </div>
        </div>
        <hr>
        <div class="container-fluid" style="text-align:center;">
            <button type="button" id="dpup" class="btn"><font color="#337ab7">确定</font></button>
        </div>
    </form>
</div>
<script type="text/javascript" src="/geo/resources/js/tag.js"></script>
<div class="geocopyright"><@spring.message "geocopyright"></@spring.message>
</div>

</body>
</html>
</#escape>

<#escape x as (x)!?html>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "gpsupsucess"></@spring.message></title>
</head>
<body>
	<#include "header.ftl" parse=true/>

	<!-- Top content -->
	<div class="top-content">

            <div class="inner-bg">
                <div class="container">
                <h1><@spring.message "gpsupsucessinfo"></@spring.message><a href="gpsup"><strong><@spring.message "gpsupagain"></@spring.message> </strong></a></h1>
	</div>
	</div>
	</div>
</body>
</html>
</#escape>
<#escape x as (x)!?html>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>  
<link href="/geo/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="/geo/resources/openlayers/ol.css" rel="stylesheet">
<link rel="stylesheet" href="/geo/resources/css/layout.css" type="text/css">
<link rel="stylesheet" href="/geo/resources/css/ol3-layerswitcher.css" type="text/css">
<link rel="stylesheet" href="/geo/resources/openlayers/bootstrap-responsive.min.css" type="text/css">	
<script type="text/javascript" src="/geo/resources/openlayers/ol.js"></script>
<script type="text/javascript" src="/geo/resources/openlayers/ol3-layerswitcher.js"></script>

<!--<script type="text/javascript" src="/geo/resources/js/googel/source/GoogleMapSource.js"></script>
<script type="text/javascript" src="/geo/resources/js/googel/layer/GoogleMapLayer.js"></script>-->


<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>Home</title>
</head>
<body style="overflow:hidden;">
 <#include "aheader.ftl" parse=true/>
<div id="map" class="map"> </div>
    

<script type="text/javascript" src="/geo/resources/js/home.js"></script>
<div class="geocopyright"><@spring.message "geocopyright"></@spring.message>  					                  
</div>
</body>
</html>
</#escape>

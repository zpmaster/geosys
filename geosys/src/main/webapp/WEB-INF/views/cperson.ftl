<#escape x as (x)!?html>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript"
	src="/geo/resources/bootstrap/js/echarts.simple.min.js"></script>
	<script type="text/javascript"
	src="/geo/resources/js/cperson.js"></script>
	<script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/geo/resources/js/jquery.easyui.min.js"></script>
	
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">
	
	<link href="/geo/resources/css/person.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "task"></@spring.message></title>
</head>
<body>
<#include "cheader.ftl" parse=true/>
<div class="container">
<div class="perleft">
<div class="span3">
<ul id="main-nav" class="nav nav-tabs nav-stacked">

							<li >
								<a id="ataginfo" href="javascript:void(0)"  class="pera">
								 检核统计
								</a>
								
							</li>

							<li>
								<a id="apersoninfo" href="javascript:void(0)" class="pera">
									<i class="icon-pushpin"></i> 个人信息
								</a>
								
							</li>

							<li>
								<a class="pera">
									<i class="icon-th-list" href="home"></i> 退出中心
								</a>
							</li>
						</ul>
</div>
</div><!--perleft end -->
<div id="taginfo" class="perright">
<div clss="rightdiv">
<div style="width:870px">
<h1 class="page-title">检&nbsp;&nbsp;&nbsp;核&nbsp;&nbsp;&nbsp;统&nbsp;&nbsp;&nbsp;计</h1>
</div>
	<div class="stat-container">
	
							<div class="stat-holder">
								<div class="stat">
									<span id="totalc">0</span>检核总数
								</div>
								<!-- /stat -->
							</div>
							<!-- /stat-holder -->

							<div class="stat-holder">
								<div class="stat">
									<span id="passc">0</span> 点要素
								</div>
								<!-- /stat -->
							</div>
							<!-- /stat-holder -->

							<div class="stat-holder">
								<div class="stat">
									<span id="unpassc">0</span> 线要素
								</div>
								<!-- /stat -->
							</div>
							<!-- /stat-holder -->

							<div class="stat-holder">
								<div class="stat">
									<span id="uncheckc">0</span>面要素
								</div>
								<!-- /stat -->
							</div>
							<!-- /stat-holder -->

						</div>
						<!-- /stat-container -->					
</div>
       <div id="main" style="width: 800px;height:400px;"></div>
    
    
</div>
<div id="personinfo" class="perright">
<div clss="rightdiv">
<div style="width:870px">
<h1 class="page-title">个&nbsp;&nbsp;&nbsp;&nbsp;人&nbsp;&nbsp;&nbsp;&nbsp;信&nbsp;&nbsp;&nbsp;&nbsp;息</h1>
</div>

<div id="why" style="display:inline;float:left">

	   <span style="font-size:14px;float:left;width:400px;height:180px;border:1px;">
	   <p style="font-size:18px"><label>提&nbsp;&nbsp;&nbsp;&nbsp;示:</label></p>
		<ul>
			<li>您是审核员用户</li>
			<li>具有要素编辑进行审核的权限</li>
			<li>当您审核的数据量足够多的时候会升级成管理员</li>
			<li>双击数据要素行能自动缩放至要素</li>
			<li>可单击表格的审核按钮审核要素</li>
			<li>如果您对要素较为确认为错误，可以单击“不通过”</li>
		</ul>
		
		
		</span>
		<span style="font-size:14px;float:right;width:400px;height:180px;border:1px;">
		<p style="font-size:18px"><label>信&nbsp;&nbsp;&nbsp;&nbsp;息:</label></p>
		<ul>
			<li><label>用&nbsp;&nbsp;户&nbsp;&nbsp;名:</label>
			    <label id="username">yzz</label></li>
			    <li><label>邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱:</label>
			    <label id="em">yzz</label></li>
			    <li><label>电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话:</label>
			    <label id="tel">yzz</label></li>	
			    <li><label>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历:</label>
			    <label id="zc">高级工程师</label></li>	
			    <li><label>职&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;称:</label>
			    <label id="prof">高级工程师</label></li>	
			    <li><label>权&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;限:</label>
			    <label id="auth">审核员</label></li>	
		</ul>	
		<button id="xgmm" class="" style="margin-left:180px; ">修改</button>		
		</span>
</div>


<div style="clear:both;"></div>
<hr>
<div style="margin:auto 0;display:none" id="xgxx">
<div id="p" class="easyui-panel" title="<label>信息修改</label>" style="width:700px;height:200px;padding:10px;">
         <span style="margin:0 auto ">
		<p style="font-size:12px"><label>均为非必填项，不填写则不更新，请将要修改的信息录入</lable></p>
		<ul style="margin-left:30%">
		
		<li style="list-style-type:none;"><label>邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱:</label>
			    <input type="text" id="emu" value="" style="heighr:20px;"/></li>
        <li style="list-style-type:none;"><label>电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话:</label>
			    <input type="text" value="" id="telu"/></li>
	    <li style="list-style-type:none;"><label>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历:</label>
			    <input type="text" id="xlu" value="" style="heighr:20px;"/></li>
        <li style="list-style-type:none;"><label>职&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;称:</label>
			    <input type="text" value="" id="zcu"/></li>
        <li style="list-style-type:none;"><label>新&nbsp;&nbsp;密&nbsp;&nbsp;&nbsp;码:</label>
			    <input type="password" value="" id="pass1u"/></li>
        <li style="list-style-type:none;"><label>确&nbsp;认&nbsp;密码:</label>
			    <input type="password" value="" id="pass2u"/>
			    <button id="xgmmupload" class="" style="margin-left:10px; ">保存</button>
			    <label id="crslt" style="color: #b92c28;margin-left:10px;"></label>	
			    </li>
			    
		</ul>
			
		</span>
</div>
</div>
</div>
	<script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));

        // 指定图表的配置项和数据
        var option = {
            title: {
                text: '标报统计时序图'
            },
            tooltip: {},
            legend: {
                data:['数量']
            },
            xAxis: {
                data: ["7月","8月","9月","10月","11月","12月"]
            },
            yAxis: {},
            series: [{
                name: '数量',
                type: 'bar',
                data: [5, 20, 36, 10, 10, 20]
            }]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>	
</body>
</html>
</#escape>
<#escape x as (x)!?html>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "register"></@spring.message></title>
</head>
<body>
<#include "header.ftl" parse=true/>
 <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong><@spring.message "register.welcome"></@spring.message></strong></h1>
                            <div class="description">
                            	<p>
	                            	<@spring.message "register.tip1"></@spring.message> <a href=""><strong><@spring.message "login"></@spring.message></strong></a><@spring.message "register.tip2"></@spring.message>
                            	</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			
                            		<p><@spring.message "register.tip3"></@spring.message></p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form  action="savetaguser" method="post" class="login-form">
			                   
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>			                    		
			                        	<input  type="text"  name="username"  placeholder="用户名..."class="form-username form-control"  >
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Emial</label>
			                        	<input type="text" name="email" placeholder="邮箱..." class="form-username form-control" >
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Tel</label>
			                        	<input type="text" name="tel" placeholder="电话..." class="form-username form-control" >
			                        </div>
			                         <div class="form-group">
			                    		<label class="sr-only" for="form-username">Password</label>
			                        	<input type="text" name="address" placeholder="地址..." class="form-username form-control" >
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Confirm Password</label>
			                        	<input type="text" name="diploma" placeholder="学历..." class="form-password form-control" >
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Confirm Password</label>
			                        	<input type="text" name="professional" placeholder="专业..." class="form-password form-control" >
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Password</label>
			                        	<input type="password" name="password" placeholder="密码..." class="form-username form-control" >
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Confirm Password</label>
			                        	<input type="password" name="cpassword" placeholder="确认密码..." class="form-password form-control" >
			                        </div>
			                        <button type="submit" class="btn"><@spring.message "register"></@spring.message></button>
			                        <span><font color="red"><center>${msg}</center></font></span>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
</body>
</html>
</#escape>
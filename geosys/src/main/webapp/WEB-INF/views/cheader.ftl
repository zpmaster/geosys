<nav class="navbar navbar-default" role="navigation" >
   <div class="navbar-header">
      <a class="navbar-brand" href="#"><@spring.message "sysname"></@spring.message></a>
   </div>
  <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left">
          <li><a href="home" class="smoothScroll"><@spring.message "nav.home"></@spring.message></a></li>
          <li> <a href="biaobao" class="smoothScroll"><@spring.message "nav.tag"></@spring.message> </a></li>
          
           <!--<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <@spring.message "nav.gps"></@spring.message>  
               <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
               <li><a href="gpsup"><@spring.message "gps.up"></@spring.message></a></li>
               <li><a href="gpslist"><@spring.message "gps.list"></@spring.message></a></li>
            </ul>
         </li>-->
       
            <li> <a href="/geo/tagchecklist" class="smoothScroll"><@spring.message "nav.checktagdata"></@spring.message></a></li>
           
         <!-- <li> <a href="qnaire" class="smoothScroll"><@spring.message "nav.questionnaire"></@spring.message></a></li> -->
          <!--<li class="dropdown">
           <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <@spring.message "nav.help"></@spring.message>
               <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
               <li><a href="guide"><@spring.message "help.guide"></@spring.message> </a></li>
               <li><a href="safe"><@spring.message "help.safe"></@spring.message></a></li>
               <li><a href="contact"><@spring.message "help.contact"></@spring.message> </a></li>
               <!--<li><a href="software"><@spring.message "help.sys"></@spring.message></a></li>
            </ul>
         </li>-->
        </ul>
        <form class="navbar-form navbar-left" role="search">
         <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
         </div>
         <button type="submit" class="btn btn-default"><@spring.message "search"></@spring.message></button>
      </form>    
       <ul class="nav navbar-nav navbar-right">
         <li>
         <#if adminuser??> 
             <li class="dropdown">
              <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="/geo/resources/images/avatar1_small.jpg">
                <span class="username">
                 ${adminuser}
                </span>
                <b class="caret">
                </b>
              </a>
              <ul class="dropdown-menu extended logout">
                <li class="log-arrow-up">
                </li>
                <li>
                  <a href="/geo/cperson">
                     个人中心
                  </a>
                </li>
                <li>
                  <a href="/geo/logout">
                    退出系统
                  </a>
                </li>
              </ul>
            </li>
         <#else> 
         <a href="/geo/login"><@spring.message "login"></@spring.message></a>
         </#if>
         </li>
        </ul>																																			
      </div>
</nav>

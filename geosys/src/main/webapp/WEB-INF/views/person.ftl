<#escape x as (x)!?html>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="/geo/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="/geo/resources/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript"
	src="/geo/resources/bootstrap/js/echarts.simple.min.js"></script>
	<script type="text/javascript"
	src="/geo/resources/js/person.js"></script>
	<script type="text/javascript" src="/geo/resources/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/geo/resources/js/jquery.easyui.min.js"></script>
	
<link href="/geo/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">
	
	<link href="/geo/resources/css/person.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><@spring.message "task"></@spring.message></title>
</head>
<body>
<#include "header.ftl" parse=true/>
<div class="container">
<div class="perleft">
<div class="span3">
<ul id="main-nav" class="nav nav-tabs nav-stacked">

							<li >
								<a id="ataginfo" href="javascript:void(0)"  class="pera">
								 标报统计
								</a>
								
							</li>

							<li>
								<a id="apersoninfo" href="javascript:void(0)" class="pera">
									<i class="icon-pushpin"></i> 个人信息
								</a>
								
							</li>

							<li>
								<a class="pera" href="/geo/biaobao">
									<i class="icon-th-list"></i> 退出中心
								</a>
							</li>
						</ul>
</div>
</div><!--perleft end -->
<div id="taginfo" class="perright">
<div clss="rightdiv">
<div style="width:870px">
<h1 class="page-title">标&nbsp;&nbsp;&nbsp;报&nbsp;&nbsp;&nbsp;统&nbsp;&nbsp;&nbsp;计</h1>
</div>
	<div class="stat-container">
	
							<div class="stat-holder">
								<div class="stat">
									<span id="totalc">0</span> 标报总数
								</div>
								<!-- /stat -->
							</div>
							<!-- /stat-holder -->

							<div class="stat-holder">
								<div class="stat">
									<span id="passc">0</span> 审核通过
								</div>
								<!-- /stat -->
							</div>
							<!-- /stat-holder -->

							<div class="stat-holder">
								<div class="stat">
									<span id="unpassc">0</span> 审核未通过
								</div>
								<!-- /stat -->
							</div>
							<!-- /stat-holder -->

							<div class="stat-holder">
								<div class="stat">
									<span id="uncheckc">0</span> 未审核
								</div>
								<!-- /stat -->
							</div>
							<!-- /stat-holder -->

						</div>
						<!-- /stat-container -->					
</div>
       <div id="main" style="width: 800px;height:400px;"></div>
    
    
</div>
<div id="personinfo" class="perright">
<div clss="rightdiv">
<div style="width:870px">
<h1 class="page-title">个&nbsp;&nbsp;&nbsp;&nbsp;人&nbsp;&nbsp;&nbsp;&nbsp;信&nbsp;&nbsp;&nbsp;&nbsp;息</h1>
</div>

<div id="why" style="display:inline;float:left">

	   <span style="font-size:14px;float:left;width:400px;height:180px;border:1px;">
	   <p style="font-size:18px"><label>提&nbsp;&nbsp;&nbsp;&nbsp;示:</label></p>
		<ul>
			<li>您为编辑员用户</li>
			<li您有权对编辑页面加载的数据进行编辑</li>
			<li>您编辑的数据会有审核员进行审核</li>
			<li>请您对数据编辑前，先查看帮助的编辑注意事项</li>
			<li>编辑有疑问也可以联系tagtest@163.com</li>
			<li>感谢您的使用</li>
		</ul>
		
		
		</span>
		<span style="font-size:14px;float:right;width:400px;height:180px;border:1px;">
		<p style="font-size:18px"><label>信&nbsp;&nbsp;&nbsp;&nbsp;息:</label></p>
		<ul>
			<li><label>用&nbsp;&nbsp;户&nbsp;&nbsp;名:</label>
			    <label id="username">yzz</label></li>
			    <li><label>邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱:</label>
			    <label id="em">yzz</label></li>
			    <li><label>电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话:</label>
			    <label id="tel">yzz</label></li>	
			     <li><label>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历:</label>
			    <label id="cxl">本科</label></li>	
			    <li><label>专&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业:</label>
			    <label id="czy">GIS</label></li>	
			    <li><label>地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;址:</label>
			    <label id="cdz">北京</label></li>	
		</ul>	
		<button id="xgmm" class="" style="margin-left:180px; ">修改</button>		
		</span>
		
</div>


<div style="clear:both;"></div>
<hr>
<div style="margin:auto 0;display:none" id="xgxx">
<div id="p" class="easyui-panel" title="<label>信息修改</label>" style="width:700px;height:200px;padding:10px;">
         <span style="margin:0 auto ">
		<p style="font-size:12px"><label>均为非必填项，不填写则不更新，请将要修改的信息录入</lable></p>
		<ul style="margin-left:30%">
		
		<li style="list-style-type:none;"><label>邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱:</label>
			    <input type="text" id="emu" value="" style="heighr:20px;"/></li>
        <li style="list-style-type:none;"><label>电&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;话:</label>
			    <input type="text" value="" id="telu"/></li>
		 <li style="list-style-type:none;"><label>学&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;历:</label>
			    <input type="text" value="" id="xlu"/></li>
		 <li style="list-style-type:none;"><label>地&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;址:</label>
			    <input type="text" value="" id="dzu"/></li>  
			    <li style="list-style-type:none;"><label>专&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业:</label>
			    <input type="text" value="" id="zyu"/></li>    
        <li style="list-style-type:none;"><label>新&nbsp;&nbsp;密&nbsp;&nbsp;&nbsp;码:</label>
			    <input type="password" value="" id="pass1u"/></li>
        <li style="list-style-type:none;"><label>确&nbsp;认&nbsp;密码:</label>
			    <input type="password" value="" id="pass2u"/>
			    <button id="xgmmupload" class="" style="margin-left:20px; ">保存</button>
			    <label id="rslt" style="color: #b92c28;margin-left:10px;"></label>
		</li>
		</ul>

		</span>
</div>

</div>

</div>
	<script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));

        // 指定图表的配置项和数据
        var option = {
            title: {
                text: '标报统计时序图'
            },
            tooltip: {},
            legend: {
                data:['数量']
            },
            xAxis: {
                data: ["7月","8月","9月","10月","11月","12月"]
            },
            yAxis: {},
            series: [{
                name: '数量',
                type: 'bar',
                data: [5, 20, 36, 10, 10, 20]
            }]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>	
</body>
</html>
</#escape>
$(document).ready(function() {
	inittaginfo();

	$("#xgmm").click(function() {
		$("#xgxx").css("display", "block");
	});
	$("#xgmmupload").click(function() {
		savepinfo();
	});

	$("#apersoninfo").click(function() {
		$("#personinfo").css("display", "block");
		$("#taginfo").css("display", "none");
		initperson();
	});

	$("#ataginfo").click(function() {
		$("#personinfo").css("display", "none");
		$("#taginfo").css("display", "block");
	});

});


function inittaginfo() {
	$.ajax({
		type : "GET",
		url : "../geo/getAdminUserCheckCount",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {

			if (!data) {
				alert("无数据！");
				return;
			}
			$("#totalc").text(data.sum);
			$("#passc").text(data.Point);
			$("#unpassc").text(data.Line);
			$("#uncheckc").text(data.Polygon);
		}

	});
}


function initperson() {
	$.ajax({
		type : "GET",
		url : "../geo/getAdminUserInfo",
		dataType : "json",
	error : function(req, status, error) {
		if (status == "timeout") {
			alert("请求超时，请稍后再试！");
			return;
		} else if (status === "error") {
			alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
			return;
		}
		return;
	},
		
		success : function(data) {			
			if (!data) {
				alert("无数据！");
				return;
			}
			$("#username").text(data.userName);
			$("#em").text(data.email);
			$("#tel").text(data.tel);
			$("#zc").text(data.academic);
			$("#prof").text(data.professional);
			if(data.authority===1){
				$("#auth").text("管理员");
			}
			if(data.authority===2){
				$("#auth").text("审核员");
			}	
			
			$("#emu").val(data.email);
			$("#telu").val(data.tel);
			$("#xlu").val(data.academic);
			$("#zcu").val(data.professional);
		}

	});
}

function savepinfo() {
	var p1 = $("#pass1u").val();
	var p2 = $("#pass2u").val();
	if (p1 === '' || p2 === "") {
		$("#crslt").text("密码不能为空!");
		return;
	}
	if (p1!== p2) {
		$("#crslt").text("输入密码两次不同！");
		return;
	}
	var pinfo = {
		academic:$("#xlu").val(),
	   professional:$("#zcu").val(),
		username : $("#username").val(),
		email : $("#emu").val(),
		tel : $("#telu").val(),
		password : $("#pass2u").val()
	};
	pinfo = JSON.stringify(pinfo);
	$.ajax({
		type : "GET",
		url : "../geo/updateAdminUserInfo",
		data : {
			data : pinfo
		},
		dataType : "json",
		processData : true,
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success:function(obj){
			if(obj.msg==="0"){
				$("#crslt").text("修改成功!");
			}
			if(obj.msg==="1"){
				$("#crslt").text("修改失败!");
			}
		}
	});
}

/**
 * 
 */
$(document).ready(function() {
	backinittaginfo();
	backinitbjtable();
	shtable();
	
	$("#queryTagerBtn").click(function () {
		tagerCQ();
	});
	$("#queryCheckerBtn").click(function () {
		CheckerCQ();
	});
	$("#savenpbtm").click(function () {
		snp();
	});
	$("#newtagbtn").click(function () {
		sntp();
	});
	$("#savesuperuser").click(function () {
		savesuper();
	});




	
});
/**
 * 主页统计
 */
function backinittaginfo() {
	$.ajax({
		type : "GET",
		url : "../geo/geBackStatistics",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据！");
				return;
			}
			$("#xhyc").text(data.admin);
			$("#bjyc").text(data.taguser);
			$("#bjc").text(data.sum);
			$("#hglc").text(data.quality+"%");
		}

	});
}
/**
 * 首页ta
 * 编辑者
 */
function backinitbjtable() {
	var rows=[];
	$.ajax({
		type : "GET",
		url : "../geo/getAllTagUser",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据！");
				return;
			}
			$.each(data,function(i,n){
				var fid=n.id;
				var fname=n.userName;
				var ftype=n.email;
				var fstatus=n.activeStatus+"";
				var ftel=n.tel;
				if(fstatus==="2"){
					fstatus="禁用"
				}
				if(fstatus==="1"){
					fstatus="启用"
				}

				rows.push({
					fid:fid,
					fname:fname,
					femail:ftype,
					fstatus:fstatus,
					ftel:ftel
				});
			});
			 $("#tagresult").datagrid({data:rows, checkOnSelect: false}).datagrid({loadFilter:pagerFilter});
		}

	});
}
/**
 * 首页
 * 审核员
 */
function shtable() {
	var rows=[];
	$.ajax({
		type : "GET",
		url : "../geo/getAllAdminUser",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据！");
				return;
			}
			$.each(data,function(i,n){
				var fname=n.userName;
				var ftype=n.email;
				var fstatus=n.authority+"";
				var ftel=n.tel;
				if(fstatus==="2"){
					fstatus="审核员"
				}
				if(fstatus==="1"){
					fstatus="管理员"
				}

				rows.push({
					fname:fname,
					femail:ftype,
					fstatus:fstatus,
					ftel:ftel
				});
			});
			$("#tadminresult").datagrid({data:rows, checkOnSelect: false}).datagrid({loadFilter:pagerFilter});
		}

	});
}

/**
 * 编辑者查询所有
 */
function tagerqtable() {
	var rows=[];
	$.ajax({
		type : "GET",
		url : "../geo/getAllTagUser",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据！");
				return;
			}
			$.each(data,function(i,n){
				var fid=n.id;
				var fname=n.userName;
				var ftype=n.email;
				var fstatus=n.activeStatus+"";
				var ftel=n.tel;
				var fac=n.diploma;
				var fprof=n.professional;
				var ebtn="<button id=" + fid + " title='编辑' style='background:url(/geo/resources/images/pencil.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				var dbtn="<button id=" + fid +"d" + " title='删除' style='background:url(/geo/resources/images/cancel.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				if(fstatus==="2"){
					fstatus="已禁用"
				}
				if(fstatus==="1"){
					fstatus="已启用"
				}
				rows.push({
					fprof:fprof,
					fac:fac,
					fid:fid,
					fname:fname,
					femail:ftype,
					fstatus:fstatus,
					ftel:ftel,
					fedit:ebtn,
					fdbtn:dbtn
				});

			});
			$("#taguerq").datagrid({data:rows, checkOnSelect: false}).datagrid({loadFilter:pagerFilter});

			$("#taguerq").datagrid({
				onClickCell: function (index, field, value) {
					var row = $('#taguerq').datagrid('getData').rows[index];
					var i = value.indexOf("id");
					var i2 = value.indexOf("title");
					var layer = value.substring(i + 3, i2 - 1);
					var layer = value.substring(i + 3, i2 - 1);
					var sl = "#" + layer;
					var rowsta=row.fstatus;
					var personid=row.fid+"";
					var status;

					if($(sl).attr("title")==="编辑"){
						if(rowsta==="已启用"){
							row.fstatus="已禁用";
							$('#taguerq').datagrid('refreshRow', index);
							status=2 + "";
							changStatus(personid,status);
						}
						if(rowsta==="已禁用"){
							row.fstatus="已启用";
							$('#taguerq').datagrid('refreshRow', index);
							status=1+"";
							changStatus(personid ,status);
						}
					}
					if($(sl).attr("title")==="删除"){
						$('#taguerq').datagrid('deleteRow', index);
						deletepersion(personid);
					}
				}
			});
		}

	});
}

/**
 * 编辑者条件
 */
function tagerCQ() {
	var rows = [];
	var seleteType = $("#tagernb").val();
	var PNtype = $("#tagers").val();
	if (seleteType === "") {
		seleteType = "all";
	}
	if (PNtype === "请选择") {
		PNtype = "0";
	}
	var param={
		userName:seleteType,
		activeStatus:PNtype,
	};
	param=JSON.stringify(param);
	$.ajax({
		type : "GET",
		url : "../geo/backGetTagUser",
		data:{data:param},
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据！");
				return;
			}
			$.each(data,function(i,n){
				var fid=n.id;
				var fname=n.userName;
				var ftype=n.email;
				var fstatus=n.activeStatus+"";
				var ftel=n.tel;
				var ebtn="<button id=" + fid + " title='编辑' style='background:url(/geo/resources/images/pencil.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				var dbtn="<button id=" + fid +"d" + " title='删除' style='background:url(/geo/resources/images/cancel.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				if(fstatus==="2"){
					fstatus="已禁用"
				}
				if(fstatus==="1"){
					fstatus="已启用"
				}
				rows.push({
					fid:fid,
					fname:fname,
					femail:ftype,
					fstatus:fstatus,
					ftel:ftel,
					fedit:ebtn,
					fdbtn:dbtn
				});

			});
			$("#taguerq").datagrid({data:rows, checkOnSelect: false}).datagrid({loadFilter:pagerFilter});

			$("#taguerq").datagrid({
				onClickCell: function (index, field, value) {
					var row = $('#taguerq').datagrid('getData').rows[index];
					var i = value.indexOf("id");
					var i2 = value.indexOf("title");
					var layer = value.substring(i + 3, i2 - 1);
					var layer = value.substring(i + 3, i2 - 1);
					var sl = "#" + layer;
					var rowsta=row.fstatus;
					var personid=row.fid+"";
					var status;

					if($(sl).attr("title")==="编辑"){
						if(rowsta==="已启用"){
							row.fstatus="已禁用";
							$('#taguerq').datagrid('refreshRow', index);
							status=2 + "";
							changStatus(personid,status);
						}
						if(rowsta==="已禁用"){
							row.fstatus="已启用";
							$('#taguerq').datagrid('refreshRow', index);
							status=1+"";
							changStatus(personid ,status);
						}
					}
					if($(sl).attr("title")==="删除"){
						$('#taguerq').datagrid('deleteRow', index);
						deletepersion(personid);
					}
				}
			});
		}

	});
}

/**
 * 审核者
 * 所有
 */
function checkqtable(){
	var rows=[];
	$.ajax({
		type : "GET",
		url : "../geo/getAllAdminUser",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据！");
				return;
			}
			$.each(data,function(i,n){

				var fid=n.id;
				var fname=n.userName;
				var ftype=n.email;
				var fstatus=n.authority+"";
				var ftel=n.tel;
				var facm=n.academic;
				var fprof=n.professional;
				var feditup="<button id=" + fid + "up title='升级' style='background:url(/geo/resources/images/pencil.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				var feditdown="<button id=" + fid + "down title='降级' style='background:url(/geo/resources/images/pencil.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				var dbtn="<button id=" + fid +"d" + " title='删除' style='background:url(/geo/resources/images/cancel.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				if(fstatus==="2"){
					fstatus="审核员"
				}
				if(fstatus==="1"){
					fstatus="管理员"
				}
				rows.push({
					fid:fid,
				    fprof:fprof,
					fname:fname,
					femail:ftype,
					fstatus:fstatus,
			     	facm:facm,
					ftel:ftel,
					feditup:feditup,
					feditdown:feditdown,
					fdbtn:dbtn
				});

			});
			$("#tagshq").datagrid({data:rows, checkOnSelect: false}).datagrid({loadFilter:pagerFilter});

			$("#tagshq").datagrid({
				onClickCell: function (index, field, value) {
					var row = $('#tagshq').datagrid('getData').rows[index];
					var i = value.indexOf("id");
					var i2 = value.indexOf("title");
					var layer = value.substring(i + 3, i2 - 1);
					var layer = value.substring(i + 3, i2 - 1);
					var sl = "#" + layer;
					var rowsta=row.fstatus;
					var personid=row.fid+"";
					var status2;

					if($(sl).attr("title")==="升级"){
						if(rowsta==="审核员"){
							row.fstatus="管理员";
							status2=1+"";
							$('#tagshq').datagrid('refreshRow', index);
							CheckpersionStatus(personid ,status2);
						}
					}
					if($(sl).attr("title")==="降级"){
						if(rowsta==="管理员"){
							row.fstatus="审核员";
							status2=2+"";
							$('#tagshq').datagrid('refreshRow', index);
							CheckpersionStatus(personid ,status2);
						}
					}
					if($(sl).attr("title")==="删除"){
						$('#tagshq').datagrid('deleteRow', index);
						deleteCheckpersion(personid);
					}
				}
			});
		}

	});
}

/**
 * 审核员查询
 * 条件
 * @constructor
 */
function CheckerCQ() {
	var rows = [];
	var seleteType = $("#checknb").val();
	var PNtype = $("#checkers").val();
	if (seleteType === "") {
		seleteType = "all";
	}
	if (PNtype === "请选择") {
		PNtype = "0";
	}
	var param={
		userName:seleteType,
		authority:PNtype,
	};
	param=JSON.stringify(param);
	$.ajax({
		type : "GET",
		url : "../geo/backGetAdminUser",
		data:{data:param},
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据！");
				return;
			}
			$.each(data,function(i,n){

				var fid=n.id;
				var fname=n.userName;
				var ftype=n.email;
				var fstatus=n.authority+"";
				var ftel=n.tel;
				var facm=n.academic;
				var fprof=n.professional;
				var feditup="<button id=" + fid + "up title='升级' style='background:url(/geo/resources/images/pencil.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				var feditdown="<button id=" + fid + "down title='降级' style='background:url(/geo/resources/images/pencil.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				var dbtn="<button id=" + fid +"d" + " title='删除' style='background:url(/geo/resources/images/cancel.png);background-repeat:no-repeat;height:20px;width:20px'" + "></button>";
				if(fstatus==="2"){
					fstatus="审核员"
				}
				if(fstatus==="1"){
					fstatus="管理员"
				}
				rows.push({
					fid:fid,
				    fprof:fprof,
					fname:fname,
					femail:ftype,
					fstatus:fstatus,
			     	facm:facm,
					ftel:ftel,
					feditup:feditup,
					feditdown:feditdown,
					fdbtn:dbtn
				});

			});
			$("#tagshq").datagrid({data:rows, checkOnSelect: false}).datagrid({loadFilter:pagerFilter});
			
			$("#tagshq").datagrid({
				onClickCell: function (index, field, value) {
					var row = $('#tagshq').datagrid('getData').rows[index];
					var i = value.indexOf("id");
					var i2 = value.indexOf("title");
					var layer = value.substring(i + 3, i2 - 1);
					var layer = value.substring(i + 3, i2 - 1);
					var sl = "#" + layer;
					var rowsta=row.fstatus;
					var personid=row.fid+"";
					var status2;

					if($(sl).attr("title")==="升级"){
						if(rowsta==="审核员"){
							row.fstatus="管理员";
							status2=1+"";
							$('#tagshq').datagrid('refreshRow', index);
							CheckpersionStatus(personid ,status2);
						}
					}
					if($(sl).attr("title")==="降级"){
						if(rowsta==="管理员"){
							row.fstatus="审核员";
							status2=2+"";
							$('#tagshq').datagrid('refreshRow', index);
							CheckpersionStatus(personid ,status2);
						}
					}
					if($(sl).attr("title")==="删除"){
						$('#tagshq').datagrid('deleteRow', index);
						deleteCheckpersion(personid);
					}
				}
			});

		}

	});
}

/**
 * 编辑员状态修改
 */
function changStatus(personid,status){
	var data={
		id:personid,
		status:status
	};
	data=JSON.stringify(data);
	$.ajax({
		type: "POST",
		url: "../geo/backUpdateTagUserActiveStatus",
		contentType: "application/x-www-form-urlencoded",
		data:{data:data},
		dataType: "json",
		processData:true,
		error: function (req, status, error) {

			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success: function () {
			alert("保存成功！");
		}
	});
}

/**
 * 编辑员删除
 */
function deletepersion ( personid ){
	var data={
		id:personid
	};
	data=JSON.stringify(data);
	$.ajax({
		type: "POST",
		url: "../geo/backDeleteTagUser",
		contentType: "application/x-www-form-urlencoded",
		data:{data:data},
		dataType: "json",
		processData:true,
		error: function (req, status, error) {

			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success: function () {
			alert("保存成功！");
		}
	});
}

/**
 * 审核员升级、降级
 */
function CheckpersionStatus ( personid ,status2){
	var data={
		id:personid,
		authority:status2
	};
	data=JSON.stringify(data);
	$.ajax({
		type: "POST",
		url: "../geo/backUpdateAdminUserActiveStatus",
		contentType: "application/x-www-form-urlencoded",
		data:{data:data},
		dataType: "json",
		processData:true,
		error: function (req, status, error) {

			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success: function () {
			alert("保存成功！");
		}
	});
}

/**
 * 审核员删除
 */
function deleteCheckpersion ( personid ){
	var data={
		id:personid
	};
	data=JSON.stringify(data);
	$.ajax({
		type: "POST",
		url: "../geo/backDeleteAdminUser",
		contentType: "application/x-www-form-urlencoded",
		data:{data:data},
		dataType: "json",
		processData:true,
		error: function (req, status, error) {

			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success: function () {
			alert("保存成功！");
		}
	});
}
/**
 * 日志
 */

function logqtable(){
	var rows=[];
	$.ajax({
		type : "GET",
		url : "../geo/getLogs",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据");
				return;
			}
			$.each(data,function(i,n){
				var fid=n.id;
				var fname=n.fname;
				var ftype=n.type;
				var fstatus=n.checkStatus+"";
				var fdes=n.des;
				var fchecker=n.adminName;
				var fadmin=n.adminName;
				var faemial=n.adminEmail;
				var tagUserName=n.tagUserName;
				var ftemail=n.tagUserEmail;

				if(fstatus==="1"){
					fstatus="通过"
				}
				if(fstatus==="2"){
					fstatus="未通过"
				}
				rows.push({
					fid:fid,
					fname:fname,
					ftype:ftype,
					fstatus:fstatus,
					fdes:fdes,
					fchecker:fchecker,
				    faemail:faemial,
				    ftager:tagUserName,
				    fteamil:ftemail
				});

			});
			$("#logtable").datagrid({data:rows , checkOnSelect: false}).datagrid({loadFilter:pagerFilter});
		}
	});
}
/**
 * 个人中心初始化
 */
function inityourself() {
	$.ajax({
		type : "GET",
		url : "../geo/getAdminUserInfo",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {
			if (!data) {
				alert("无数据！");
				return;
			}
			$("#superusername").val(data.userName);
			$("#supernewp").val(data.password);
			$("#superemail").val(data.email);
			$("#supertel").val(data.tel);
			$("#superprofa").val(data.professional);
			$("#superacma").val(data.academic);
		}
	});
}
/**
 * 新建管理员或审核员
 */
function snp() {
	var  authority=$("#newpqx").val();
	var  newptel=$("#ctel").val();
	var  newpemil=$("#cemail").val();
	var  newuname=$("#cusername").val();
	var  newp=$("#cnewp").val();
	var  newacm=$("#cacma").val();
	var  newprof=$("#cprofa").val();
	//var  cnewpc=$("#cnewpc").val();

	if (newp === '') {
		$("#hcrslt").text("密码不能为空！");
		return;
	}
	/*if (newp!== cnewpc) {
		alert("两次输入密码不一致！");
		return;
	}*/
	var data={
		academic:newacm,
	professional:newprof,
		userName:newuname,
		email:newpemil,
		authority:authority,
		passWord:newp,
		tel:newptel
	};
	data=JSON.stringify(data);
	$.ajax({
		type: "POST",
		url: "../geo/backsaveadminuser",
		contentType: "application/x-www-form-urlencoded",
		data:{data:data},
		dataType: "json",
		processData:true,
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(obj) {
			if (!obj) {
				return;
			}
			if(obj.msg==="0"){
				$("#hcrslt").text("新建成功！");
			}
			if(obj.msg==="1"){
				$("#hcrslt").text("新建失败！");
			}
			
			}
	});
}

/**
 * 新建tagerperson
 */
function sntp() {

	var  newptel=$("#ctelt").val();
	var  newpemil=$("#cemailt").val();
	var  newuname=$("#cusernamet").val();
	var  newp=$("#cnewpt").val();
	var  newpacm=$("#cacmt").val();
	var  newproft=$("#cproft").val();
	var  newadret=$("#cadresst").val();
	//var  cnewpc=$("#cnewpct").val();

	if (newp === '') {
		$("#htrslt").text("密码不能为空！");
		return;
	}
//	if (newp!== cnewpc) {
//		alert("两次输入密码不一致！");
//		return;
//	}
	var data={
		address:newadret,
	    diploma:newpacm,
	professional:newproft,
		username:newuname,
		email:newpemil,
		password:newp,
		tel:newptel
	};
	data=JSON.stringify(data);
	$.ajax({
		type: "POST",
		url: "../geo/backAddTagUser",
		contentType: "application/x-www-form-urlencoded",
		data:{data:data},
		dataType: "json",
		processData:true,
		error: function (req, status, error) {

			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success: function (obj) {
			if(obj.msg==="0"){
				$("#htrslt").text("新建成功！");
			}
			if(obj.msg==="1"){
				$("#htrslt").text("新建失败！");
			}
		}
	});
}

/**
 * 个人中心保存
 */
function savesuper() {
	var data={
		password:$("#supernewp").val(),
		email:$("#superemail").val(),
		tel:$("#supertel").val(),
	professional:$("#superprofa").val(),
		academic:$("#superacma").val()   
	};
	data=JSON.stringify(data);
	$.ajax({
		type : "POST",
		url : "../geo/updateAdminUserInfo",
		data:{data:data},
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(obj) {
	     if(obj.msg==="0"){
	    	 $("#hrslt2").text("保存成功！");
	     }
	     if(obj.msg==="1"){
	    	 $("#hrslt2").text("保存失败！");
	     }
		}
	});

}
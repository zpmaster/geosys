/**
 * yzz
 * 
 */
// 自定义了一个Map对象，类似Java里的Map 对象


var hvectorSource;
var hvector_layer
var showSource=new ol.source.Vector();
var showfill= new ol.style.Fill({
	color : 'rgba(255, 0, 0, 0)'
});


$("document").ready(function() {
	   queryEdithistory();
	   $("#queryBtn").on("click",function(){
	   showSource.clear();  
	   CQuery();
	   });
});

/**
 * 地图容器
 */
var hmap = new ol.Map(
		{
			layers : [
					new ol.layer.Group(
							{'title' : '基础底图',
							layers : [
							new ol.layer.Tile(
									{
										title : '天地图影像',
										type : 'base',
										source : new ol.source.XYZ(
												{
													url : 'http://t3.tianditu.com/DataServer?T=img_w&x={x}&y={y}&l={z}'
												})
									})
					]
								})
				],
			target:'hmap',
			view : new ol.View({
				center : [ 11842307, 4098936 ],
				zoom : 3
			})
		});
			    /**
				 * 创建矢量图层
				 */
			       hvectorSource = new ol.source.Vector();
			       hvector_layer = new ol.layer.Vector({
						name : '多边形图层',
						source : hvectorSource, // 加载数据源
						style : new ol.style.Style({
							fill : new ol.style.Fill({
								color : 'rgba(255, 255, 255,0)'
							}),
							stroke : new ol.style.Stroke({
								color : '#ffcc33',
								width : 2
							}),
							image : new ol.style.Circle({
								radius :5,
								fill : new ol.style.Fill({
									color : '#ffcc33'
								})
							})
						})
					});
						
        /**
		 * 绘制查询结果到矢量图层
		 */
		function hdrawWktFeatures(data, zoomlevel) {
			var wktformate = new ol.format.WKT();
			$.each(data, function(i, n) {
				var id = n.id;
				var wkt = n.geom;
				console.log(wkt);
				var feature = wktformate.readFeature(wkt, {
					dataProjection : 'EPSG:4326',
					featureProjection : 'EPSG:3857'
				});
				//feature.setId(id);
				hdrawFeature(feature);
			});

			var extent = hvectorSource.getExtent();
			hmap.getView().setZoom(zoomlevel);
			hmap.getView().setCenter(ol.extent.getCenter(extent));
		}
		hmap.addLayer(hvector_layer);
		hvector_layer.setZIndex(3);

		/**
		 * 添加到图层
		 */
		function hdrawFeature(feature) {
			if (hvectorSource) {
				hvectorSource.addFeature(feature);
			}
		}
						
	/**
	 * 所有 查询编辑历史
	 */
		function queryEdithistory() {
			var seleteType ="all";
			var PNtype ="0";
			var param={
				ftype:seleteType,
			    status:PNtype,
			};
			param=JSON.stringify(param);
		    $.ajax({
				type : 'POST',
				url : "../geo/getmyhistorytag",
				data:{data:param},
				dataType : "json",
				processData : true,
				error : function(req, status, error) {
					if (status == "timeout") {
						alert("请求超时，请稍后再试!！");
						return;
					} else if (status === "error") {
						alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
						return;
					}
					return;
				},
				success : function(obj) {
					// 查询到数据为空时
					if (!obj) {
						return;
					}
				 if(obj.point.length>0){
					 
					 hdrawWktFeatures(obj.point, 10);
				 }
				 if(obj.line.length>0){
					 
					 hdrawWktFeatures(obj.line, 10);
				 }
				 if(obj.polygon.length>0){
					 
					 hdrawWktFeatures(obj.polygon, 10);
				 }
					 var rows = [];
					 //绑定面
					 $.each(obj.polygon,function(i,n){
						 var fid=n.id;
						 var fname=n.fname;
						 var ftype=n.ftype;
						 var fstatus=n.status;
						 if(fstatus==="2"){
							 fstatus="未通过"
						 }
						 if(fstatus==="1"){
							 fstatus="通过"
						 }
						 if(fstatus==="3"){
							 fstatus="未审核"
						 }
						 var geom=n.geom;
						 
                     rows.push({
	                 fid:fid,
	                 fname:fname,
	                 ftype:ftype,
	                 fstatus:fstatus,
	                 geom:geom
                        });
					 });
					 //绑定线
					 $.each(obj.line,function(i,n){
						 var fid=n.id;
						 var fname=n.fname;
						 var ftype=n.ftype;
						 var fstatus=n.status;
						 if(fstatus==="2"){
							 fstatus="未通过"
						 }
						 if(fstatus==="1"){
							 fstatus="通过"
						 }
						 if(fstatus==="3"){
							 fstatus="未审核"
						 }
						 var geom=n.geom;
						 
                     rows.push({
	                 fid:fid,
	                 fname:fname,
	                 ftype:ftype,
	                 fstatus:fstatus,
	                 geom:geom
                        });
					 });
					 //绑定点
					  $.each(obj.point,function(i,n){
						 var fid=n.id;
						 var fname=n.fname;
						 var ftype=n.ftype;
						 var fstatus=n.status;
						 if(fstatus==="2"){
							 fstatus="未通过"
						 }
						 if(fstatus==="1"){
							 fstatus="通过"
						 }
						 if(fstatus==="3"){
							 fstatus="未审核"
						 }
						 var geom=n.geom;
                     rows.push({
	                 fid:fid,
	                 fname:fname,
	                 ftype:ftype,
	                 fstatus:fstatus,
	                 geom:geom
                        });
					 });
					  
					  //关联table
					 $("#hresult").datagrid({data:rows, checkOnSelect: false});
					 $("#hresult").datagrid({
						 onDblClickRow:function(index,row){
						 var wkt=row.geom;
						 //显示
						 ShowFeature(wkt);
					 }
					 });
				}//success结束
			});
		}

		/**
		 * 条件查询
		 */
		function CQuery(){
			var rows = [];
			var seleteType = $("#selectFtype").val();
			var PNtype = $("#PNtype").val();
			if (seleteType === "请选择") {
				seleteType = "all";
			}
			if (PNtype === "请选择") {
				PNtype = "0";
			}
			var param={
               ftype:seleteType,
		       status:PNtype,
			};
			param=JSON.stringify(param);
			$.ajax({
		     type:"GET",
			 url : "../geo/getmyhistorytag",
			 data:{data:param},
			 dataType: "json",
		     processData: true,
			 error: function (req, status, error) {
		        
		         if (status == "timeout") {
		             alert("请求超时，请稍后再试!！");
		             return;
		         } else if (status === "error") {
		             alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
		             return;
		         }
		         return;
		     },
		     success: function (obj) {
		    	// 查询到数据为空时
				if (!obj) {
					return;
				}
				 hvectorSource.clear();
				 if(obj.point && obj.point.length>0){
					  hdrawWktFeatures(obj.point, 10);
					  $.each(obj.point,function(i,n){
					 var fid=n.id;
					 var fname=n.fname;
					 var ftype=n.ftype;
					 var fstatus=n.status;
					 if(fstatus==="2"){
						 fstatus="未通过"
					 }
					 if(fstatus==="1"){
						 fstatus="通过"
					 }
					 if(fstatus==="3"){
						 fstatus="未审核"
					 }
					 var geom=n.geom;
					 
                 rows.push({
                 fid:fid,
                 fname:fname,
                 ftype:ftype,
                 fstatus:fstatus,
                 geom:geom
                    });
				 });
				 }
				 if(obj.line && obj.line.length>0){
					 
					 hdrawWktFeatures(obj.line, 10);
					 $.each(obj.line,function(i,n){
						 var fid=n.id;
						 var fname=n.fname;
						 var ftype=n.ftype;
						 var fstatus=n.status;
						 if(fstatus==="2"){
							 fstatus="未通过"
						 }
						 if(fstatus==="1"){
							 fstatus="通过"
						 }
						 if(fstatus==="3"){
							 fstatus="未审核"
						 }
						 var geom=n.geom;
						 
	                 rows.push({
	                 fid:fid,
	                 fname:fname,
	                 ftype:ftype,
	                 fstatus:fstatus,
	                 geom:geom
	                    });
					 });
				 }
				 if(obj.polygon && obj.polygon.length>0){
					 
					 hdrawWktFeatures(obj.polygon, 10);
					 $.each(obj.polygon,function(i,n){
						 var fid=n.id;
						 var fname=n.fname;
						 var ftype=n.ftype;
						 var fstatus=n.status;
						 if(fstatus==="2"){
							 fstatus="未通过"
						 }
						 if(fstatus==="1"){
							 fstatus="通过"
						 }
						 if(fstatus==="3"){
							 fstatus="未审核"
						 }
						 var geom=n.geom;
	                    rows.push({
	                    fid:fid,
	                    fname:fname,
	                    ftype:ftype,
	                    fstatus:fstatus,
	                    geom:geom
	                    });
					 });
				 }	 
				 
				 $("#hresult").datagrid({data:rows, checkOnSelect: false});
				 $("#hresult").datagrid({
					 onDblClickRow:function(index,row){
					 var wkt=row.geom;
					 //显示
					 ShowFeature(wkt);
				 }
				 });
		     }
			});
		}
		
/**
 * 绘制单个要素到展示图层
 */
function ShowFeature(wkt){
	
	showSource.clear();

  /**默认style**/
	var show_layer=new ol.layer.Vector({
		name : '展示图层',
	    source : showSource// 加载数据源
	});
	
	var pointstyle=new ol.style.Style({
		image: new ol.style.Circle({
		      fill: new ol.style.Fill({
		        color: 'rgba(255,0,0,1)'
		      }),
		      radius:5,
		      stroke: new ol.style.Stroke({
		        color: 'rgba(255,0,0,1)',
		        width: 2
		      })
		  })
	});
	
	var polygonstyle=new ol.style.Style({
		fill: new ol.style.Fill({
		      color: 'rgba(255,0,0,0.1)',
		      width:2
		    }),
		    stroke: new ol.style.Stroke({
		      color: 'rgba(255,0,0,1)',
		      width: 2
		    })
	});
	
	var linestringstyle=new ol.style.Style({
	    stroke: new ol.style.Stroke({
	      color: 'rgba(255,0,0,1)',
	      width: 2
	    })
	  });
	
	var i=wkt.indexOf("POINT");
	var j=wkt.indexOf("LINESTRING");
	var k=wkt.indexOf("POLYGON");
	if(i==0){
		show_layer.setStyle(pointstyle);
	}
	if(j==0){
		show_layer.setStyle(linestringstyle);
	}
	if(k==0){
		show_layer.setStyle(polygonstyle);
	}
	
    	/**默认style**/
	
	var wktformate = new ol.format.WKT();
	var feature = wktformate.readFeature(wkt, {
		dataProjection : 'EPSG:4326',
		featureProjection : 'EPSG:3857'
	});
	showSource.addFeature(feature);
	
	var extent = showSource.getExtent();
	hmap.getView().setZoom(12);
	hmap.getView().setCenter(ol.extent.getCenter(extent));
	
	show_layer.setZIndex(4);
	show_layer.setMap(hmap);
}



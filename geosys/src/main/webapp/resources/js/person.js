$(document).ready(function() {
	inittaginfo();

	$("#xgmm").click(function() {
		$("#xgxx").css("display", "block");
	});
	$("#xgmmupload").click(function() {
		savepinfo();
	});

	$("#apersoninfo").click(function() {
		initperson();
		$("#personinfo").css("display", "block");
		$("#taginfo").css("display", "none");
		
	});

	$("#ataginfo").click(function() {
		inittaginfo();
		$("#personinfo").css("display", "none");
		$("#taginfo").css("display", "block");
	});

});
function inittaginfo() {
	$.ajax({
		type : "GET",
		url : "../geo/getTagStatusCount",
		dataType : "json",
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(data) {

			if (!data) {
				alert("无数据！");
				return;
			}
			$("#totalc").text(data.sum);
			$("#passc").text(data.checkpass);
			$("#unpassc").text(data.checkunpass);
			$("#uncheckc").text(data.uncheck);
		}

	});
}

function initperson() {
	$.ajax({
		type : "GET",
		url : "../geo/getUserInfo",
		dataType : "json",
	error : function(req, status, error) {
		if (status == "timeout") {
			alert("请求超时，请稍后再试！");
			return;
		} else if (status === "error") {
			alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
			return;
		}
		return;
	},
		
		success : function(data) {			
			if (!data) {
				alert("无数据！");
				return;
			}
			$("#username").text(data.userName);
			$("#em").text(data.email);
			$("#tel").text(data.tel);
			$("#cxl").text(data.diploma);
			$("#cdz").text(data.address);
			$("#czy").text(data.professional);
			
			$("#emu").val(data.email);
			$("#telu").val(data.tel);
			$("#xlu").val(data.diploma);
			$("#dzu").val(data.address);
			$("#zyu").val(data.professional);
		}

	});
}

function savepinfo() {
	var p1 = $("#pass1u").val();
	var p2 = $("#pass2u").val();
	if (p1 === '' || p2 === "") {
		//alert("密码不能为空！");
		$("#rslt").text("密码不能为空!");
		return;
	}
	if (p1!== p2) {
		//alert("两次输入密码不一致！");
		$("#rslt").text("输入密码两次不同！");
		return;
	}
	var pinfo = {
		username : $("#username").val(),
	    address:$("#dzu").val(),
	    diploma:$("#xlu").val(),
		email : $("#emu").val(),
	professional:$("#zyu").val(),
		tel : $("#telu").val(),
		password : $("#pass2u").val()
	};
	pinfo = JSON.stringify(pinfo);
	$.ajax({
		type : "GET",
		url : "../geo/updateUserInfo",
		data : {
			data : pinfo
		},
		dataType : "json",
		processData : true,
		error : function(req, status, error) {
			if (status == "timeout") {
				alert("请求超时，请稍后再试！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success:function(obj){
			if(obj.msg==="0"){
				$("#rslt").text("修改成功!");
			}
			if(obj.msg==="1"){
				$("#rslt").text("修改失败!");
			}
		}
			
	});
}

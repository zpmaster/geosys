/**所有
 * 查询编辑历史
 */
function queryEdithistory() {
	var seleteType ="all";
	var PNtype ="0";
	var param={
		ftype:seleteType,
	    status:PNtype,
	};
	param=JSON.stringify(param);
$.ajax({
		type : 'POST',
		url : "../geo/getmyhistorytag",
		data:{data:param},
		dataType : "json",
		processData : true,
		error : function(req, status, error) {

			if (status == "timeout") {
				alert("请求超时，请稍后再试!！");
				return;
			} else if (status === "error") {
				alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
				return;
			}
			return;
		},
		success : function(obj) {
			// 查询到数据为空时
			if (!obj) {
				return;
			}
			 var rows = [];
			 $.each(obj.data,function(i,n){
				 var fid=n.fid;
				 var fname=n.fname;
				 var ftype=n.ftype;
				 var fstatus=n.status;
				 var geom=n.geom;
			 });
			 rows.push({
				 fid:fid,
				 fname:fname,
				 ftype:ftype,
				 fstatus:fstatus,
				// flook："<button id="+ fid +"class='easyui-linkbutton' onclick='look(this.id)'"+></button>",
				 geom:geom
			 });
			 $("#hresult").datagrid({data:rows, checkOnSelect: false});
		}
	});
}

/**
 * 条件查询
 */
function CQuery(){
	var seleteType = $("#selectFtype").val();
	var PNtype = $("#selectFtype").val();
	if (seleteType === "请选择") {
		seleteType = "all";
	}
	if (PNtype === "请选择") {
		PNtype = "0";
	}
	var param={
		seleteType:seleteType,
	    PNtype:PNtype,
	};
	param=JSON.stringify(param);
	$.ajax({
     type:"GET",
     url:""+param,
	 dataType: "json",
     processData: true,
	 error: function (req, status, error) {
        ajaxLoadEnd();
         if (status == "timeout") {
             alert("请求超时，请稍后再试!！");
             return;
         } else if (status === "error") {
             alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
             return;
         }
         return;
     },
     success: function (obj) {
    	// 查询到数据为空时
		if (!obj) {
			return;
		}
		 var rows = [];
		 $.each(obj.data,function(i,n){
			 var fid=n.fid;
			 var fname=n.fname;
			 var ftype=n.ftype;
			 var fstatus=n.status;
			 var geom=n.geom;
		 });
		 rows.push({
			 fid:fid,
			 fname:fname,
			 ftype:ftype,
			 fstatus:fstatus,
			 geom:geom
		 });
		 $("#hresult").datagrid({
			 data:rows}
	  );
     }
	});
		
}

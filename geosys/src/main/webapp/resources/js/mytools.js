/**
 * 转换到经纬度坐标
 */
function XYtoLonLat(str) {
	var outstr="";
	var ta = [];
	var taout = [];
	var pstr="";
	//str = "POLYGON((13382296.864912655 3933857.2235766347,13383713.974985957 3935045.5813073516,13385170.817117378 3933366.0712109404,13383303.410385275 3932573.9337118184,13382296.864912655 3933857.2235766347))";
	var sindex = str.lastIndexOf("(") + 1;
	var eindex = str.indexOf(")");

	var pref = str.substring(0, sindex);
	var bcaks = str.substring(eindex);

	var vstr = str.substring(sindex, eindex);
	var parray = vstr.split(",");
	// parray=["13382296.864912655 3933857.2235766347", "13383713.974985957
	// 3935045.5813073516", "13385170.817117378 3933366.0712109404",
	// "13383303.410385275 3932573.9337118184", "13382296.864912655
	// 3933857.2235766347"]
	for ( var i = 0; i < parray.length; i++) {
		var ca = parray[i].split(" ");
		for ( var j = 0; j < 2; j++) {
			ca[j] = parseFloat(ca[j]);
			ta.push(ca[j]);
		}
		var taout = ol.proj.toLonLat(ta, "EPSG:3857");
		ta = [];
		//console.log(taout);
		// taout = [120.21521810375793, 33.28997780998594]
		for ( var k = 0; k < 2; k++) {
			if (k == 0) {
				var ttt1=String(taout[0]);
				pstr =ttt1 + " ";
			}
			if (k ==1) {
				var ttt=String(taout[1]);
				pstr = pstr + ttt;
			}
		}
		
		if (i != parray.length - 1) {
			outstr =outstr+pstr + ",";
			//console.log(outstr);
		}
		else{
			outstr =outstr+pstr;
		}
		
		pstr="";
	}
	outstr = pref + outstr + bcaks;
	//console.log(outstr);
	return outstr;
}
/**
 * 获取要素一个顶点
 */
function getFeatureV(feature){
	var extent=feature.geom;
	var wktformate = new ol.format.WKT();
	var wkt = wktformate.readFeature(extent, {
		dataProjection : 'EPSG:4326',
		featureProjection : 'EPSG:3857'
	});
	var sindex = wkt.lastIndexOf("(") + 1;
	var eindex = wkt.indexOf(",");
	var incoord=wkt.substring(sindex,eindex);
	var outcoord=[];
	var parray = vstr.split(" ");
	for(var i=0;i<2;i++){
		parray[i]=parseFloat(parray[i]);
		outcoord.push(parray[i]);
	}
	return outcoord;
}
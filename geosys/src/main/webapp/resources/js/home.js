

var map = new ol.Map(
		{
			layers : [
					new ol.layer.Group(
							{'title' : '基础底图',
							layers : [
							new ol.layer.Tile(
									{
										title : '天地图影像',
										type : 'base',
										source : new ol.source.XYZ(
												{
													url : 'http://t3.tianditu.com/DataServer?T=img_w&x={x}&y={y}&l={z}'
												})
									}),
// // // 谷歌地圖
// // //初始化Google图层
// new ol.layer.Tile(
// {
// title : ' OSM地图',
// type : 'base',
// source : new ol.source.XYZ(
// {
// url : 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
// })
// }),
							new ol.layer.Tile(
							{
								title : ' 必应影像',
								type : 'base',
								source :  new ol.source.BingMaps({
									         imagerySet: 'Aerial',
					             	         key: 'AkGbxXx6tDWf1swIhPJyoAVp06H0s0gDTYslNWWHZ6RoPqMpB9ld5FY1WutX8UoF'
										})
							})

					]
								}),
							
					new ol.layer.Group(
							{
								title : '叠加图层',
							layers : [
							
							new ol.layer.Tile(
									{
										title : ' OSM地图',
										source : new ol.source.XYZ(
												{
													url : 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
												})
									}),
									
									new ol.layer.Tile(
											{
												title : '天地图矢量',
												source : new ol.source.XYZ(
														{
															url : "http://t4.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}"
														})
										
							}),
									new ol.layer.Tile(
									{
										title : '天地图注记',
										source : new ol.source.XYZ(
												{
													url : 'http://t3.tianditu.com/DataServer?T=cva_w&x={x}&y={y}&l={z}'
												})

									})],
								})],

			target:'map',
			view : new ol.View({
				center : [ 11842307, 4098936 ],
				zoom : 3

			})
		});
var layerSwitcher = new ol.control.LayerSwitcher({
	tipLabel : '图层管理' // Optional label for button
});
map.addControl(layerSwitcher);
var tian_di_tu_road_layer = new ol.layer.Tile({
	title : "天地图路网",
	source : new ol.source.XYZ({
		url : "http://t4.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}"
	})
});

var tian_di_tu_annotation = new ol.layer.Tile({
	title : "天地图文字标注",
	source : new ol.source.XYZ({
		url : 'http://t3.tianditu.com/DataServer?T=cva_w&x={x}&y={y}&l={z}'
	})
});

var tian_di_tu_satellite_layer = new ol.layer.Tile({
	title : "天地图卫星影像",
	source : new ol.source.XYZ({
		url : 'http://t3.tianditu.com/DataServer?T=img_w&x={x}&y={y}&l={z}'
	})
});

var wkt = 'POLYGON((10.689 -25.092, 34.595 '
		+ '-20.170, 38.814 -35.639, 13.502 ' + '-39.155, 10.689 -25.092))';

var wkt2 = 'POLYGON((15.689 -20.092, 40.595 '
		+ '-10.170, 43.814 -30.639, 18.502 ' + '-34.155, 15.689 -20.092))';

var format = new ol.format.WKT();// 创建wkt 解析器

var feature1 = format.readFeature(wkt, {
	dataProjection : 'EPSG:4326',
	featureProjection : 'EPSG:3857'
});

var feature2 = format.readFeature(wkt2, {
	dataProjection : 'EPSG:4326',
	featureProjection : 'EPSG:3857'
});

var farray = [];
farray.push(feature1);
farray.push(feature2);

// 创建矢量图层
var vector = new ol.layer.Vector({
	source : new ol.source.Vector({
		features : farray
	// 加载要素数组
	})
});

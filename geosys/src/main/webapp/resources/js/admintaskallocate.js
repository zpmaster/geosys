/**
 * Created by yzz on 2016/12/18.
 */
var adminvectorSource;
var adminvector_layer;
var admintaskgridSource;
var admintaskgrid_layer;


$(document).ready(function () {
    queryUser();
    $("#adminedittaskbtn").click(function () {
        adminEditTask();

    });
});

var style = new ol.style.Style({
    fill: new ol.style.Fill({
        color: 'rgba(255, 255, 255,0.1)'
    }),
    stroke: new ol.style.Stroke({
        color: '#5eff38',
        width: 2
    }),
    image: new ol.style.Circle({
        radius: 5,
        fill: new ol.style.Fill({
            color: '#ffcc33'
        })
    })
});
/**
 * 加载数据
 */
function adminLoaddata() {
    $.ajax({
        type: "GET",
        url: "../geo/getAllNgccOrigPolygon",
        dataType: "json",
        processData: true,
        error: function (req, status, error) {
            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (data) {
            //无区域坐标时不绘制
            if (!data) {
                return;
            }
            admindrawWktFeatures(data, 15);
        }
    });
}
/**
 * 获取所有审核任务
 */
function adminTaskLoadData() {
    $.ajax({
        type: "GET",
        url: "../geo/getAuditerTask",
        dataType: "json",
        processData: true,
        error: function (req, status, error) {
            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (data) {
            //无区域坐标时不绘制
            if (data.length==0) {
                alert("编辑员还未提交审核任务！");
                return;
            }
            adminTaskdrawWktFeatures(data, 15);
        }
    });
}

/**
 * 绘制审核任务查询结果到矢量图层
 */
function adminTaskdrawWktFeatures(data, zoomlevel) {
    var wktformate = new ol.format.WKT();
    $.each(data, function (i, n) {
        var id = n.uuid;
        var wkt = n.geom;
        var feature = wktformate.readFeature(wkt, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        });
        feature.setId(id);


        if (admintaskgridSource) {
            admintaskgridSource.addFeature(feature);
        }
    });

    var extent = admintaskgridSource.getExtent();
    admintaskmap.getView().setZoom(zoomlevel);
    admintaskmap.getView().setCenter(ol.extent.getCenter(extent));
    admintaskmap.addLayer(admintaskgrid_layer);
    admintaskgrid_layer.setZIndex(10);


}

function adminCreateTask() {
    $("#createtask").css("dispaly", "block");
}
/**
 * 提交格网参数
 */
function adminEditTask() {
    var data = {
        numl: $("#gridnuml").val(),
        numc: $("#gridnumc").val(),
        minLon: $("#gridminlng").val(),
        maxLon: $("#gridmaxlng").val(),
        minLat: $("#gridminlat").val(),
        maxLat: $("#gridmaxlat").val(),
        personnum: $("#gridpersionnum").val()
    };
    data = JSON.stringify(data);
    $.ajax({
        type: "POST",
        url: "../geo/getGridParm",
        contentType: "application/x-www-form-urlencoded",
        data: {data: data},
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function () {
        }
    });
}


/**
 * 地图容器
 */
var admintaskmap = new ol.Map(
    {
        layers: [
            new ol.layer.Group(
                {
                    'title': '基础底图',
                    layers: [
                        new ol.layer.Tile(
                            {
                                title: ' 必应影像',
                                type: 'base',
                                source: new ol.source.BingMaps({
                                    imagerySet: 'Aerial',
                                    key: 'AkGbxXx6tDWf1swIhPJyoAVp06H0s0gDTYslNWWHZ6RoPqMpB9ld5FY1WutX8UoF'
                                })
                            })
                    ]
                })
        ],
        target: 'admintaskmap',
        view: new ol.View({
            center: [11842307, 4098936],
            zoom: 3
        })
    });
/**
 * 创建矢量图层
 */
adminvectorSource = new ol.source.Vector();
adminvector_layer = new ol.layer.Vector({
    name: '多边形图层',
    source: adminvectorSource, // 加载数据源
    style: new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255,0)'
        }),
        stroke: new ol.style.Stroke({
            color: '#ffcc33',
            width: 2
        }),
        image: new ol.style.Circle({
            radius: 5,
            fill: new ol.style.Fill({
                color: '#ffcc33'
            })
        })
    })
});

/**
 * 绘制查询结果到矢量图层
 */
function admindrawWktFeatures(data, zoomlevel) {
    var wktformate = new ol.format.WKT();
    $.each(data, function (i, n) {
        var id = n.id;
        var wkt = n.geom;
        var feature = wktformate.readFeature(wkt, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        });
        //feature.setId(id);
        if (adminvectorSource) {
            adminvectorSource.addFeature(feature);
        }
    });

    var extent = adminvectorSource.getExtent();
    admintaskmap.getView().setZoom(zoomlevel);
    admintaskmap.getView().setCenter(ol.extent.getCenter(extent));
}
admintaskmap.addLayer(adminvector_layer);
adminvector_layer.setZIndex(3);


/**
 * 展示任务
 */
function adminshowTask() {
    $.ajax({
        type: "GET",
        url: "../geo/getTask",
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (data) {
            //无区域坐标时不绘制
            if (!data) {
                return;
            }
            admindrawTasks(data, 14);
        }
    });
}

/**
 * 创建任务图层
 */
admintaskgridSource = new ol.source.Vector();
admintaskgrid_layer = new ol.layer.Vector({
    name: '多边形图层',
    source: admintaskgridSource, // 加载数据源
    style: new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255,0.1)'
        }),
        stroke: new ol.style.Stroke({
            color: '#45ff22',
            width: 2
        }),
        image: new ol.style.Circle({
            radius: 5,
            fill: new ol.style.Fill({
                color: '#ff151f'
            })
        })
    })
});

/**
 * 绘制任务
 */

function admindrawTasks(data, zoomlevel) {
    var wktformate = new ol.format.WKT();
    $.each(data, function (i, n) {
        var id = n.uuid;
        var wkt = n.value;
        var feature = wktformate.readFeature(wkt, {
            dataProjection: 'EPSG:4326',
            featureProjection: 'EPSG:3857'
        });
        feature.setId(id);
        if (admintaskgridSource) {
            admintaskgridSource.addFeature(feature);
        }
    });

    var extent = admintaskgridSource.getExtent();
    admintaskmap.getView().setZoom(zoomlevel);
    admintaskmap.getView().setCenter(ol.extent.getCenter(extent));
}
admintaskmap.addLayer(admintaskgrid_layer);
admintaskgrid_layer.setZIndex(4);


// 面 交互
var taskadminselect_interaction;
taskadminselect_interaction = new ol.interaction.Select({
    // 操作图层
    layers: function (admintaskgrid_layer) {
        return admintaskgrid_layer.get('name') === '多边形图层';// 选择多边形图层
    }
});
admintaskmap.addInteraction(taskadminselect_interaction);


/**
 * 任务分配
 */
function adminAllocateTask() {
    var taskselected_features;
    var format = new ol.format['WKT'](),
        taskgeom;
    taskselected_features = taskadminselect_interaction.getFeatures();
    var taskfea = taskselected_features.item(0);
    var taskid = taskfea.getId() + "";

    // $("#dialog").css("display", "block");
    $("#dialog").dialog();
    var tasdata = {
        taskid: taskid
    }
    $("#taskup").unbind("click").on("click", tasdata, saveeditortask);
}
/**
 * 保存审核员任务分配
 */
function saveeditortask(tasdata) {

    var selectId = $('#editors>option:selected');
    var editid;
    var un = $("#editors").find("option:selected").text();
    selectId.val(function () {
        editid = this.id;
    })

    var parameters = {
        taskId: tasdata.data.taskid,
        editerId: editid,
        editerName: un
    };
    parameters = JSON.stringify(parameters);
    $.ajax({
        type: "POST",
        url: "../geo/setAuditerAndName",
        contentType: "application/x-www-form-urlencoded",
        data: {data: parameters},
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function () {

        }

    });
    $("#dialog").dialog("close");
}
/**
 * 查询用户
 */
function queryUser() {
    $.ajax({
        type: "GET",
        url: "../geo/getAllAuditer",
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (data) {
            $("#editors").append("<option>请选择</option>");
            $.each(data, function (i, n) {
                var name = n.userName;
                var id = n.id;
                $("#editors").append("<option id='" + id + "'>" + name + "</option>");
            });
        }
    });
}

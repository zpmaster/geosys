// 自定义了一个Map对象，类似Java里的Map 对象
var vecLayers = new Map();

var hvectorSource = new ol.source.Vector();
/**
 * 地图容器
 */
var hmap = new ol.Map(
		{
			layers : [
					new ol.layer.Group(
							{'title' : '基础底图',
							layers : [
							new ol.layer.Tile(
									{
										title : '天地图影像',
										type : 'base',
										source : new ol.source.XYZ(
												{
													url : 'http://t3.tianditu.com/DataServer?T=img_w&x={x}&y={y}&l={z}'
												})
									})
// new ol.layer.Tile(
// {
// title : ' 必应影像',
// type : 'base',
// source : new ol.source.BingMaps({
// imagerySet: 'Aerial',
// key: 'AkGbxXx6tDWf1swIhPJyoAVp06H0s0gDTYslNWWHZ6RoPqMpB9ld5FY1WutX8UoF'
// })
// })

					]
								})
// new ol.layer.Group(
// {
// title : '叠加图层',
// layers : [
// new ol.layer.Tile(
// {
// title : '天地图注记',
// source : new ol.source.XYZ(
// {
// url : 'http://t3.tianditu.com/DataServer?T=cva_w&x={x}&y={y}&l={z}'
// })
//
// })],
// })
					],

			target:'hmap',
			view : new ol.View({
				center : [ 11842307, 4098936 ],
				zoom : 3
			})
		});

var hlayerSwitcher = new ol.control.LayerSwitcher({
	tipLabel : '图层管理' // Optional label for button
});
hmap.addControl(hlayerSwitcher);
var layers = [ new ol.layer.Tile(
		{
			source : new ol.source.BingMaps(
					{
						imagerySet : 'Aerial',
						key : 'AkGbxXx6tDWf1swIhPJyoAVp06H0s0gDTYslNWWHZ6RoPqMpB9ld5FY1WutX8UoF'
					})
		})
// new ol.layer.Tile({
// source: new ol.source.TileWMS({
// url: 'http://localhost:8080/geoserver/changsha/wms',
// params: {'LAYERS': 'changsha:Area'},
// serverType: 'geoserver'
// })
// }),
// new ol.layer.Tile({
// source: new ol.source.TileWMS({
// url: 'http://localhost:8080/geoserver/changsha/wms',
// params: {'LAYERS': 'changsha:allpopint'},
// serverType: 'geoserver'
// })
// })
];
	    /**
		 * 创建矢量图层
		 */
		var hvector_layer = new ol.layer.Vector({
			name : '多边形图层',
			source : hvectorSource, // 加载数据源
			style : new ol.style.Style({
				fill : new ol.style.Fill({
					color : 'rgba(255, 255, 255, 0.2)'
				}),
				stroke : new ol.style.Stroke({
					color : '#ffcc33',
					width : 2
				}),
				image : new ol.style.Circle({
					radius : 7,
					fill : new ol.style.Fill({
						color : '#ffcc33'
					})
				})
			})
		});
		//hmap.addLayer(hvector_layer);
		//hvector_layer.setVisible(true);
		//hvector_layer.setZIndex(0);
		
		

	/**
	 * 所有 查询编辑历史
	 */
		function queryEdithistory() {
			var seleteType ="all";
			var PNtype ="0";
			var param={
				ftype:seleteType,
			    status:PNtype,
			};
			param=JSON.stringify(param);
		$.ajax({

				type : 'POST',
				url : "../geo/getmyhistorytag",
				data:{data:param},
				dataType : "json",
				processData : true,
				error : function(req, status, error) {

					if (status == "timeout") {
						alert("请求超时，请稍后再试!！");
						return;
					} else if (status === "error") {
						alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
						return;
					}
					return;
				},
				success : function(obj) {
					// 查询到数据为空时
					if (!obj) {
						return;
					}
					 hdrawWktFeatures(obj.polygon, 12);
					 var rows = [];
					 $.each(obj.polygon,function(i,n){
						 var fid=n.id;
						 var fname=n.fname;
						 var ftype=n.ftype;
						 var fstatus=n.status;
						 if(fstatus==="2"){
							 fstatus="未通过"
						 }
						 if(fstatus==="1"){
							 fstatus="通过"
						 }
						 if(fstatus==="3"){
							 fstatus="未审核"
						 }
						 var geom=n.geom;
						 
                     rows.push({
	                 fid:fid,
	                 fname:fname,
	                 ftype:ftype,
	                 fstatus:fstatus,
	                 geom:geom
                        });
					 });
					 $("#hresult").datagrid({data:rows, checkOnSelect: false});
					 $("#hresult").datagrid({
						 onDbClickRow:function(index,row){
						 var feature=row.fid;
						 alert(feature);
						 feature.setStyle(new ol.style.Style(
							 {
								 fill:new ol.style.Fill({
									 color:'rgba(255, 255, 255, 0.2)'
								 })
							 }));
						 
						 var centerp=getFeatureV(feature);
						 hmap.getView().setCenter(centerp);
					 }
					 });
				}
			});
		}

		/**
		 * 条件查询
		 */
		function CQuery(){
			var seleteType = $("#selectFtype").val();
			var PNtype = $("#PNtype").val();
			if (seleteType === "请选择") {
				seleteType = "all";
			}
			if (PNtype === "请选择") {
				PNtype = "0";
			}
			var param={
               ftype:seleteType,
		       status:PNtype,
			};
			param=JSON.stringify(param);
			$.ajax({
		     type:"GET",
			url : "../geo/getmyhistorytag",
			data:{data:param},
			 dataType: "json",
		     processData: true,
			 error: function (req, status, error) {
		        
		         if (status == "timeout") {
		             alert("请求超时，请稍后再试!！");
		             return;
		         } else if (status === "error") {
		             alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
		             return;
		         }
		         return;
		     },
		     success: function (obj) {
		    	// 查询到数据为空时
				if (!obj) {
					return;
				}
				 hdrawWktFeatures(obj.polygon, 12);
				 var rows = [];
				 $.each(obj.polygon,function(i,n){
					 var fid=n.id;
					 var fname=n.fname;
					 var ftype=n.ftype;
					 var fstatus=n.status;
					 if(fstatus==="2"){
						 fstatus="未通过"
					 }
					 if(fstatus==="1"){
						 fstatus="通过"
					 }
					 if(fstatus==="3"){
						 fstatus="未审核"
					 }
					 var geom=n.geom;
					 
                 rows.push({
                 fid:fid,
                 fname:fname,
                 ftype:ftype,
                 fstatus:fstatus,
                 geom:geom
                    });
				 });
				
				 $("#hresult").datagrid({data:rows, checkOnSelect: false});
				 $("#hresult").datagrid({
					 onDbClickRow:function(index,row){
					 var feature=row.fid;
					 
					 feature.setStyle(new ol.style.Style(
						 {
							 fill:new ol.style.Fill({
								 color:'rgba(255, 255, 255, 0.2)'
							 })
						 }));
					 
					 var centerp=getFeatureV(feature);
					 hmap.getView().setCenter(centerp);
				 }
				 });
		     }
			});
		}
		
		
	    /**
		 * 绘制查询结果到矢量图层
		 */
		function hdrawWktFeatures(data, zoomlevel) {
			var wktformate = new ol.format.WKT();
			$.each(data, function(i, n) {
				var id = n.id;
				var wkt = n.geom;
				var feature = wktformate.readFeature(wkt, {
					dataProjection : 'EPSG:4326',
					featureProjection : 'EPSG:3857'
				});
				feature.setId(id);
				hdrawFeature(feature);
			});

			var extent = hvectorSource.getExtent();
			hmap.getView().setZoom(zoomlevel);
			//hmap.getView().setCenter(ol.extent.getCenter(extent));
		}

		/**
		 * 添加到图层
		 */
		function hdrawFeature(feature) {
			if (hvectorSource) {
				hvectorSource.addFeature(feature);
			}
		}
		
		
var wkt = 'POLYGON((10.689 -25.092, 34.595 '
+ '-20.170, 38.814 -35.639, 13.502 ' + '-39.155, 10.689 -25.092))';

var wkt2 = 'POLYGON((15.689 -20.092, 40.595 '
+ '-10.170, 43.814 -30.639, 18.502 ' + '-34.155, 15.689 -20.092))';

var format2 = new ol.format.WKT();// 创建wkt 解析器

var feature1 = format2.readFeature(wkt, {
dataProjection : 'EPSG:4326',
featureProjection : 'EPSG:3857'
});

var feature2 = format2.readFeature(wkt2, {
dataProjection : 'EPSG:4326',
featureProjection : 'EPSG:3857'
});

var farray = [];
farray.push(feature1);
farray.push(feature2);

//创建矢量图层
var vector = new ol.layer.Vector({
source : new ol.source.Vector({
features : farray
// 加载要素数组
})
});
hmap.addLayer(vector);
hmap.addLayer(hvector_layer);


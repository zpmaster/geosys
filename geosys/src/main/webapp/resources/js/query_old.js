/**
 * 数据库
 */

$(document).ready(function(){

});

/**
 * 原始面数据查询
 * 数据库取shp表
 */
function queryPolygondata(taskid){
	 $.ajax({
	        type: "GET",
	        url: "../geo/getDataByTaskId",
	        dataType: "json",
	        processData: true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function (data) {
	             //无区域坐标时不绘制
	            if (!data) {
	                return;
	            }
	            drawWktFeatures(data, 12);
	        }
	   });
}

	
	
/**
 * 点数据查询
 * 数据库取shp表
 */
function queryPointdata(){
	 $.ajax({
	        type: "GET",
	        url: "../geo/getOrigPoint",
	        dataType: "json",
	        processData: true,
	        error: function (req, status, error) {
	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function (data) {
	             //无区域坐标时不绘制
	            if (!data) {
	                return;
	            }
	            drawPointWktFeatures(data, 12);
	        }
	   });
}


    /**
	 * 线数据
	 * 查询
	 */
function queryLinedata(){
	 $.ajax({
	        type: "GET",
	        url: "../geo/getOrigLine",
	        dataType: "json",
	        processData: true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function (data) {
	             //无区域坐标时不绘制
	            if (!data) {
	                return;
	            }
	            drawLineWktFeatures(data, 12);
	        }
	   });
}

/**
 * 面  new
 * 保存到数据库
 */

function savecdata(d){
	  var geodata=d.data.geom;
	  var pdata={
			 ftype:$("#oldtype").val(),
			 fname:$("#fname").val(),
			 fdes:$("#fdes").val(),
			 geom:geodata
	 }
	 var data = JSON.stringify(pdata);
	
	 $("#dialog").dialog("close");
	 $.ajax({
	        type: "POST",
	        url: "../geo/newPolygon",
	        contentType: "application/x-www-form-urlencoded",
	        data:{data:data},
	        dataType: "json",	  
	        processData:true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function () {
	            alert("保存成功！");
	        }
	   });
	 $("#dpup").unbind("click");
}

/**
 * 点  new
 * 保存到数据库
 */

function savecpdata(d){
	  var geodata=d.data.geom;
	  var pdata={
			 ftype:$("#oldtype").val(),
			 fname:$("#fname").val(),
			 fdes:$("#fdes").val(),
			 geom:geodata
	 }
	 var data = JSON.stringify(pdata);
	 
	 $("#dialog").dialog("close");
	 $.ajax({
	        type: "POST",
	        url: "../geo/newPoint",
	        contentType: "application/x-www-form-urlencoded",
	        data:{data:data},
	        dataType: "json",	  
	        processData:true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function () {
	            alert("保存成功！");
	        }
	   });
	 $("#dpup").unbind("click");
}


/**
 * 线  new
 * 保存到数据库
 */
function savecldata(d){
	  var geodata=d.data.geom;
	  var pdata={
			 ftype:$("#oldtype").val(),
			 fname:$("#fname").val(),
			 fdes:$("#fdes").val(),
			 geom:geodata
	 }
	 var data = JSON.stringify(pdata);
	 
	 $("#dialog").dialog("close");
	 $.ajax({
	        type: "POST",
	        url: "../geo/newLine",
	        contentType: "application/x-www-form-urlencoded",
	        data:{data:data},
	        dataType: "json",	  
	        processData:true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function () {
	            alert("保存成功！");
	        }
	   });
	 $("#dpup").unbind("click");
}

/**
 * 保存属性信息
 * 
 */

function savedesdata(){
	   var pdata={
			 ftype:$("#oldtype").val(),
			 fname:$("#fname").val(),
			 fdes:$("#fdes").val()
	 }
	 var data = JSON.stringify(pdata);
	  
	 $("#dialog").dialog("close");
	 $.ajax({
	        type: "POST",
	        url: "../geo/newPolygon",
	        contentType: "application/x-www-form-urlencoded",
	        data:{data:data},
	        dataType: "json",	  
	        processData:true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function () {
	            alert("保存成功！");
	        }
	   });
	 $("#dpup").unbind("click");
}

/**
 * 面
 * 修改
 * 保存到数据库
 */

function saveudata(d){
	  var geodata=d.data.geom;
	  var pdata={
			 id:d.data.id,
			 ftype:$("#oldtype").val(),
			 fname:$("#fname").val(),
			 fdes:$("#fdes").val(),
			 geom:geodata
	 }
	 var data = JSON.stringify(pdata);
	 
	 $("#dialog").dialog("close");
	 $.ajax({
	        type: "POST",
	        url: "../geo/savePolygon",
	        contentType: "application/x-www-form-urlencoded",
	        data:{data:data},
	        dataType: "json",	  
	        processData:true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function () {
	            alert("保存成功！");
	        }
	   });
	 $("#dpup").unbind("click");
}

/**
 * 点
 * 修改保存
 */
function psaveudata(d){
	  var geodata=d.data.geom;
	  var pdata={
			 id:d.data.id,
			 ftype:$("#oldtype").val(),
			 fname:$("#fname").val(),
			 fdes:$("#fdes").val(),
			 geom:geodata
	 }
	 var data = JSON.stringify(pdata);
	 
	 $("#dialog").dialog("close");
	 $.ajax({
	        type: "POST",
	        url: "../geo/savePoint",
	        contentType: "application/x-www-form-urlencoded",
	        data:{data:data},
	        dataType: "json",	  
	        processData:true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function () {
	            alert("保存成功！");
	        }
	   });
	 $("#dpup").unbind("click");
}

/**
 * 点
 * 修改保存
 */
function lsaveudata(d){
	  var geodata=d.data.geom;
	  var pdata={
			 id:d.data.id,
			 ftype:$("#oldtype").val(),
			 fname:$("#fname").val(),
			 fdes:$("#fdes").val(),
			 geom:geodata
	 }
	 var data = JSON.stringify(pdata);
	 
	 $("#dialog").dialog("close");
	 $.ajax({
	        type: "POST",
	        url: "../geo/saveLine",
	        contentType: "application/x-www-form-urlencoded",
	        data:{data:data},
	        dataType: "json",	  
	        processData:true,
	        error: function (req, status, error) {

	            if (status == "timeout") {
	                alert("请求超时，请稍后再试!！");
	                return;
	            } else if (status === "error") {
	                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
	                return;
	            }
	            return;
	        },
	        success: function () {
	            alert("保存成功！");
	        }
	   });
	 $("#dpup").unbind("click");
}


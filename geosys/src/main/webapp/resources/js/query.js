/**
 * 初始编辑面任务
 */
var globetaskid;
var doubleglobetaskid;
function getTaskByEditerName() {
    $.ajax({
        type: "GET",
        url: "../geo/getTaskByEditerName",
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (data) {
            var rows = [];
            $.each(data, function (i, n) {
                var taskid = n.taskId;
                var startbtn = "<button title='开始任务' style='background: url(/geo/resources/images/start.png);background-repeat:no-repeat;height:20px;width:20px'></button>";
                var endbtn = "<button title='提交任务' style='background: url(/geo/resources/images/ok.png);background-repeat:no-repeat;height:20px;width:20px'></button>"
                rows.push({
                    taskid: taskid,
                    startbtn: startbtn,
                    endbtn: endbtn
                });
            });
            $("#taskslist").datagrid({data: rows, checkOnSelect: false});
            $("#taskslist").datagrid({
                onClickCell: function (index, field, value) {
                    if (field === "startbtn") {
                        var selectRow = $(this).datagrid("getData").rows[index];
                        var taskid = selectRow.taskid;
                        vectorSource.clear();
                        task_Source.clear();
                        queryTaskById(taskid);
                        getTaskGeomById(taskid);
                        globetaskid = taskid;
                    }
                    if (field === "endbtn") {
                        var selectRow = $(this).datagrid("getData").rows[index];
                        var taskid = selectRow.taskid;
                        upLoadTask(taskid, "1");
                        task_Source.clear();
                        vectorSource.clear();
                    }
                }
            });
        }
    });
}
/**
 * 审核未通过任务
 */
function getDoubleTaskByEditerName() {
    $.ajax({
        type: "GET",
        url: "../geo/findNotAuditStatus",
        dataType: "json",
        processData: true,
        error: function (req, status, error) {
            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (data) {
            var rows = [];
            $.each(data, function (i, n) {
                var taskid = n.taskId;
                var startbtn = "<button title='开始任务' style='background: url(/geo/resources/images/start.png);background-repeat:no-repeat;height:20px;width:20px'></button>";
                var endbtn = "<button title='提交任务' style='background: url(/geo/resources/images/ok.png);background-repeat:no-repeat;height:20px;width:20px'></button>"
                rows.push({
                    doubletaskid: taskid,
                    doublestartbtn: startbtn,
                    doublendbtn: endbtn
                });
            });
            $("#doubletaskslist").datagrid({data: rows, checkOnSelect: false});
            $("#doubletaskslist").datagrid({
                onClickCell: function (index, field, value) {
                    /**
                     * 开始任务
                     */
                    if (field === "doublestartbtn") {
                        var doubleselectRow = $(this).datagrid("getData").rows[index];
                        var doubletaskid = doubleselectRow.doubletaskid;
                        vectorSource.clear();
                        task_Source.clear();
                        queryDoubleTaskById(doubletaskid);
                        getTaskGeomById(doubletaskid);
                        globetaskid = doubletaskid;
                    }
                    /**
                     * 结束任务
                     */
                    if (field === "doublendbtn") {
                        var selectRow = $(this).datagrid("getData").rows[index];
                        var taskid = selectRow.doubletaskid;
                        upLoadTask(taskid, "2");
                        task_Source.clear();
                        vectorSource.clear();
                    }
                }
            });
        }
    });
}
/**
 * 查询某审核未通过任务的要素
 * @param tskid
 */
function queryDoubleTaskById(tskid) {
    var data = {
        taskid: tskid + ""
    };
    data = JSON.stringify(data);
    $.ajax({
        type: "GET",
        url: "../geo/getAllNotAuditFeature",
        data: {data: data},
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (data) {
            //无区域坐标时不绘制
            if (!data) {
                return;
            }
            drawWktFeatures(data, 15);
        }
    });
}
/**
 * 查询某编辑任务的要素
 * @param tskid
 */
function queryTaskById(tskid) {
    var data = {
        taskId: tskid + ""
    };
    data = JSON.stringify(data);
    $.ajax({
        type: "GET",
        url: "../geo/getDataByTaskId",
        data: {data: data},
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (data) {
            //无区域坐标时不绘制
            if (!data) {
                return;
            }
            drawWktFeatures(data, 15);
        }
    });
}
/**
 * 面  new
 * 保存到数据库
 */

function savecdata(d) {
    var geodata = d.data.geom;
    var pdata = {
        taskId:globetaskid+"",
        ftype: $("#oldtype").val(),
        fname: $("#fname").val(),
        //fdes:$("#fdes").val(),
        geom: geodata
    }
    var data = JSON.stringify(pdata);

    $("#dialog").dialog("close");
    $.ajax({
        type: "POST",
        url: "../geo/saveNewNgccPolygon",
        contentType: "application/x-www-form-urlencoded",
        data: {data: data},
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function () {
            alert("保存成功！");
        }
    });
    $("#dpup").unbind("click");
}

/**
 * 保存属性信息
 *
 */

function savedesdata() {
    var pdata = {
        ftype: $("#oldtype").val(),
        fname: $("#fname").val(),
        fdes: $("#fdes").val()
    }
    var data = JSON.stringify(pdata);

    $("#dialog").dialog("close");
    $.ajax({
        type: "POST",
        url: "../geo/newPolygon",
        contentType: "application/x-www-form-urlencoded",
        data: {data: data},
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function () {
            alert("保存成功！");
        }
    });
    $("#dpup").unbind("click");
}

/**
 * 面
 * 修改
 * 保存到数据库
 */

function saveudata(d) {
    var geodata = d.data.geom;
    var pdata = {
        id: d.data.id,
        taskId: globetaskid + "",
        ftype: $("#oldtype").val(),
        fname: $("#fname").val(),
        // fdes:$("#fdes").val(),
        geom: geodata
    }
    var data = JSON.stringify(pdata);

    $("#dialog").dialog("close");
    $.ajax({
        type: "POST",
        url: "../geo/saveNgccPolygon",
        contentType: "application/x-www-form-urlencoded",
        data: {data: data},
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function () {
            alert("保存成功！");
        }
    });
    $("#dpup").unbind("click");
}
/**
 * 完成编辑tskid任务
 * @param tskid
 * @param statuscode
 */
function upLoadTask(tskid, statuscode) {
    var pdata = {
        taskId: tskid + "",
        status: statuscode
    }
    var data = JSON.stringify(pdata);
    $.ajax({
        type: "POST",
        url: "../geo/setTaskStatus",
        contentType: "application/x-www-form-urlencoded",
        data: {data: data},
        dataType: "json",
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function () {
            alert("保存成功！");
        }
    });
}
/**
 * 加载任务区
 *  **/
function getTaskGeomById(tid) {
    var data = {
        taskid: tid + "",
    };
    data = JSON.stringify(data);
    $.ajax({
        type: "POST",
        url: "../geo/getTaskGeomByID",
        contentType: "application/x-www-form-urlencoded",
        data: {data: data},
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (obj) {
            obj = JSON.parse(obj);
            drawTaskGeom(obj);
        }
    });
}
/**
 * 通过要素审核要素id 获取要素属性
 * @param id
 */
function queryFeatureAttrbuitById(id) {
    var data = {
        id: id + "",
    };
    data = JSON.stringify(data);
    $.ajax({
        type: "POST",
        url: "../geo/findByTagFeatureId",
        contentType: "application/x-www-form-urlencoded",
        data: {data: data},
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                queryEditFeatureAttrbuitById(id);
            }
            return;
        },
        success: function (obj) {
            obj = JSON.parse(obj);
            var selectCode = document.getElementById("oldtype");
            var code = obj.ftype;
            for (var i = 0; i < selectCode.options.length; i++) {
                if (selectCode.options[i].value == code) {
                    selectCode.options[i].selected = true;
                    break;
                }
            }
            if (obj.fname) {
                $("#fname").val(obj.fname);
            } else {
                $("#fname").val("无");
            }
            if (obj.reason) {
                $("#fdes").val(obj.reason);
            } else {
                $("#fdes").val("无");
            }
        }
    });
}
/**
 * 通过编辑要素id获取要素属性
 * @param id
 */
function queryEditFeatureAttrbuitById(id) {
    var data = {
        id: id + "",
    };
    data = JSON.stringify(data);
    $.ajax({
        type: "POST",
        url: "../geo/findByFeatureId",
        contentType: "application/x-www-form-urlencoded",
        data: {data: data},
        processData: true,
        error: function (req, status, error) {

            if (status == "timeout") {
                alert("请求超时，请稍后再试!！");
                return;
            } else if (status === "error") {
                alert("数据请求失败，请稍后再试!如果还未解决，请联系管理员！");
                return;
            }
            return;
        },
        success: function (obj) {
            obj = JSON.parse(obj);
            var selectCode = document.getElementById("oldtype");
            var code = obj.ftype;
            for (var i = 0; i < selectCode.options.length; i++) {
                if (selectCode.options[i].value == code) {
                    selectCode.options[i].selected = true;
                    break;
                }
            }
            if (obj.fname) {
                $("#fname").val(obj.fname);
            } else {
                $("#fname").val("无");
            }
            if (obj.reason) {
                $("#fdes").val(obj.reason);
            } else {
                $("#fdes").val("无");
            }
        }
    });
}
package com.csu.geo.directive;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;


import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
/**
 *  自定义GPS轨迹列表获取标签
 * @author zhangpan
 *
 */
public class GPSListDirective implements TemplateDirectiveModel{

	@Autowired
	//private GPSService gpsService;
	@Override
	public void execute(Environment env, @SuppressWarnings("rawtypes") Map map, TemplateModel[] loopVars, TemplateDirectiveBody body)
			throws TemplateException, IOException {
	
	}

}

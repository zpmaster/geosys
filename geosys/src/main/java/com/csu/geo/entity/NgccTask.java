package com.csu.geo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="ngcc_task")
public class NgccTask {
	
	private Integer id;
	private Integer taskId;
	private  Integer editStatus;
	private Integer auditStatus;
	private String editerName;
	private  Integer count;
	private  String auditName;
	private Integer editerId;
	private Integer auditerId;
	private String  reason;
	@Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="task_id")
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	@Column(name="edit_status")
	public Integer getEditStatus() {
		return editStatus;
	}
	public void setEditStatus(Integer editStatus) {
		this.editStatus = editStatus;
	}
	@Column(name="audit_status")
	public Integer getAuditStatus() {
		return auditStatus;
	}
	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}
	@Column(name="editer_name")
	public String getEditerName() {
		return editerName;
	}
	public void setEditerName(String editerName) {
		this.editerName = editerName;
	}
	@Column(name="count")
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	@Column(name="audit_name")
	public String getAuditName() {
		return auditName;
	}
	public void setAuditName(String auditName) {
		this.auditName = auditName;
	}
	@Column(name="editer_id")
	public Integer getEditerId() {
		return editerId;
	}
	public void setEditerId(Integer editerId) {
		this.editerId = editerId;
	}
	@Column(name="auditer_id")
	public Integer getAuditerId() {
		return auditerId;
	}
	public void setAuditerId(Integer auditerId) {
		this.auditerId = auditerId;
	}
	@Column(name="reason")
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	

}

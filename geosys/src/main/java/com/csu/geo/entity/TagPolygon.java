package com.csu.geo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Polygon;

@Entity
@Table(name="\"tag_polygon\"")
public class TagPolygon {
	
	private Integer id;
	/**
	 * 原始要素ID
	 */
	private Integer fID;
	
	private String fName;
	
	private String fType;
	private String fdes;
	
	 @Type(type="org.hibernate.spatial.JTSGeometryType")
	private Polygon geom;
	
	private String failReason;
	
	private Integer version;
	/**
	 * 审核状态，空为未审核  1 审核功过   2不通过
	 */
	private Integer status;

	private String  createDate;
	
	
	private Integer  tagUserID;
	
	
	private Integer adminUserID;
	 @Id
     @GeneratedValue(generator="increment")
     @GenericGenerator(name="increment", strategy = "increment")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="featureid")
	public Integer getFeatureID() {
		return fID;
	}

	public void setFeatureID(Integer featureID) {
		this.fID = featureID;
	}
	@Column(name="featurename")
	public String getFeatureName() {
		return fName;
	}

	public void setFeatureName(String featureName) {
		this.fName = featureName;
	}
	@Column(name="featuretype")
	public String getFeatureType() {
		return fType;
	}

	public void setFeatureType(String featureType) {
		this.fType = featureType;
	}
	
	@Column(name="failreason")
	public String getFailReason() {
		return failReason;
	}

	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	@Column(name="createdate")
	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
    
	@Column(name="taguserid")
	public Integer getTagUserID() {
		return tagUserID;
	}

	public void setTagUserID(Integer tagUserID) {
		this.tagUserID = tagUserID;
	}
	@Column(name="adminuserid")
	public Integer getAdminUserID() {
		return adminUserID;
	}

	public void setAdminUserID(Integer adminUserID) {
		this.adminUserID = adminUserID;
	}

	public Polygon getgeom() {
		return geom;
	}

	public void setgeom(Polygon geom) {
		this.geom = geom;
	}
    @Column(name="fdes")
	public String getFdes() {
		return fdes;
	}

	public void setFdes(String fdes) {
		this.fdes = fdes;
	}
	

}

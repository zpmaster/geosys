package com.csu.geo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Polygon;
/**
 * 更新后面数据实体
 * @author zhangpan
 *
 */
@Entity
@Table(name="\"ngcc_orig_polygon\"")
public class NgccNewPolygon {
	
	private Integer id;
	
	private String fName;
	private String fType;
	private Integer version;
	@Type(type="org.hibernate.spatial.JTSGeometryType")
	 private Polygon geom;
	@Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="fname")
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	@Column(name="ftype")
	public String getfType() {
		return fType;
	}
	public void setfType(String fType) {
		this.fType = fType;
	}
	@Column(name="version")
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	@Column(name="geom")
	public Polygon getGeom() {
		return geom;
	}
	public void setGeom(Polygon geom) {
		this.geom = geom;
	}
	

}

package com.csu.geo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="\"log\"")
public class Log {
	
	private Integer id;
	
	private String adminName;
	
	private String type;//点 线 面
	
	private String fname;
	private String des;
	private Integer checkStatus; 
	
	private String createTime;
	private String tagUserName;
	private String tagUserEmail;
	private String adminEmail;
	private String reason;
	private Integer version;
	
	 @Id
     @GeneratedValue(generator="increment")
     @GenericGenerator(name="increment", strategy = "increment")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
   @Column(name="adminname")
	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
    @Column(name="type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	@Column(name="fname")
	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}
	@Column(name="des")
	public String getDes() {
		return des;
	}
	
	public void setDes(String des) {
		this.des = des;
	}
	@Column(name="checkstatus")
	public Integer getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(Integer checkStatus) {
		this.checkStatus = checkStatus;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getTagUserName() {
		return tagUserName;
	}

	public void setTagUserName(String tagUserName) {
		this.tagUserName = tagUserName;
	}

	public String getTagUserEmail() {
		return tagUserEmail;
	}

	public void setTagUserEmail(String tagUserEmail) {
		this.tagUserEmail = tagUserEmail;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
	
	

}

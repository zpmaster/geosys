package com.csu.geo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Polygon;

/**
 * 编辑增量面数据
 * @author zhangpan
 *
 */
@Entity
@Table(name="\"ngcc_tag_polygon\"")
public class NgccTagPolygon {

	private Integer id;
	private Integer origId;
	private String fName;
	private String fType;
	private Integer version;
	private Integer taskId;
	private  Integer editerId;
	private Integer auditerId;
	private Integer auditStatus;
	private String  reason;
	@Type(type="org.hibernate.spatial.JTSGeometryType")
	 private Polygon geom;
	@Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="origid")
	public Integer getOrigId() {
		return origId;
	}
	
	public void setOrigId(Integer origId) {
		this.origId = origId;
	}
	@Column(name="fname")
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	@Column(name="ftype")
	public String getfType() {
		return fType;
	}
	public void setfType(String fType) {
		this.fType = fType;
	}
	@Column(name="version")
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	@Column(name="taskid")
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	@Column(name="editerid")
	public Integer getEditerId() {
		return editerId;
	}
	public void setEditerId(Integer editerId) {
		this.editerId = editerId;
	}
	@Column(name="auditerid")
	public Integer getAuditerId() {
		return auditerId;
	}
	public void setAuditerId(Integer auditerId) {
		this.auditerId = auditerId;
	}
	@Column(name="auditStatus")
	public Integer getAuditStatus() {
		return auditStatus;
	}
	public void setAuditStatus(Integer auditStatus) {
		this.auditStatus = auditStatus;
	}
	@Column(name="reason")
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Column(name="geom")
	public Polygon getGeom() {
		return geom;
	}
	public void setGeom(Polygon geom) {
		this.geom = geom;
	}
	
	
	
	
}

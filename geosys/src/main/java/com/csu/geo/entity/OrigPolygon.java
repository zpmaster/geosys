package com.csu.geo.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Polygon;

@Entity
@Table(name="\"orig_polygon\"")
public class OrigPolygon implements Serializable {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	 
	 private String fName;
	 
	 private String fType;
	 
	 private String fDes;
	 @Type(type="org.hibernate.spatial.JTSGeometryType")
	 private Polygon geom;
	 
	 private Integer version;

	 @Id
     @GeneratedValue(generator="increment")
     @GenericGenerator(name="increment", strategy = "increment")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="fname")
	public String getFeatureName() {
		return fName;
	}

	public void setFeatureName(String featureName) {
		this.fName = featureName;
	}
	@Column(name="ftype")
	public String getFeatureType() {
		return fType;
	}

	public void setFeatureType(String featureType) {
		this.fType = featureType;
	}
	@Column(name="fdes")
	public String getFeatureDes() {
		return fDes;
	}

	public void setFeatureDes(String featureDes) {
		this.fDes = featureDes;
	}
	
	

	public Polygon getGeom() {
		return geom;
	}

	public void setGeom(Polygon geom) {
		this.geom = geom;
	}

	@Column(name="version")
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	 

}

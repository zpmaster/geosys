package com.csu.geo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Point;

@Entity
@Table(name="\"tag_point\"")
public class TagPoint {
	
	private Integer id;
	/**
	 * 原始要素ID
	 */
	private Integer fID;
	
	private String fName;
	
	private String fType;
	 @Type(type="org.hibernate.spatial.JTSGeometryType")
	private Point geom;
	
	private String failReason;
	
	private Integer version;
	/**
	 * 审核状态，空为未审核  1 审核功过   2不通过
	 */
	private Integer status;

	private String  createDate;
	
	
	private Integer tagUserID;
	
	private String fdes;
	
	private Integer adminUserID;
	 @Id
     @GeneratedValue(generator="increment")
     @GenericGenerator(name="increment", strategy = "increment")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="fid")
	public Integer getFeatureID() {
		return fID;
	}

	public void setFeatureID(Integer featureID) {
		this.fID = featureID;
	}
	@Column(name="fname")
	public String getFeatureName() {
		return fName;
	}

	public void setFeatureName(String fName) {
		this.fName = fName;
	}
	@Column(name="featuretype")
	public String getFeatureType() {
		return fType;
	}

	public void setFeatureType(String featureType) {
		this.fType = featureType;
	}
	
	public Point getgeom() {
		return geom;
	}

	public void setgeom(Point geom) {
		this.geom = geom;
	}

	@Column(name="failreason")
	public String getFailReason() {
		return failReason;
	}

	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	@Column(name="createdate")
	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
    @Column(name="taguserid")
	public Integer getTagUserID() {
		return tagUserID;
	}

	public void setTagUserID(Integer tagUserID) {
		this.tagUserID = tagUserID;
	}
   @Column(name="adminuserid")
	public Integer getAdminUserID() {
		return adminUserID;
	}

	public void setAdminUserID(Integer adminUserID) {
		this.adminUserID = adminUserID;
	}

	public String getFdes() {
		return fdes;
	}

	public void setFdes(String fdes) {
		this.fdes = fdes;
	}
  
	

}

package com.csu.geo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.OrigLine;
import com.csu.geo.repository.OrigLineDao;

@Component
@Transactional
public class OrigLineService {
	
	@Autowired
	private  EntityManagerFactory entityManagerFactory;
	
	@Autowired
	private OrigLineDao origLineDao;
	
	public List<OrigLine> getOrigLineList()
	{
		
		return origLineDao.getOrigLineList();
	}
	
	public Integer updateOrigLine(String fname,String ftype,String fdes,String geom,Integer id)
	{
		String queryStr="UPDATE orig_line SET fname='"+fname+"',ftype='"+ftype+"',fdes='"+fdes+"',geom='"+geom+"',version=version+1"+" WHERE id="+id;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		entityManager.getTransaction().begin();
		int flag=query.executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close();
		return flag;
	}
	
	 public String saveOrigLine(OrigLine origLine)
	 {
		 if(origLineDao.save(origLine)!=null)
		 {
			 return "0000";
		 }
		 else {
			return "0001";
		}
	 }

}

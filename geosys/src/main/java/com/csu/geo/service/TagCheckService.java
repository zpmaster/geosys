package com.csu.geo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.TagLine;
import com.csu.geo.entity.TagPoint;
import com.csu.geo.entity.TagPolygon;
import com.csu.geo.repository.TagLinedao;
import com.csu.geo.repository.TagPointDao;
import com.csu.geo.repository.TagPolygonDao;

@Component
@Transactional
public class TagCheckService {
	
	
	@Autowired
	private TagPointDao tagPointDao;
	
	@Autowired 
	TagLinedao tagLinedao;
	
	@Autowired 
	TagPolygonDao tagPolygonDao;
	
	public Map<String, List<Map<String, String>>> getUnCheckTagData()
	{
		List<TagPoint> points=tagPointDao.findTagPointByUnCheck();
		List<Map<String, String>> pointMap=pointListToMap(points);
		List<TagLine> lines=tagLinedao.findTagLineByUnCheck();
		List<Map<String, String>> lineMap=lineListToMap(lines);
		List<TagPolygon> polygons=tagPolygonDao.findTagPolygonByUnCheck();
		List<Map<String, String>> polygonMap=polygonListToMap(polygons);
		Map<String, List<Map<String, String>>> resultMap=new HashMap<String, List<Map<String, String>>>();
		resultMap.put("point", pointMap);
		resultMap.put("line", lineMap);
		resultMap.put("polygon", polygonMap);
		return resultMap;
	}
	
	public List<Map<String, String>> pointListToMap(List<TagPoint> pointList)
	{
		
		List<Map<String, String>> resultList=new ArrayList<Map<String,String>>(); 
		int size=pointList.size();
		for(int i=0;i<size;i++)
		{
			TagPoint tagPoint=pointList.get(i);
			Map<String,String> tempMap=new HashMap<String, String>();
			tempMap.put("mark", "Point");
			tempMap.put("id", tagPoint.getId().toString());
			tempMap.put("taguserid", tagPoint.getTagUserID().toString());
			tempMap.put("fid", tagPoint.getId().toString());
			tempMap.put("ftype", tagPoint.getFeatureType());
			tempMap.put("fname", tagPoint.getFeatureName());
			tempMap.put("fdes", tagPoint.getFdes());
			tempMap.put("status", tagPoint.getStatus().toString());
			tempMap.put("geom", tagPoint.getgeom().toString());
			resultList.add(tempMap);
		}
		return resultList;
	}
	public List<Map<String, String>> lineListToMap(List<TagLine> lineList)
	{
		
		List<Map<String, String>> resultList=new ArrayList<Map<String,String>>(); 
		int size=lineList.size();
		for(int i=0;i<size;i++)
		{
			TagLine tagLine=lineList.get(i);
			Map<String,String> tempMap=new HashMap<String, String>();
			tempMap.put("mark", "Line");
			tempMap.put("id", tagLine.getId().toString());
			tempMap.put("taguserid", tagLine.getTagUserID().toString());
			tempMap.put("fid",tagLine.getFeatureID().toString());
			tempMap.put("ftype", tagLine.getFeatureType());
			tempMap.put("fdes", tagLine.getFdes());
			tempMap.put("fname", tagLine.getFeatureName());
			tempMap.put("status", tagLine.getStatus().toString());
			tempMap.put("geom", tagLine.getgeom().toString());
			resultList.add(tempMap);
		}
		return resultList;
	}

	public List<Map<String, String>> polygonListToMap(List<TagPolygon> polygonList)
	{
		
		List<Map<String, String>> resultList=new ArrayList<Map<String,String>>(); 
		int size=polygonList.size();
		for(int i=0;i<size;i++)
		{
			TagPolygon tagPolygon=polygonList.get(i);
			Map<String,String> tempMap=new HashMap<String, String>();
			tempMap.put("mark", "Polygon");
			tempMap.put("id", tagPolygon.getId().toString());
			tempMap.put("taguserid", tagPolygon.getTagUserID().toString());
			tempMap.put("fid", tagPolygon.getFeatureID().toString());
			tempMap.put("ftype", tagPolygon.getFeatureType());
			tempMap.put("fdes", tagPolygon.getFdes());
			tempMap.put("fname", tagPolygon.getFeatureName());
			tempMap.put("status", tagPolygon.getStatus().toString());
			tempMap.put("geom", tagPolygon.getgeom().toString());
			resultList.add(tempMap);
		}
		return resultList;
	}
	 

}

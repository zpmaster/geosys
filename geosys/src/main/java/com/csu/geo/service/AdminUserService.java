package com.csu.geo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.AdminUser;
import com.csu.geo.repository.AdminDao;
@Component
@Transactional
public class AdminUserService {
	
	@Autowired
	private AdminDao adminDao;
	@Autowired
	private  EntityManagerFactory entityManagerFactory;
	
	
	public String backLogin(String name,String password)
	{
		AdminUser adminUser=adminDao.findByUserNameAndPassword(name, password);
		if(adminUser!=null&&adminUser.getAuthority()==1)
		{
			return "0000";
		}
		else if(adminUser!=null&&adminUser.getAuthority()==2)
		{
			return "0002";
		}
		else {
			return "0001";
		}
	}
	
	public Integer saveAdminUser(AdminUser adminUser)
	{
		if(adminDao.save(adminUser)!=null)
		{
			return 1;
		}
		else {
			return 0;
		}
	}
	
	public List<AdminUser> getAllAdminUser()
	{
		return adminDao.getAllAdminUser();
	}
    
	public Long getAdminUserCount()
	{
		return adminDao.count();
	}
	public List<AdminUser> getAdminUserByName(String name)
	{
		return adminDao.getAdminUserByName(name);
	}
	public List<AdminUser> getAdminUserByAuthority(Integer authority)
	{
		return adminDao.getAdminUserByAuthority(authority);
	}
	public List<AdminUser> getAdminUserByAuthorityAndUserName(Integer authority,String name)
	{
		return adminDao.getAdminUserByAuthorityAndUserName(authority, name);
	}
	
	public Integer deleteAdminUser(Integer id)
	{
		//tagUserDao.deleteTagUser(id);
		String queryStr="DELETE FROM admin_user WHERE id="+id;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
	}
	
	public Integer updateAdminUserAuth(Integer authority,Integer id)
	{
		//tagUserDao.tagUserActive(status, id);
		String queryStr="UPDATE admin_user SET  authority="+authority+" WHERE id="+id;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
	}
	
	public AdminUser getAdminUser(String name)
	{
		return adminDao.getAdminUser(name);
	}
	
	public Integer updateAdminUser(String email,String tel,String password,String professional,String academic ,String name)
	{
		String queryStr="UPDATE admin_user SET email='"+email+"',tel='"+tel+"',password='"+password+"',professional='"+professional+
				"',academic='"+academic+"' WHERE username='"+name+"'";
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
	}
}

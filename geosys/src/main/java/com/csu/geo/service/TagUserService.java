package com.csu.geo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.TagUser;
import com.csu.geo.repository.TagUserDao;

@Component
@Transactional
public class TagUserService {
	
	@Autowired
	private TagUserDao tagUserDao;
	
	@Autowired
	private  EntityManagerFactory entityManagerFactory;
	
	public String saveTagUser(TagUser user)
	{
		if(tagUserDao.save(user)!=null)
			return "login";
		else {
			return "register";
		}
	}
	
	public TagUser findTagUserByNameAndPass(String name,String pass)
	{
		return tagUserDao.findByNameAndPass(name, pass);
	}
    
	public TagUser findTagUserByName(String userName)
	{
		return tagUserDao.findUserInfoByUserName(userName);
	}
	public Integer updateTagUser(String email,String tel,String password,String username,String address,String diploma,String professional )
	{
		//return tagUserDao.updateUserInfo(email, tel, password, username);
		String queryStr="UPDATE tag_user SET password='"+password+"',email='"+email+"',tel='"+tel+"',address='"+address+"',diploma='"+diploma+"',professional='"+professional+"' WHERE username='"+username+"'";
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
				
	}
	
	public Integer findUserID(String name)
	{
		return tagUserDao.findUserID(name);
	}
	
	public List<TagUser> getAllTagUser()
	{
		return tagUserDao.getAllTagUser();
	}
	
	public Long getTagUserCount()
	{
		return tagUserDao.count();
	}
	
	public List<TagUser> geTagUsersByTagUserName(String userName)
	{
		return tagUserDao.geTagUsersByTagUserName(userName);
	}
	public List<TagUser> getTagUsersByActiveStatus(Integer activeStatus)
	{
		return tagUserDao.getTagUsersByActiveStatus(activeStatus);
	}
	
	public List<TagUser> getTagUsersByActiveStatusAndName(Integer activeStatus,String userName)
	{
		return tagUserDao.getTagUsersByActiveStatusAndName(activeStatus, userName);
	}
	
	public Integer deleteTagUser(Integer id)
	{
		//tagUserDao.deleteTagUser(id);
		String queryStr="DELETE FROM tag_user WHERE id="+id;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
	}
	
	public Integer updateTagUserStatus(Integer status,Integer id)
	{
		//tagUserDao.tagUserActive(status, id);
		String queryStr="UPDATE tag_user SET  activestatus="+status+" WHERE id="+id;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
	}
	
	public TagUser geTagUserById(Integer id)
	{
		return tagUserDao.geTagUserById(id);
	}
	
	 public TagUser findUserInfoByUserName(String userName)
	 {
		 return  tagUserDao.findUserInfoByUserName(userName);
	 }
	 
	
}

package com.csu.geo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.TagLine;
import com.csu.geo.entity.TagPoint;
import com.csu.geo.entity.TagPolygon;
import com.csu.geo.repository.TagLinedao;
import com.csu.geo.repository.TagPointDao;
import com.csu.geo.repository.TagPolygonDao;
import com.fasterxml.jackson.core.JsonProcessingException;
@Component
@Transactional
public class HistoryTagService {
	
	@Autowired
	private TagPointDao tagPointDao;
	
	@Autowired TagLinedao tagLinedao;
	
	@Autowired TagPolygonDao tagPolygonDao;
	
	/**
	 * 查询该用户下的
	 * @param id
	 * @return
	 * @throws JsonProcessingException 
	 */
	public Map<String, List<Map<String, String>>> getTagHistory(Integer id,String fType,Integer status) throws JsonProcessingException
	{
		
		if("all".equals(fType)&&status==0)
		{//查询所有数据
			List<TagLine> lineLists=tagLinedao.findTagLinenByUserID(id);
			List<Map<String, String>> lineMap=lineListToMap(lineLists);
			List<TagPoint> pointLists=tagPointDao.findTagPointByUserID(id);
			List<Map<String, String>> pointMap=pointListToMap(pointLists);
			List<TagPolygon> polygonLists=tagPolygonDao.findTagPolygonByUserID(id);
			List<Map<String, String>> polygonMap=polygonListToMap(polygonLists);
			Map<String, List<Map<String, String>>> resultMap=new HashMap<String, List<Map<String, String>>>();
			resultMap.put("point", pointMap);
			resultMap.put("line", lineMap);
			resultMap.put("polygon", polygonMap);
			return resultMap;
			
		} else if(fType!="all"&&status==0)
		{//只选择一个类型条件
			switch (fType) {
			case "Point":
				List<TagPoint> pointLists=tagPointDao.findTagPointByUserID(id);
				List<Map<String, String>> pointMap=pointListToMap(pointLists);
				Map<String, List<Map<String, String>>> resultMap=new HashMap<String, List<Map<String, String>>>();
				resultMap.put("point", pointMap);
				return resultMap;
			case "Line":
				List<TagLine> lineLists=tagLinedao.findTagLinenByUserID(id);
				List<Map<String, String>> lineMap=lineListToMap(lineLists);
				Map<String, List<Map<String, String>>> resultMap1=new HashMap<String, List<Map<String, String>>>();
				resultMap1.put("line", lineMap);
				return resultMap1;
				
			case "Polygon":
				
				List<TagPolygon> polygonLists=tagPolygonDao.findTagPolygonByUserID(id);
				List<Map<String, String>> polygonMap=polygonListToMap(polygonLists);
				Map<String, List<Map<String, String>>> resultMap2=new HashMap<String, List<Map<String, String>>>();
				resultMap2.put("polygon",polygonMap);
				return resultMap2;
			}
		}else if(fType.equals("all")&&status!=0)
		{//只选择审核状态
			
			List<TagLine> lineLists=tagLinedao.findTagLineByStatus(id, status);
			List<Map<String, String>> lineMap=lineListToMap(lineLists);
			List<TagPoint> pointLists=tagPointDao.findTagPointByStatus(id, status);
			List<Map<String, String>> pointMap=pointListToMap(pointLists);
			List<TagPolygon> polygonLists=tagPolygonDao.findTagPolygonByStatus(id, status);
			List<Map<String, String>> polygonMap=polygonListToMap(polygonLists);
			Map<String, List<Map<String, String>>> resultMap=new HashMap<String, List<Map<String, String>>>();
			resultMap.put("point", pointMap);
			resultMap.put("line", lineMap);
			resultMap.put("polygon", polygonMap);
			return resultMap;
			
		} else if(fType!="all"&&status!=0)
		{
			
			switch (fType) {
			case "Point":
				List<TagPoint> pointLists=tagPointDao.findTagPointByStatus(id, status);
				List<Map<String, String>> pointMap=pointListToMap(pointLists);
				Map<String, List<Map<String, String>>> resultMap=new HashMap<String, List<Map<String, String>>>();
				resultMap.put("point", pointMap);
				return resultMap;
			case "Line":
				List<TagLine> lineLists=tagLinedao.findTagLineByStatus(id, status);
				List<Map<String, String>> lineMap=lineListToMap(lineLists);
				Map<String, List<Map<String, String>>> resultMap1=new HashMap<String, List<Map<String, String>>>();
				resultMap1.put("line", lineMap);
				return resultMap1;	
			case "Polygon":
				List<TagPolygon> polygonLists=tagPolygonDao.findTagPolygonByStatus(id, status);
				List<Map<String, String>> polygonMap=polygonListToMap(polygonLists);
				Map<String, List<Map<String, String>>> resultMap2=new HashMap<String, List<Map<String, String>>>();
				resultMap2.put("polygon",polygonMap);
				return resultMap2;
			}
		}
		return null;
		
	}
	
	public List<Map<String, String>> pointListToMap(List<TagPoint> pointList)
	{
		
		List<Map<String, String>> resultList=new ArrayList<Map<String,String>>(); 
		int size=pointList.size();
		for(int i=0;i<size;i++)
		{
			TagPoint tagPoint=pointList.get(i);
			Map<String,String> tempMap=new HashMap<String, String>();
			tempMap.put("id", tagPoint.getId().toString());
			tempMap.put("ftype", tagPoint.getFeatureType());
			tempMap.put("fname", tagPoint.getFeatureName());
			tempMap.put("status", tagPoint.getStatus().toString());
			tempMap.put("geom", tagPoint.getgeom().toString());
			resultList.add(tempMap);
		}
		return resultList;
	}
	public List<Map<String, String>> lineListToMap(List<TagLine> lineList)
	{
		
		List<Map<String, String>> resultList=new ArrayList<Map<String,String>>(); 
		int size=lineList.size();
		for(int i=0;i<size;i++)
		{
			TagLine tagLine=lineList.get(i);
			Map<String,String> tempMap=new HashMap<String, String>();
			tempMap.put("id", tagLine.getId().toString());
			tempMap.put("ftype", tagLine.getFeatureType());
			tempMap.put("fname", tagLine.getFeatureName());
			tempMap.put("status", tagLine.getStatus().toString());
			tempMap.put("geom", tagLine.getgeom().toString());
			resultList.add(tempMap);
		}
		return resultList;
	}

	public List<Map<String, String>> polygonListToMap(List<TagPolygon> polygonList)
	{
		
		List<Map<String, String>> resultList=new ArrayList<Map<String,String>>(); 
		int size=polygonList.size();
		for(int i=0;i<size;i++)
		{
			TagPolygon tagPolygon=polygonList.get(i);
			Map<String,String> tempMap=new HashMap<String, String>();
			tempMap.put("id", tagPolygon.getId().toString());
			tempMap.put("ftype", tagPolygon.getFeatureType());
			tempMap.put("fname", tagPolygon.getFeatureName());
			tempMap.put("status", tagPolygon.getStatus().toString());
			tempMap.put("geom", tagPolygon.getgeom().toString());
			resultList.add(tempMap);
		}
		return resultList;
	}
}

package com.csu.geo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.TagPolygon;
import com.csu.geo.repository.TagPolygonDao;

@Component
@Transactional
public class TagPolygonService {
	
	@Autowired
	private TagPolygonDao tagPolygonDao;
	@Autowired
	private  EntityManagerFactory entityManagerFactory;
	
	public String saveTagPolygon(TagPolygon tagPolygon)
	{
		if(tagPolygonDao.save(tagPolygon)!=null)
		{
			return "0000";
		}
		else {
			return "0001";
		}
	}
	
	public List<TagPolygon> findTagPolygonByUserID(Integer tagUserID)
	{
		return tagPolygonDao.findTagPolygonByUserID(tagUserID);
		
	}
	/**
	 * 统计审核数量
	 * @return
	 */
	public Map<Integer, Integer> getStatusCount(Integer tagUserId)
	
	{
		String queryStr="SELECT status ,count(status) FROM tag_polygon  WHERE taguserid="+tagUserId+" group by status";
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		 query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		 @SuppressWarnings("rawtypes")
		List list=query.getResultList();
		 Map<Integer, Integer> result=new HashMap<>();
		 for(Object object:list)
		 {
			 @SuppressWarnings("rawtypes")
			Map row=(Map) object;
			 result.put(Integer.parseInt(row.get("status").toString()),Integer.parseInt(row.get("count").toString()));
		 }
		 if(!result.containsKey(1))
		 {
			 result.put(1, 0);
		 }
		 if(!result.containsKey(2))
		 {
			 result.put(2, 0);
		 }
		 if(!result.containsKey(3))
		 {
			 result.put(3, 0);
		 }
		 entityManager.close();
		 return result;
	}
public Map<Integer, Integer> getStatusCount()
	
	{
		String queryStr="SELECT status ,count(status) FROM tag_polygon  group by status";
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		 query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		 @SuppressWarnings("rawtypes")
		List list=query.getResultList();
		 Map<Integer, Integer> result=new HashMap<>();
		 for(Object object:list)
		 {
			 @SuppressWarnings("rawtypes")
			Map row=(Map) object;
			 result.put(Integer.parseInt(row.get("status").toString()),Integer.parseInt(row.get("count").toString()));
		 }
		 if(!result.containsKey(1))
		 {
			 result.put(1, 0);
		 }
		 if(!result.containsKey(2))
		 {
			 result.put(2, 0);
		 }
		 if(!result.containsKey(3))
		 {
			 result.put(3, 0);
		 }
		 entityManager.close();
		 return result;
	}
	public List<TagPolygon> geTagPolygonUnCheck()
	{
		return tagPolygonDao.findTagPolygonByUnCheck();
	}
	
	public Integer updateTagPolygon(Integer id,Integer status,String failreason)
	{
		String queryStr="UPDATE tag_polygon SET status="+status+",failreason='"+failreason+"' WHERE id="+id;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		entityManager.getTransaction().begin();
		int flag=query.executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close();
		return flag;
	}
}

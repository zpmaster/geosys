package com.csu.geo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.OrigPoint;
import com.csu.geo.repository.OrigPointDao;

@Component
@Transactional
public class OrigPointService {
	
	@Autowired
	private OrigPointDao origPointDao;
	@Autowired
	private  EntityManagerFactory entityManagerFactory;
	
	public List<OrigPoint> findAll()
	{
		return origPointDao.getOrigPointnList();
	}
	
	public Integer updateOrigPoint(String fname,String ftype,String fdes,String geom,Integer id)
	{
		String queryStr="UPDATE orig_point SET fname='"+fname+"',ftype='"+ftype+"',fdes='"+fdes+"',geom='"+geom+"',version=version+1"+" WHERE id="+id;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		entityManager.getTransaction().begin();
		int flag=query.executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close();
		return flag;
	}
     public String saveOrigPoint(OrigPoint origPoint)
     {
    	 if(origPointDao.save(origPoint)!=null)
    	 {
    		 return "0000";
    	 }
    	 else {
			return "0001";
		}
     }
}

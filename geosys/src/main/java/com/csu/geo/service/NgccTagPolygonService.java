package com.csu.geo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.apache.log4j.nt.NTEventLogAppender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.NgccTagPolygon;
import com.csu.geo.repository.NgccTagPolygonDao;

@Component
@Transactional
public class NgccTagPolygonService {
	
	@Autowired
	private NgccTagPolygonDao ngccTagPolygonDao;
	@Autowired
	private  EntityManagerFactory entityManagerFactory;
	
	
	/**
	 * 更新编辑数据审核状态
	 * @param ngccTagPolygon
	 * @return
	 */
	public  Integer updateAuditStatus(NgccTagPolygon ngccTagPolygon)
	{
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		String sql="UPDATE NgccTagPolygon tag SET tag.auditStatus=:auditStatus,tag.reason=:reason WHERE tag.id=:id";
		Query query=entityManager.createQuery(sql);
		query.setParameter("auditStatus", ngccTagPolygon.getAuditStatus()).
		setParameter("reason",ngccTagPolygon.getReason()).
		setParameter("id", ngccTagPolygon.getId());
		entityManager.getTransaction().begin();
		int flag=query.executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close();
		return flag;
	}
	
    public String save(NgccTagPolygon ngccTagPolygon)
    {
    	if(ngccTagPolygonDao.save(ngccTagPolygon)!=null)
    	{
    		return "0000";
    	}
    	else {
			return "0001";
		}
    }
    
    public List<NgccTagPolygon> geNgccTagPolygonsByTaskId(Integer taskId)
    {
        return  ngccTagPolygonDao.getByTaskId(taskId);
    }
    
    public NgccTagPolygon findById(Integer id)
    {
    	return ngccTagPolygonDao.findById(id);
    }
    
    public List<NgccTagPolygon> findByTaskIdAndStatus(Integer taskId,Integer status)
    {
    	return ngccTagPolygonDao.findByTaskIdAndAuditStatus(taskId, status);
    }
    public Integer updateByNewPolygon(NgccTagPolygon ngccTagPolygon)
    {
    	EntityManager entityManager=entityManagerFactory.createEntityManager();
		String sql="UPDATE NgccTagPolygon tag SET tag.fName=:fname,tag.fType=:ftype,tag.geom=:geom,tag.auditStatus=:auditStatus WHERE tag.id=:id";
		Query query=entityManager.createQuery(sql);
		query.setParameter("auditStatus", ngccTagPolygon.getAuditStatus()).
		setParameter("fname",ngccTagPolygon.getfName()).
		setParameter("ftype", ngccTagPolygon.getfType()).
		setParameter("geom", ngccTagPolygon.getGeom()).
		setParameter("id", ngccTagPolygon.getId());
		entityManager.getTransaction().begin();
		int flag=query.executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close();
		return flag;
    }
}

package com.csu.geo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.NgccGrid;
import com.csu.geo.repository.NgccGridDao;

@Component
@Transactional
public class NgccGridService {
	
	@Autowired
	private NgccGridDao ngccGridDao;
	
    public NgccGrid save(NgccGrid ngccGrid)
    {
    	 return ngccGridDao.save(ngccGrid);
    }
	
    public List<NgccGrid> getAll()
    {
    	return ngccGridDao.getAllGrid();
    }
    
    public List<NgccGrid> getGridByid(int uuid){
		return ngccGridDao.getGridByUuid(uuid);
    	
    }
   
}

package com.csu.geo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder.In;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.NgccTask;
import com.csu.geo.repository.NgccTaskDao;

@Component
@Transactional
public class NgccTaskService {
	@Autowired
	private  EntityManagerFactory entityManagerFactory;
	@Autowired
	private NgccTaskDao ngccTaskDao;
	
	public String save(NgccTask ngccTask)
	{
		if(ngccTaskDao.save(ngccTask)!=null)
		{
			return "0000";
		}
		else {
			return "0001";
		}
	}
	
    public  List<NgccTask> findByEditerName(String editername)
    {
    	return ngccTaskDao.findByEditerName(editername);
    }
	
	public List<NgccTask> findByTaskId(Integer taskid){
		return ngccTaskDao.findByTaskId(taskid);
	}
	
	public List<NgccTask> findByEditStatus(Integer editstatus){
		return ngccTaskDao.findByEditStatus(editstatus);
	}
	public List<NgccTask> findByAuditName(String auditname){
		return ngccTaskDao.findByAuditName(auditname);
	}
	
    /**
     * 任务的状态就用edit_statu全权负责
     * @param status
     * @param taskId
     * @return
     */
	public Integer updateStatus(Integer status,Integer taskId,String reason)
	{
		String queryStr="UPDATE ngcc_task SET  edit_status="+status+",reason='"+reason+"' WHERE task_id="+taskId;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
	}
	 /**
     * 更新编辑员信息
     * @param status
     * @param taskId
     * @return
     */
	public Integer updateEditerId(Integer editerId,String editerName,Integer taskId)
	{
		String queryStr="UPDATE ngcc_task SET  editer_id="+editerId+",editer_name='"+editerName+"' WHERE task_id="+taskId;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
	}
	 /**
     * 更新审核员任务信息
     * @param status
     * @param taskId
     * @return
     */
	public Integer updateAuditerId(Integer auditerId,String auditerName,Integer taskId)
	{
		String queryStr="UPDATE ngcc_task SET  edit_status= 2,auditer_id="+auditerId+",audit_name='"+auditerName+"' WHERE task_id="+taskId;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		//query.executeUpdate();
		entityManager.getTransaction().begin();
	  int flag= query.executeUpdate();
	  entityManager.getTransaction().commit();
	  entityManager.close();
	  return flag;
	}
	
	public List<NgccTask> findByEditerNameAndStatus(String editerName,Integer editStatus)
	{
		return ngccTaskDao.findByEditerNameAndEditStatus(editerName, editStatus);
	}
}

package com.csu.geo.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.csu.geo.entity.OrigPolygon;
import com.csu.geo.repository.OrigPolygonDao;

@Component
@Transactional
public class OrigPolygonService {
	
	@Autowired
	private  OrigPolygonDao origPolygonDao;
	@Autowired
	private  EntityManagerFactory entityManagerFactory;
	
	public List<OrigPolygon> findAll()
	{
		return origPolygonDao.getOrigPolygonList();
	}
	
	public Integer updateOrigPolygon(String fname,String ftype,String fdes,String geom,Integer id)
	{
		String queryStr="UPDATE orig_polygon SET fname='"+fname+"',ftype='"+ftype+"',fdes='"+fdes+"',geom='"+geom+"',version=version+1"+" WHERE id="+id;
		EntityManager entityManager=entityManagerFactory.createEntityManager();
		Query query=entityManager.createNativeQuery(queryStr);
		entityManager.getTransaction().begin();
		int flag=query.executeUpdate();
		entityManager.getTransaction().commit();
		entityManager.close();
		return flag;
	}
	
	public String saveOrigPolygon(OrigPolygon origPolygon)
	{
		if(origPolygonDao.save(origPolygon)!=null)
		{
			return "0000";
		}
		else {
			return "0001";
		}
	}
}

package com.csu.geo.utils;

import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

public class WKTUtil {
	private static GeometryFactory geometryFactory = new GeometryFactory();;
	
	
	/**
	 * wkt 转point
	 * @param pointWKT
	 * @return
	 * @throws ParseException
	 */
	public static Point createPointByWKT(String pointWKT) throws ParseException
	{
		WKTReader reader=new WKTReader(geometryFactory);
		Point point=(Point) reader.read(pointWKT);
		return point;
	}
	
	/**
	 * wkt转 lineString
	 * @param lineWKT
	 * @return
	 * @throws ParseException
	 */
	public static LineString createLineByWKT(String lineWKT) throws ParseException
	{
		WKTReader reader=new WKTReader(geometryFactory);
		LineString lineString=(LineString) reader.read(lineWKT);
		return lineString;
	}
	/**
	 * wkt转 polygn
	 * @param polygonWKT
	 * @return
	 * @throws ParseException
	 */
	public static Polygon createPolygon(String polygonWKT) throws ParseException 
	{
		WKTReader reader=new WKTReader(geometryFactory);
		Polygon polygon=(Polygon) reader.read(polygonWKT);
		return polygon;
	}
	
	

}

package com.csu.geo.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Grid {

	/**
	 * 
	 * @param numL 行
	 * @param numC 列
	 * @param minLng 最小精度
	 * @param minLat  最小維度
	 * @param maxLng  最大精度
	 * @param maxLat   最大維度
	 */
	public static List<String> generateGrid(int numL,int numC, double minLng, double minLat,double maxLng,double maxLat) {
		double stepLng = (maxLng - minLng) / numC;
		double stepLat = (maxLat - minLat) / numL;
	
		String[][] polygon = new String[numL][numC];
		List polygons=new ArrayList<>();
		for (int i = 0; i < numL; i++) {
			for (int j = 0; j < numC; j++) {
				polygon[i][j] = "POLYGON";

				double pointULLng;
				double pointULLat;

				double pointURLng;
				double pointURLat;

				double pointLLLng;
				double pointLLLat;

				double pointLRLng;
				double pointLRLat;

				pointULLng = minLng + j * stepLng;
				pointULLat = maxLat - i * stepLat;

				pointURLng = minLng + (j + 1) * stepLng;
				pointURLat = maxLat - i * stepLat;

				pointLLLng = minLng + j * stepLng;
				pointLLLat = maxLat - (i + 1) * stepLat;

				pointLRLng = minLng + (j + 1) * stepLng;
				pointLRLat = maxLat - (i + 1) * stepLat;

				/**
				 * 
				 */
				polygon[i][j]+="((";
				
				polygon[i][j] += String.valueOf(pointULLng);
				polygon[i][j] +=" ";
				polygon[i][j] += String.valueOf(pointULLat);
				polygon[i][j] +=",";
				
				polygon[i][j] += String.valueOf(pointURLng);
				polygon[i][j] +=" ";
				polygon[i][j] += String.valueOf(pointURLat);
				polygon[i][j] +=",";
				
				polygon[i][j] += String.valueOf(pointLRLng);
				polygon[i][j] +=" ";
				polygon[i][j] += String.valueOf(pointLRLat);
				polygon[i][j] +=",";
				
				polygon[i][j] += String.valueOf(pointLLLng);
				polygon[i][j] +=" ";
				polygon[i][j] += String.valueOf(pointLLLat);
				polygon[i][j] +=",";
				
				polygon[i][j] += String.valueOf(pointULLng);
				polygon[i][j] +=" ";
				polygon[i][j] += String.valueOf(pointULLat);
				
				
				polygon[i][j]+="))";
				
				System.out.println(polygon[i][j]);
				polygons.add(polygon[i][j]);
			}
			
			
		}
		return polygons;
	}
	
	//	public static void main(String[] arg){
	//		List<String> tt=generateGrid(2, 3, 116.43, 39.917, 116.469,39.945);
	//		System.out.println("ok");
	//	}
}

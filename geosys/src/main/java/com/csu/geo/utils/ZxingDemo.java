package com.csu.geo.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

public class ZxingDemo {

	public static void main(String[] args) throws WriterException, IOException {
		// TODO Auto-generated method stub
		String content = "http://192.168.1.255:8082/file/2016.docx";
	     String path = "/Users/zhangpan/Downloads/";
	     
	     MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
	     
	     Map<EncodeHintType, String> hints = new HashMap<EncodeHintType, String>();
	     hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
	     BitMatrix bitMatrix = multiFormatWriter.encode(content, BarcodeFormat.QR_CODE, 400, 400,hints);
	     File file1 = new File(path,"biaobao.jpg");
	     MatrixToImageWriter.writeToFile(bitMatrix, "jpg", file1);
	}

}

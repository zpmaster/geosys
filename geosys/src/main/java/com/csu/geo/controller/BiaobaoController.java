package com.csu.geo.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 *  标报控制器
 * @author zhangpan
 *
 */
@Controller
public class BiaobaoController {

	private static final Logger logger=LoggerFactory.getLogger(BiaobaoController.class);
	
	@RequestMapping(value="/biaobao")
	public ModelAndView  task(HttpSession session)
	{
		if(session.getAttribute("user")!=null)
	{
		ModelAndView view =new ModelAndView("biaobao");
		return view;
	}
		else {
			ModelAndView view =new ModelAndView("login");
			return view;
		}
	}
	
}

package com.csu.geo.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.csu.geo.entity.TagPolygon;
import com.csu.geo.service.TagPolygonService;
import com.csu.geo.service.TagUserService;
import com.csu.geo.utils.WKTUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.ParseException;

@Controller
public class TagPolygonController {
	
	@Autowired
	private TagPolygonService tagPolygonService;
	@Autowired
	private TagUserService tagUserService;
	private static final Logger logger=LoggerFactory.getLogger(TagPolygonController.class);
	/**
	 * 修改多边形保存
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 * @throws ParseException
	 */
	@RequestMapping(value = "/savePolygon")
	public void saveTagPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException
	{
	//String user=session.getAttribute("user").toString();
		if(session.getAttribute("user")==null)
		{
			response.sendRedirect("login");
		}
		else {
			String user=session.getAttribute("user").toString();
			String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
			
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			TagPolygon tagPolygon=new TagPolygon();
			tagPolygon.setFeatureID(Integer.valueOf(map.get("id")));
			tagPolygon.setFeatureName(map.get("fname"));
			tagPolygon.setFeatureType(map.get("ftype"));
			tagPolygon.setFdes(map.get("fdes"));
			Polygon polygon=WKTUtil.createPolygon(map.get("geom"));
			tagPolygon.setgeom(polygon);
			tagPolygon.setStatus(3);
			tagPolygon.setTagUserID(tagUserService.findUserID(user));
			 SimpleDateFormat sd=new SimpleDateFormat("yyyyMMDDHHmmss");
			 String createDate=sd.format(new Date());
			tagPolygon.setCreateDate(createDate);
			String result=tagPolygonService.saveTagPolygon(tagPolygon);
			logger.info("------------------ 修改多边形保存成功-------------------------");
			response.getWriter().println(result);
		}
	}
	
	/**
	 * 新建多边形保存
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 * @throws ParseException
	 */
	@RequestMapping(value = "/newPolygon")
	public void newTagPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException
	{
	//String user=session.getAttribute("user").toString();
		if(session.getAttribute("user")==null)
		{
			response.sendRedirect("login");
		}
		else {
			String user=session.getAttribute("user").toString();
			String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			TagPolygon tagPolygon=new TagPolygon();
			
			tagPolygon.setFeatureName(map.get("fname"));
			tagPolygon.setFeatureType(map.get("ftype"));
			tagPolygon.setFdes(map.get("fdes"));
			Polygon polygon=WKTUtil.createPolygon(map.get("geom"));
			tagPolygon.setgeom(polygon);
			tagPolygon.setFeatureID(0);
			tagPolygon.setStatus(3);
			tagPolygon.setTagUserID(tagUserService.findUserID(user));
			 SimpleDateFormat sd=new SimpleDateFormat("yyyyMMDDHHmmss");
			 String createDate=sd.format(new Date());
			tagPolygon.setCreateDate(createDate);
			String result=tagPolygonService.saveTagPolygon(tagPolygon);
			logger.info("--------------新建面数据保存成功----------------");
			response.getWriter().println(result);
		}
	}
	
	
	@RequestMapping(value="/findTagPolygon")
	public void  findTagPolygon(HttpSession session,HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		//String user=session.getAttribute("user").toString();
		if(session.getAttribute("user")==null)
		{
			response.sendRedirect("login");
		}
		else {
			String user=session.getAttribute("user").toString();
			Integer userID=tagUserService.findUserID(user);
			List<TagPolygon> tagPolygons=tagPolygonService.findTagPolygonByUserID(userID);
			List<Map<String, String>> result=new ArrayList<Map<String,String>>();
			int size=tagPolygons.size();
			for(int i=0;i<size;i++)
			{
				TagPolygon tagPolygon=tagPolygons.get(i);
				Map<String, String> tempMap=new HashMap<String, String>();
				tempMap.put("id", tagPolygon.getId().toString());
				tempMap.put("fname", tagPolygon.getFeatureName());
				tempMap.put("ftype", tagPolygon.getFeatureType());
				tempMap.put("fdes", tagPolygon.getFdes());
				tempMap.put("geom", tagPolygon.getgeom().toString());
				result.add(tempMap);	
			}
			ObjectMapper mapper=new ObjectMapper();
			String data=new String(mapper.writeValueAsString(result).getBytes("UTF-8"),"iso-8859-1");
			response.getWriter().println(data);
		}
		
		
	}
	
}

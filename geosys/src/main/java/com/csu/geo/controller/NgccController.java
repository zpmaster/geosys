package com.csu.geo.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.ws.http.HTTPBinding;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.csu.geo.entity.AdminUser;
import com.csu.geo.entity.NgccGrid;
import com.csu.geo.entity.NgccOrigPolygon;
import com.csu.geo.entity.NgccTagPolygon;
import com.csu.geo.entity.NgccTask;
import com.csu.geo.entity.TagUser;
import com.csu.geo.service.AdminUserService;
import com.csu.geo.service.NgccGridService;
import com.csu.geo.service.NgccNewPolygonService;
import com.csu.geo.service.NgccOrigPolygonService;
import com.csu.geo.service.NgccTagPolygonService;
import com.csu.geo.service.NgccTaskService;
import com.csu.geo.service.TagUserService;
import com.csu.geo.utils.Grid;
import com.csu.geo.utils.WKTUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.ParseException;

/**
 * 处理任务分配
 * @author zhangpan
 *
 */
@Controller
public class NgccController {
	private static final Logger logger = LoggerFactory.getLogger(AdminUserController.class);

	@Autowired
	private  NgccGridService ngccGridService;
	
	@Autowired
	private NgccNewPolygonService ngccNewPolygonService;
	
	@Autowired
	private NgccTagPolygonService ngccTagPolygonService;
	@Autowired
	private NgccOrigPolygonService ngccOrigPolygonService;
	@Autowired
	private NgccTaskService ngccTaskService;
	@Autowired
	private TagUserService tagUserService;
	@Autowired 
	private AdminUserService adminUserService;
	/**
	 * 获取编辑员任务列表
	 * @param request
	 * @param response
	 * @param session
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value="/getTaskByEditerName")
	public void getTaskByEditerName(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws JsonParseException, JsonMappingException, IOException
	{
		String userName=session.getAttribute("user").toString();
		//List<NgccTask> ngccTasks=ngccTaskService.findByEditerName(userName);
		List<NgccTask> ngccTasks=ngccTaskService.findByEditerNameAndStatus(userName, 0);
		ObjectMapper mapper=new ObjectMapper();
		String  result=new String(mapper.writeValueAsString(ngccTasks).getBytes("UTF-8"),"iso-8859-1");
		logger.info("----------------获取编辑员任务列表------------------------------");
		response.getWriter().println(result);
	}
	
	/**
	 * 获取审核员任务列表
	 * @param request
	 * @param response
	 * @param session
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value="/getTaskByAuditerName")
	public void getTaskByAuditerName(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws JsonParseException, JsonMappingException, IOException
	{
		String auditname=session.getAttribute("adminuser").toString();
		List<NgccTask> ngccTasks=ngccTaskService.findByAuditName(auditname);
		ObjectMapper mapper=new ObjectMapper();
		String  result=new String(mapper.writeValueAsString(ngccTasks).getBytes("UTF-8"),"iso-8859-1");
		logger.info("----------------获取审核员任务列表------------------------------");
		response.getWriter().println(result);
	}
	
	/**
	 * 根据tsskId获取原始编辑任务
	 * @param request
	 * @param response
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value="/getDataByTaskId")
	public void getDataByTaskId(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	{
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper=new ObjectMapper();
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class); 
		Integer taskId=Integer.parseInt(map.get("taskId"));
		List<NgccOrigPolygon> ngccOrigPolygons=ngccOrigPolygonService.findByTaskID(taskId);
		List<Map<String, String>> result= new ArrayList<>();
		for(int i=0;i<ngccOrigPolygons.size();i++)
		{
			Map<String, String> teMap=new HashMap<>();
			NgccOrigPolygon ngccOrigPolygon=ngccOrigPolygons.get(i);
			
			teMap.put("id", ngccOrigPolygon.getId().toString());
			teMap.put("fname", ngccOrigPolygon.getfName());
			teMap.put("ftype", ngccOrigPolygon.getfType());
			teMap.put("geom", ngccOrigPolygon.getGeom().toString());
	        result.add(teMap);
		}
		String  data1=new String(mapper.writeValueAsString(result).getBytes("UTF-8"),"iso-8859-1");
		logger.info("----------------根据TaskID获取编辑数据------------------------------");
		response.getWriter().println( data1);
	}
    /**
     * 保存修改的要素数据
     * @param request
     * @param response
     * @param session
     * @throws IOException 
     * @throws ParseException 
     */
	@RequestMapping(value="/saveNgccPolygon")
	public void savePolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException
	{if(session.getAttribute("user")==null)
	{
		response.sendRedirect("login");
	}
	else {
		
		String user=session.getAttribute("user").toString();
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper = new ObjectMapper();  
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		String editOrCheckTask=map.get("eorc");
		if(editOrCheckTask!=null&&"edit".equals(editOrCheckTask))
		{
		NgccTagPolygon ngccTagPolygon=new NgccTagPolygon();
		ngccTagPolygon.setfName(map.get("fname"));
		ngccTagPolygon.setfType(map.get("ftype"));
		ngccTagPolygon.setAuditStatus(3);
		ngccTagPolygon.setOrigId(Integer.parseInt(map.get("id")));
		Polygon polygon=WKTUtil.createPolygon(map.get("geom"));
		ngccTagPolygon.setGeom(polygon);
		ngccTagPolygon.setTaskId(Integer.parseInt(map.get("taskId")));
		ngccTagPolygon.setEditerId(tagUserService.findUserID(user));
		String result=ngccTagPolygonService.save(ngccTagPolygon);
		logger.info("------------NGCC保存面数据数据修改成功--------------------");

		response.getWriter().println(result);
		}else if(editOrCheckTask!=null&&"check".equals(editOrCheckTask)){
		    Integer id=Integer.parseInt(map.get("id"));
			NgccTagPolygon ngccTagPolygon=ngccTagPolygonService.findById(id);
			ngccTagPolygon.setfName(map.get("fname"));
			ngccTagPolygon.setfType(map.get("ftype"));
			ngccTagPolygon.setAuditStatus(3);
			Polygon polygon=WKTUtil.createPolygon(map.get("geom"));
			ngccTagPolygon.setGeom(polygon);
			Integer  result=ngccTagPolygonService.updateAuditStatus(ngccTagPolygon);
			logger.info("------------NGCC保存面数据数据修改成功--------------------");

			response.getWriter().println(result.toString());
		}
		
		
	}
		
	}
	 /**
     * 保存新建的要素数据
     * @param request
     * @param response
     * @param session
     * @throws IOException 
     * @throws ParseException 
     */
	@RequestMapping(value="/saveNewNgccPolygon")
	public void saveNewPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException
	{if(session.getAttribute("user")==null)
	{
		response.sendRedirect("login");
	}
	else {
		String user=session.getAttribute("user").toString();
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper = new ObjectMapper();  
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		
		NgccTagPolygon ngccTagPolygon=new NgccTagPolygon();
		ngccTagPolygon.setfName(map.get("fname"));
		ngccTagPolygon.setfType(map.get("ftype"));
		ngccTagPolygon.setAuditStatus(3);
		ngccTagPolygon.setOrigId(0);
		Polygon polygon=WKTUtil.createPolygon(map.get("geom"));
		ngccTagPolygon.setGeom(polygon);
		ngccTagPolygon.setTaskId(Integer.parseInt(map.get("taskId")));
		ngccTagPolygon.setEditerId(tagUserService.findUserID(user));
		String result=ngccTagPolygonService.save(ngccTagPolygon);
		logger.info("------------NGCC保存面数据数据修改成功--------------------");

		response.getWriter().println(result);
		
	}
		
	}
	/**
	 * 0:未编辑状态，1:编辑完成状态；2:已分配审核；3:完成审核状态;4：审核不通过
	 * @param request
	 * @param response
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value="setTaskStatus")
	public  void setTaskStatus(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	{
		//String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		String data =request.getParameter("data");
		ObjectMapper mapper = new ObjectMapper();  
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		Integer status=Integer.parseInt(map.get("status"));
		Integer taskId=Integer.parseInt(map.get("taskId"));
		String  reason=map.get("reason");
		int flag=ngccTaskService.updateStatus(status, taskId,reason);
		if(flag>0)
		{
			response.getWriter().println("0000");
		}
		else {
			response.getWriter().println("0001");
		}
	}
	
	/**
	 * 获取所有原始数据
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value="/getAllNgccOrigPolygon")
	public void getAllNgccOrigPolygon(HttpServletResponse response) throws IOException
	{
		ObjectMapper mapper=new ObjectMapper();
		List<NgccOrigPolygon> ngccOrigPolygons=ngccOrigPolygonService.findAll();
		List<Map<String, String>> result=new ArrayList<>();
		int size=ngccOrigPolygons.size();
		for(int i=0;i<size;i++)
		{
			NgccOrigPolygon ngccOrigPolygon=ngccOrigPolygons.get(i);
			Map<String, String> tempMap=new HashMap<>();
			tempMap.put("geom", ngccOrigPolygon.getGeom().toString());
			result.add(tempMap);
		}
		String  data=mapper.writeValueAsString(result);
		logger.info("----------------获取编辑数据------------------------------");
		response.getWriter().println(data);
	}
	
	/**
	 * 通过参数来分配任务，
	 * @param response
	 * @param request
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws ParseException 
	 */
	@RequestMapping(value="/getGridParm")
	public void getGridParm(HttpServletResponse response,HttpServletRequest request) throws JsonParseException, JsonMappingException, IOException, ParseException
	{
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper = new ObjectMapper();  
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		Integer numL=Integer.parseInt(map.get("numl"));
		Integer numC=Integer.parseInt(map.get("numc"));
		Double maxLat=Double.parseDouble(map.get("maxLat"));
		Double minLat=Double.parseDouble(map.get("minLat"));
		Double maxLon=Double.parseDouble(map.get("maxLon"));
		Double minLon=Double.parseDouble(map.get("minLon"));
		//1:通过数据获取网格  2:将数据进行任务分配
		List<String> grid=Grid.generateGrid(numL, numC, minLon, minLat, maxLon, maxLat);
		List<Polygon> taskGrid=new ArrayList<>();
		for(int i=0;i<grid.size();i++)
		{
			NgccGrid ngccGrid=new NgccGrid();
			ngccGrid.setUuid(i+1);
			ngccGrid.setValue(grid.get(i));
			//保存格网信息
			ngccGridService.save(ngccGrid);
			NgccTask ngccTask=new NgccTask();
			ngccTask.setTaskId(i+1);
			ngccTask.setEditStatus(0);
			//保存任务数据
			ngccTaskService.save(ngccTask);
			Polygon polygon=WKTUtil.createPolygon(grid.get(i));
			taskGrid.add(polygon);
		}
		List<NgccOrigPolygon> allPolygon=ngccOrigPolygonService.findAll();
		int size=allPolygon.size();
		int taskSize=taskGrid.size();
		for(int i=0;i<size;i++)
		{
			Polygon taskPolygon=allPolygon.get(i).getGeom();
			for(int j=0;j<taskSize;j++)
			{
				Polygon gridPolygon=taskGrid.get(j);
				if(taskPolygon.within(gridPolygon)||taskPolygon.intersects(gridPolygon))
				{
					NgccOrigPolygon ngccOrigPolygon=allPolygon.get(i);
					ngccOrigPolygon.setTaskId(j+1);
					ngccOrigPolygonService.setTaskID(ngccOrigPolygon);
					break;
				}
			}
		}
	}
	
	/**
	 * 获取网格任务数据
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value="/getTask")
	public  void getGridResult(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		List<NgccGrid> result=ngccGridService.getAll();
		List<NgccGrid> newResult=new ArrayList<>();
		for(int i=0;i<result.size();i++)
		{
			NgccGrid ngccGrid=result.get(i);
			NgccTask ngccTask=ngccTaskService.findByTaskId(ngccGrid.getUuid()).get(0);
			if(ngccTask.getEditStatus()==0||ngccTask.getEditStatus()==4)
			{
				newResult.add(ngccGrid);
			}
		}
		ObjectMapper mapper=new ObjectMapper();
		String data=mapper.writeValueAsString(newResult);
		response.getWriter().println(data);
	}
	
	/**
	 * 转到任务分配页面
	 * @return
	 */
	@RequestMapping(value="/admintasklist")
	public ModelAndView tagCheckList()
	{
		ModelAndView modelAndView=new ModelAndView("admintasklist");
		return modelAndView;
	}
	@RequestMapping(value="/adminchecktasklist")
	public ModelAndView adminchecktasklist()
	{
		ModelAndView modelAndView=new ModelAndView("adminchecktasklist");
		return modelAndView;
	}
	/**
	 * 通过任务ID获取编辑任务
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@RequestMapping(value="/getEditerTaskByEditerId")
	public void getEditerTaskByEditerId(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	{
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper = new ObjectMapper();  
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		Integer taskId=Integer.parseInt(map.get("taskId"));
		List<NgccOrigPolygon> list=ngccOrigPolygonService.findByTaskID(taskId);
		List<Map<String, String>> result=new ArrayList<>();
		int size=list.size();
		
		for(int i=0;i<size;i++)
		{
			NgccOrigPolygon ngccOrigPolygon=list.get(i);
			Map<String, String> tempMap=new HashMap<>();
			tempMap.put("id", ngccOrigPolygon.getId().toString());
			tempMap.put("fname", ngccOrigPolygon.getfName());
			tempMap.put("ftype", ngccOrigPolygon.getfType());
			tempMap.put("geom", ngccOrigPolygon.getGeom().toString());
			result.add(tempMap);
		}
		String  data1=mapper.writeValueAsString(result);
		logger.info("----------------根据TaskID获取编辑数据------------------------------");
		response.getWriter().println(data1);
	}
	/**
	 * 更新任务的状态,编辑任务完成，状态变为1，
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@RequestMapping(value="/updateTaskStatus")
	public void updateTaskStatus(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	{
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper = new ObjectMapper();  
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		Integer status=Integer.parseInt(map.get("status"));
		Integer taskId=Integer.parseInt(map.get("taskId"));
		int flag=ngccTaskService.updateStatus(status, taskId,"编辑完成");
		if(flag>0)
		{
			response.getWriter().println("0000");
		}else {
			response.getWriter().println("0001");
		}
	}
	/**
	 * 分配任务给编辑员，向task数据表添加数据
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@RequestMapping(value="/setEditerIdAndName")
	public  void  setEditerIdAndName(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	{
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper = new ObjectMapper();  
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		Integer taskId=Integer.parseInt(map.get("taskId"));
		Integer editerId=Integer.parseInt(map.get("editerId"));
		String  editerName=map.get("editerName");
		int flag=ngccTaskService.updateEditerId(editerId, editerName, taskId);
		if(flag>0)
		{
			response.getWriter().println("0000");
		}else {
			response.getWriter().println("0001");
		}
	}
	
	/**
	 * 分配任务给审核员，向task数据表添加数据
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@RequestMapping(value="/setAuditerAndName")
	public  void  setAuditerAndName(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	{
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper = new ObjectMapper();  
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		Integer taskId=Integer.parseInt(map.get("taskId"));
		Integer auditerId=Integer.parseInt(map.get("editerId"));
		String auditerName=map.get("editerName");
		int flag=ngccTaskService.updateAuditerId(auditerId, auditerName, taskId);
		if(flag>0)
		{
			response.getWriter().println("0000");
		}else {
			response.getWriter().println("0001");
		}
	}
	
	/**
	 * 获取审核任务分配网格数据
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value="/getAuditerTask")
	public void getAuditerTask(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		List<NgccGrid> grid=ngccGridService.getAll();
		List<Map<String, String>> result=new ArrayList<>();
		for(int i=0;i<grid.size();i++)
		{
			Map<String, String> teMap=new HashMap<>();
			NgccGrid ngccGrid=grid.get(i);
			NgccTask ngccTask=ngccTaskService.findByTaskId(ngccGrid.getUuid()).get(0);
			//获取只能符合条件的任务
			if(ngccTask.getEditStatus()==1)
			{
			teMap.put("uuid", ngccTask.getTaskId().toString());
			teMap.put("geom", ngccGrid.getValue());
			teMap.put("status",ngccTask.getEditStatus().toString());
			result.add(teMap);
			}
		}
		
		ObjectMapper mapper=new ObjectMapper();
		String data=mapper.writeValueAsString(result);
		response.getWriter().println(data);
	}
	
	/**
	 * 获取审核任务，通过任务ID获取审核数据
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@RequestMapping(value="/getFeatureByAuditTaskId")
	 public void getAuditTask(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	 {
		 String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			Integer taskId=Integer.parseInt(map.get("taskId"));
			List<NgccTagPolygon> ngccTagPolygons=ngccTagPolygonService.geNgccTagPolygonsByTaskId(taskId);
			List<NgccOrigPolygon> ngccOrigPolygons=ngccOrigPolygonService.findByTaskID(taskId);
			List<Map<String, String>> result=new ArrayList<>();
			for(int i=0;i<ngccTagPolygons.size();i++)
			{
				NgccTagPolygon ngccTagPolygon=ngccTagPolygons.get(i);
				Map<String, String> temp=new HashMap<>();
				temp.put("id", ngccTagPolygon.getId().toString());
				temp.put("fname", ngccTagPolygon.getfName());
				temp.put("ftype", ngccTagPolygon.getfType());
				temp.put("geom", ngccTagPolygon.getGeom().toString());
                result.add(temp);
			}
			for(int i=0;i<ngccOrigPolygons.size();i++){
				NgccOrigPolygon ngccOrigPolygon=ngccOrigPolygons.get(i);
				Map<String, String> temp=new HashMap<>();
				temp.put("id", ngccOrigPolygon.getId().toString());
				temp.put("fname", ngccOrigPolygon.getfName());
				temp.put("ftype", ngccOrigPolygon.getfType());
				temp.put("geom", ngccOrigPolygon.getGeom().toString());
                result.add(temp);
			}
			ObjectMapper mapper1=new ObjectMapper();
			String  data1=new String(mapper.writeValueAsString(result).getBytes("UTF-8"),"iso-8859-1");
			response.getWriter().println(data1);
	 }
	 /**
	  * 单个要素审核状态
	  * @param request
	  * @param response
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	  */
	 @RequestMapping(value="/updateStatus")
	 public void updateStatus(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	 {
		 // 根据要素ID即可更新，该要素的审核状态
		    String data =new String(request.getParameter("data"));
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			Integer id=Integer.parseInt(map.get("id"));
			Integer auditStatus=Integer.parseInt(map.get("auditStatus"));
			String reason=map.get("reason");
			NgccTagPolygon ngccTagPolygon=new NgccTagPolygon();
			ngccTagPolygon.setId(id);
			ngccTagPolygon.setAuditStatus(auditStatus);
			ngccTagPolygon.setReason(reason);
			int flg=ngccTagPolygonService.updateAuditStatus(ngccTagPolygon);
			if(flg>0)
			{
				response.getWriter().println("0000");
			}
			else {
				response.getWriter().println("0001");
			}
	 }
	 @RequestMapping(value="/getAllEditerUser")
	 public void getAllEditer(HttpServletRequest request ,HttpServletResponse response) throws IOException
	 {
		 List<TagUser> users=tagUserService.getAllTagUser();
		 ObjectMapper mapper1=new ObjectMapper();
			String data1=mapper1.writeValueAsString(users);
			response.getWriter().println(data1);
	 }
	 /**
	  * 
	  * @param request
	  * @param response
	 * @throws IOException 
	  */
	 @RequestMapping(value="/getAllAuditer")
	 public void getAllAuditer(HttpServletRequest request,HttpServletResponse response) throws IOException
	 {
		 List<AdminUser> lists=adminUserService.getAdminUserByAuthority(1);
		 ObjectMapper mapper1=new ObjectMapper();
			String data=mapper1.writeValueAsString(lists);
			response.getWriter().println(data);
	 }
	 /**
	  * 
	  * 根据要素id获取任务geom
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	  */
	 @RequestMapping(value="/getTaskGeomByID")
	 public void getTaskGeomByID(HttpServletRequest request ,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException{
		 String data=new String(request.getParameter("data").getBytes("iso-8859-1"),"UTF-8");
		 ObjectMapper mapper=new ObjectMapper();
		 @SuppressWarnings("unchecked")
		 Map<String,String> map=mapper.readValue(data,Map.class);
		 Integer tskid=Integer.parseInt(map.get("taskid"));
		 List<NgccTask> tasks=ngccTaskService.findByTaskId(tskid);
		 List<NgccGrid> tasksgrid=ngccGridService.getGridByid(tskid);
		 List<Map<String,String>> result=new ArrayList<>();
		 for(int i=0;i<tasksgrid.size();i++){
			 Map<String,String> tMap=new HashMap<>();
			 NgccGrid ngccgrid=tasksgrid.get(i);
			 tMap.put("taskId", ngccgrid.getUuid().toString());
			 tMap.put("geom",ngccgrid.getValue());
			 result.add(tMap);
		 }
		 			
		ObjectMapper mapper1=new ObjectMapper();
		String  data1=new String(mapper.writeValueAsString(result).getBytes("UTF-8"),"iso-8859-1");
		response.getWriter().println(data1);
	 }
	 /**
	  * 通过要素ID获取要素
	  */
	 @RequestMapping(value="/findByFeatureId")
	 public void findByFeatureId(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	 {
		 ObjectMapper mapper1=new ObjectMapper();
		 String data=new String(request.getParameter("data").getBytes("iso-8859-1"),"UTF-8");
		 ObjectMapper mapper=new ObjectMapper();
		 @SuppressWarnings("unchecked")
		 Map<String,String> map=mapper.readValue(data,Map.class);
		 Integer id=Integer.parseInt(map.get("id"));
		 NgccOrigPolygon ngccOrigPolygon=ngccOrigPolygonService.findById(id);
			Map<String, String> tempMap=new HashMap<>();
			tempMap.put("id", ngccOrigPolygon.getId().toString());
			tempMap.put("fname", ngccOrigPolygon.getfName());
			tempMap.put("ftype", ngccOrigPolygon.getfType());

			tempMap.put("geom", ngccOrigPolygon.getGeom().toString());
		 String  data1=new String(mapper.writeValueAsString(tempMap).getBytes("UTF-8"),"iso-8859-1");
		 response.getWriter().println(data1);
	 }
	 /**
	  * 获取审核未通过的任务
	  * @param response
	  * @param request
	  * @param session
	 * @throws IOException 
	  */
	 @RequestMapping(value="/findNotAuditStatus")
	 public void findNotAuditStatus(HttpServletResponse response,HttpServletRequest request,HttpSession session) throws IOException
	 {
		 String userName=session.getAttribute("user").toString();
			List<NgccTask> ngccTasks=ngccTaskService.findByEditerNameAndStatus(userName, 4);
			ObjectMapper mapper=new ObjectMapper();
			String  result=new String(mapper.writeValueAsString(ngccTasks).getBytes("UTF-8"),"iso-8859-1");
			logger.info("----------------获取审核未通过的任务列表------------------------------");
			response.getWriter().println(result);
	 }
	 
	 /**
	  * 通过编辑后要素ID获取要素
	  */
	 @RequestMapping(value="/findByTagFeatureId")
	 public void findByTagFeatureId(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	 {
		 ObjectMapper mapper1=new ObjectMapper();
		 String data=new String(request.getParameter("data").getBytes("iso-8859-1"),"UTF-8");
		 ObjectMapper mapper=new ObjectMapper();
		 @SuppressWarnings("unchecked")
		 Map<String,String> map=mapper.readValue(data,Map.class);
		 Integer id=Integer.parseInt(map.get("id"));
		 NgccTagPolygon ngccTagPolygon=ngccTagPolygonService.findById(id);
			Map<String, String> tempMap=new HashMap<>();
			tempMap.put("id", ngccTagPolygon.getId().toString());
			tempMap.put("fname", ngccTagPolygon.getfName());
			tempMap.put("ftype", ngccTagPolygon.getfType());
			tempMap.put("reason", ngccTagPolygon.getReason());
			tempMap.put("geom", ngccTagPolygon.getGeom().toString());
		 String  data1=new String(mapper1.writeValueAsString(tempMap).getBytes("UTF-8"),"iso-8859-1");
		 response.getWriter().println(data1);
	 }
	 /**
	  * 通过任务ID获取审核未通过的要素
	  * @param request
	  * @param response
	  * @throws JsonParseException
	  * @throws JsonMappingException
	  * @throws IOException
	  */
	 @RequestMapping(value="/getAllNotAuditFeature")
	 public void getAllNotAuditFeature(HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException
	 {
		 
		 String data=new String(request.getParameter("data").getBytes("iso-8859-1"),"UTF-8");
		 ObjectMapper mapper=new ObjectMapper();
		 @SuppressWarnings("unchecked")
		 Map<String,String> map=mapper.readValue(data,Map.class);
		 Integer taskId=Integer.parseInt(map.get("taskid"));
		 List<NgccTagPolygon>  ngccTagPolygons=ngccTagPolygonService.findByTaskIdAndStatus(taskId, 4);
		 
		 List<Map<String, String>> result=new ArrayList<>();
			int size=ngccTagPolygons.size();
			for(int i=0;i<size;i++)
			{
				NgccTagPolygon ngccTagPolygon=ngccTagPolygons.get(i);
				Map<String, String> temp=new HashMap<>();
				temp.put("id", ngccTagPolygon.getId().toString());
				temp.put("fname", ngccTagPolygon.getfName());
				temp.put("ftype", ngccTagPolygon.getfType());
				temp.put("geom", ngccTagPolygon.getGeom().toString());
				temp.put("reason", ngccTagPolygon.getReason());
             result.add(temp);
			}
			ObjectMapper mapper1=new ObjectMapper();
			String  data1=new String(mapper1.writeValueAsString(result).getBytes("UTF-8"),"iso-8859-1");
			response.getWriter().println(data1);
	 }
}

package com.csu.geo.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.csu.geo.entity.Log;
import com.csu.geo.service.LogService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LogController {
	
	@Autowired 
	private  LogService logService;
	private static final Logger logger=LoggerFactory.getLogger(LogController.class);
	/**
	 * 获取所有日志信息
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "/getLogs")
	public void  getAllLog(HttpServletRequest request ,HttpServletResponse response) throws IOException
	{
		ObjectMapper mapper=new ObjectMapper();
	    List<Log> resultList=logService.getAllLog();  
		String  result=new String(mapper.writeValueAsString(resultList).getBytes("UTF-8"),"iso-8859-1");
	    logger.info("-------------获取所有审核日志信息-----------------");
	    response.getWriter().println(result);
	}
   @RequestMapping(value="/getAdminUserCheckCount")
   public void getAdminUserCheckcount(HttpServletRequest request ,HttpServletResponse response,HttpSession session) throws IOException
   {
	   String adminUser=session.getAttribute("adminuser").toString();
	   Map<String, Integer> map=logService.getAdminCheckCount(adminUser);
	   ObjectMapper mapper=new ObjectMapper();
		String  result=new String(mapper.writeValueAsString(map).getBytes("UTF-8"),"iso-8859-1");
	   response.getWriter().println(result);
   }
}

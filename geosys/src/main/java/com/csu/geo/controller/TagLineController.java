package com.csu.geo.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.csu.geo.entity.TagLine;
import com.csu.geo.service.TagLineService;
import com.csu.geo.service.TagUserService;
import com.csu.geo.utils.WKTUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.io.ParseException;

@Controller
public class TagLineController {
	
	@Autowired
	private TagLineService tagLineService;
	@Autowired
	private TagUserService tagUserService;
	private static final Logger logger=LoggerFactory.getLogger(TagLineController.class);
	/**
	 * 修改线数据保存
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 * @throws ParseException
	 */
	@RequestMapping(value = "/saveLine")
	public void saveTagPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException
	{
	//String user=session.getAttribute("user").toString();
		if(session.getAttribute("user")==null)
		{
			response.sendRedirect("login");
		}
		else {
			String user=session.getAttribute("user").toString();
			String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			TagLine tagLine=new TagLine();
			tagLine.setFeatureID(Integer.valueOf(map.get("id")));
			tagLine.setFeatureName(map.get("fname"));
			tagLine.setFeatureType(map.get("ftype"));
			tagLine.setFdes(map.get("fdes"));
			LineString polygon=WKTUtil.createLineByWKT(map.get("geom"));
			tagLine.setgeom(polygon);
			tagLine.setStatus(3);
			tagLine.setTagUserID(tagUserService.findUserID(user));
			 SimpleDateFormat sd=new SimpleDateFormat("yyyyMMDDHHmmss");
			 String createDate=sd.format(new Date());
			 tagLine.setCreateDate(createDate);
			String result=tagLineService.saveTagLine(tagLine);
			logger.info("------------保存线数据修改成功--------------------");
			response.getWriter().println(result);
		}
	}
	
	/**
	 * 新线数据保存
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 * @throws ParseException
	 */
	@RequestMapping(value = "/newLine")
	public void newTagPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException
	{
	//String user=session.getAttribute("user").toString();
		if(session.getAttribute("user")==null)
		{
			response.sendRedirect("login");
		}
		else {
			String user=session.getAttribute("user").toString();
			String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
			//String data=request.getParameter("data");
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			TagLine tagLine=new TagLine ();
			
			tagLine.setFeatureName(map.get("fname"));
			tagLine.setFeatureType(map.get("ftype"));
			tagLine.setFdes(map.get("fdes"));
			LineString polygon=WKTUtil.createLineByWKT(map.get("geom"));
			tagLine.setgeom(polygon);
			tagLine.setStatus(3);
			tagLine.setFeatureID(0);
			tagLine.setTagUserID(tagUserService.findUserID(user));
			SimpleDateFormat sd=new SimpleDateFormat("yyyyMMDDHHmmss");
			String createDate=sd.format(new Date());
			tagLine.setCreateDate(createDate);
			String result=tagLineService.saveTagLine(tagLine);
			logger.info("--------------新建线数据保存成功---------------");
			response.getWriter().println(result);
		}
	}

}

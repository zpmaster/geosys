package com.csu.geo.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
/**
 * 
 * 标报数据审核
 * @author 
 *
 */
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.csu.geo.entity.AdminUser;
import com.csu.geo.entity.Log;
import com.csu.geo.entity.OrigLine;
import com.csu.geo.entity.OrigPoint;
import com.csu.geo.entity.OrigPolygon;
import com.csu.geo.entity.TagUser;
import com.csu.geo.service.AdminUserService;
import com.csu.geo.service.LogService;
import com.csu.geo.service.OrigLineService;
import com.csu.geo.service.OrigPointService;
import com.csu.geo.service.OrigPolygonService;
import com.csu.geo.service.TagCheckService;
import com.csu.geo.service.TagLineService;
import com.csu.geo.service.TagPointService;
import com.csu.geo.service.TagPolygonService;
import com.csu.geo.service.TagUserService;
import com.csu.geo.utils.WKTUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.io.ParseException;
@Controller
public class CheckTagDataController {
	private static final Logger logger=LoggerFactory.getLogger(CheckTagDataController.class);
	
	@Autowired
	private TagCheckService tagCheckService;
	@Autowired
	private OrigLineService origLineService;
	@Autowired
	private OrigPointService  origPointService;
	@Autowired
	private OrigPolygonService origPolygonService;
	@Autowired
	private LogService logService;
	@Autowired 
	private TagPointService tagPointService;
	@Autowired 
	private TagLineService tagLineService;
	@Autowired
	private TagPolygonService tagPolygonService;
	@Autowired
	private TagUserService tagUserService;
	@Autowired 
	private AdminUserService adminUserService;
	
	@RequestMapping(value="/tagchecklist")
	public ModelAndView tagCheckList()
	{
		ModelAndView modelAndView=new ModelAndView("tagchecklist");
		return modelAndView;
	}
	/**
	 * 获取未审核的数据
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(value="/getCheckData")
	public void getCheckData(HttpServletRequest request,HttpServletResponse response) throws IOException
	{
		Map<String, List<Map<String, String>>> resultMap=tagCheckService.getUnCheckTagData();
		ObjectMapper mapper=new ObjectMapper();
		String  result=new String(mapper.writeValueAsString(resultMap).getBytes("UTF-8"),"iso-8859-1");
		logger.info("-----------------获取所有未审核的数据------------");
		response.getWriter().println(result);
	}
	
	/**
	 * 更新标报审核审核数据
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 * @throws ParseException 
	 */
	@RequestMapping(value="/updateTagData")
	public void updateTagData(HttpSession session,HttpServletRequest request,HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException, ParseException
	{
		
		String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
		ObjectMapper mapper=new ObjectMapper();
		@SuppressWarnings("unchecked")
		Map<String, String> map=mapper.readValue(data, Map.class);
		Integer id=Integer.parseInt(map.get("id"));
		Integer fid=Integer.parseInt(map.get("fid"));
		String fname=map.get("fname");
		String ftype=map.get("ftype");
		String mark=map.get("mark");
		String fdes=map.get("fdes");
		String geom=map.get("geom");
		String failreason=map.get("failreason");
		String tagUserId=map.get("taguserid");
		String adminName=session.getAttribute("adminuser").toString();
		AdminUser adminUser=adminUserService.getAdminUser(adminName);
		TagUser tagUser=tagUserService.geTagUserById(Integer.parseInt(tagUserId));
		Integer status=Integer.parseInt(map.get("status"));
		DateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		  String time=format.format(new Date());
		  
		Log log=new Log();
		log.setAdminName(adminUser.getUserName());
		log.setAdminEmail(adminUser.getEmail());
		log.setTagUserName(tagUser.getUserName());
		log.setTagUserEmail(tagUser.getEmail());
		log.setReason(failreason);
		log.setCreateTime(time);
		log.setCheckStatus(status);
		log.setDes(fdes);
		log.setFname(fname);
		log.setType(mark);
		logService.saveLog(log);
		
		if(mark.equals("Point"))
		{
			if(fid==0)
			{
				
				OrigPoint origPoint=new OrigPoint();
				origPoint.setFeatureDes(fdes);
				origPoint.setFeatureName(fname);
				origPoint.setFeatureType(ftype);
				origPoint.setgeom(WKTUtil.createPointByWKT(geom));
				origPoint.setVersion(0);
				origPointService.saveOrigPoint(origPoint);
				tagPointService.updateTagPoint(id, status, failreason);
				 logger.info("-------审核数据操作成功，审核要素名称为："+fname);
				response.getWriter().println("0000");
			}else {
				if(status==1)
				{
				origPointService.updateOrigPoint(fname, ftype, fdes, geom, fid);
				tagPointService.updateTagPoint(id, status, failreason);
				}else {
					tagPointService.updateTagPoint(id, status, failreason);
				}
				 logger.info("-------审核数据操作成功，审核要素名称为："+fname);
				response.getWriter().println("0000");
			}
		}else if (mark.equals("Line")) {
			if(fid==0){
				OrigLine origLine=new OrigLine();
				origLine.setFeatureDes(fdes);
				origLine.setFeatureName(fname);
				origLine.setFeatureType(ftype);
				origLine.setgeom(WKTUtil.createLineByWKT(geom));
				origLine.setVersion(0);
				origLineService.saveOrigLine(origLine);
				tagLineService.updateTagLine(id, status, failreason);
				 logger.info("-------审核数据操作成功，审核要素名称为："+fname);
				response.getWriter().println("0000");
			}else {
				if(status==1)
				{
				tagLineService.updateTagLine(id, status, failreason);
				origLineService.updateOrigLine(fname, ftype, fdes, geom, fid);
				}else {
					origLineService.updateOrigLine(fname, ftype, fdes, geom, fid);
				}
				 logger.info("-------审核数据操作成功，审核要素名称为："+fname);
				response.getWriter().println("0000");
			}
			
		}else if (mark.equals("Polygon")) {
			if(fid==0)
			{
			  OrigPolygon origPolygon=new OrigPolygon();
			  origPolygon.setFeatureDes(fdes);
			  origPolygon.setFeatureName(fname);
			  origPolygon.setFeatureType(ftype);
			  origPolygon.setGeom(WKTUtil.createPolygon(geom));
			  origPolygon.setVersion(0);
			  origPolygonService.saveOrigPolygon(origPolygon);
			  tagPolygonService.updateTagPolygon(id, status, failreason);
				 logger.info("-------审核数据操作成功，审核要素名称为："+fname);
			  response.getWriter().println("0000");
				
			}else {
				if(status==1)
				{
				tagPolygonService.updateTagPolygon(id, status, failreason);
				origPolygonService.updateOrigPolygon(fname, ftype, fdes, geom, fid);
				}else {
					tagPolygonService.updateTagPolygon(id, status, failreason);
				}
				 logger.info("-------审核数据操作成功，审核要素名称为："+fname);
				response.getWriter().println("0000");
			}
		}
		
	}
}

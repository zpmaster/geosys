package com.csu.geo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Transactional
public class PersonController {
	
	@RequestMapping(value="/person",method={RequestMethod.GET,RequestMethod.POST})
	public ModelAndView person(HttpSession session){
		if(session.getAttribute("user").toString()!=null)
		{
		ModelAndView view =new ModelAndView("person");
		view.addObject("user",session.getAttribute("user"));
		return  view;
		}
		else {
			ModelAndView view =new ModelAndView("login");
			return view;
		}
	}
	
	@RequestMapping(value="/cperson",method={RequestMethod.GET,RequestMethod.POST})
	public ModelAndView cperson(HttpSession session){
		if(session.getAttribute("adminuser").toString()!=null)
		{
		ModelAndView view =new ModelAndView("cperson");
		view.addObject("adminuser",session.getAttribute("adminuser"));
		return  view;
		}
		else {
			ModelAndView view =new ModelAndView("login");
			return view;
		}
	}
}

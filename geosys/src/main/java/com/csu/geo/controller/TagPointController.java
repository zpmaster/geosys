package com.csu.geo.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.csu.geo.entity.TagPoint;
import com.csu.geo.service.TagPointService;
import com.csu.geo.service.TagUserService;
import com.csu.geo.utils.WKTUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.io.ParseException;

@Controller
public class TagPointController {
	@Autowired
	private TagPointService tagPointService;
	@Autowired
	private TagUserService tagUserService;
	private static final Logger logger=LoggerFactory.getLogger(TagPointController.class);
	/**
	 * 修改点数据保存
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 * @throws ParseException
	 */
	@RequestMapping(value = "/savePoint")
	public void saveTagPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException
	{
	//String user=session.getAttribute("user").toString();
		if(session.getAttribute("user")==null)
		{
			response.sendRedirect("login");
		}
		else {
			String user=session.getAttribute("user").toString();
			String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			TagPoint tagPoint=new TagPoint();
			tagPoint.setFeatureID(Integer.valueOf(map.get("id")));
			tagPoint.setFeatureName(map.get("fname"));
			tagPoint.setFeatureType(map.get("ftype"));
			tagPoint.setFdes(map.get("fdes"));
			Point polygon=WKTUtil.createPointByWKT(map.get("geom"));
			tagPoint.setgeom(polygon);
			tagPoint.setStatus(3);
			tagPoint.setTagUserID(tagUserService.findUserID(user));
			 SimpleDateFormat sd=new SimpleDateFormat("yyyyMMDDHHmmss");
			 String createDate=sd.format(new Date());
			 tagPoint.setCreateDate(createDate);
			String result=tagPointService.saveTagPoint(tagPoint);
			logger.info("--------------修改点数据保存成功--------------");
			response.getWriter().println(result);
		}
	}
	
	/**
	 * 新建点数据保存成功
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 * @throws ParseException
	 */
	@RequestMapping(value = "/newPoint")
	public void newTagPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException, ParseException
	{
	//String user=session.getAttribute("user").toString();
		if(session.getAttribute("user")==null)
		{
			response.sendRedirect("login");
		}
		else {
			String user=session.getAttribute("user").toString();
			String data =new String(request.getParameter("data").getBytes("iso-8859-1"), "utf-8");
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			TagPoint tagPoint=new TagPoint ();
			
			tagPoint.setFeatureName(map.get("fname"));
			tagPoint.setFeatureType(map.get("ftype"));
			tagPoint.setFdes(map.get("fdes"));
			Point polygon=WKTUtil.createPointByWKT(map.get("geom"));
			tagPoint.setgeom(polygon);
			tagPoint.setStatus(3);
			tagPoint.setFeatureID(0);
			tagPoint.setTagUserID(tagUserService.findUserID(user));
			 SimpleDateFormat sd=new SimpleDateFormat("yyyyMMDDHHmmss");
			 String createDate=sd.format(new Date());
			 tagPoint.setCreateDate(createDate);
			String result=tagPointService.saveTagPoint(tagPoint);
			logger.info("--------------新建点数据保存成功---------------");
			response.getWriter().println(result);
		}
	}

}

package com.csu.geo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.csu.geo.entity.OrigPolygon;
import com.csu.geo.service.OrigPolygonService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class OrigPolygonController {
	
	@Autowired
	private OrigPolygonService origPolygonService;
	
	private static final Logger logger=LoggerFactory.getLogger(OrigPolygonController.class);
	/**
	 * 提供原始面数据
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 */
	@RequestMapping(value="/getOrigPolygon")
	public void getOrigPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException
	{
		List<OrigPolygon> origPolygonList=origPolygonService.findAll();
		List<Map<String, String>> result=new ArrayList<Map<String,String>>();
		int size=origPolygonList.size();
		for(int i=0;i<size;i++)
		{
			OrigPolygon origPolygon=origPolygonList.get(i);
			Map<String, String> tempMap=new HashMap<String, String>();
			tempMap.put("id", origPolygon.getId().toString());
			tempMap.put("fname", origPolygon.getFeatureName());
			tempMap.put("ftype", origPolygon.getFeatureType());
			tempMap.put("fdes", origPolygon.getFeatureDes());
			tempMap.put("geom", origPolygon.getGeom().toString());
			result.add(tempMap);
			
		}
		ObjectMapper mapper = new ObjectMapper(); 
		String  data=new String(mapper.writeValueAsString(result).getBytes("UTF-8"),"iso-8859-1");
		logger.info("------------获取原始面数据成功-------------");
		response.getWriter().println(data);
		
	}

}

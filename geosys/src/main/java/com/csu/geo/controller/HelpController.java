package com.csu.geo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *  帮助菜单控制器
 * @author zhangpan
 *
 */
@Controller
public class HelpController {

	private static final Logger logger=LoggerFactory.getLogger(HelpController.class);
	
	@RequestMapping(value="/guide")
	public String helpguide()
	{
		logger.info("");
		return "helpguide";
		
	}
	@RequestMapping(value="/safe")
	public String helpsafe()
	{
		return "helpsafe";
	}
	@RequestMapping(value="/contact")
	public String helpcontact()
	{
		return "helpcontact";
	}
	@RequestMapping(value="/software")
	public String helpsys()
	{
		return "software";
	}
}

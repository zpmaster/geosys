package com.csu.geo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.csu.geo.entity.OrigLine;
import com.csu.geo.service.OrigLineService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class OrigLineController {
	@Autowired
	private OrigLineService origLineService;
	private static final Logger logger=LoggerFactory.getLogger(OrigLineController.class);
	/**
	 * 提供原始线数据--操作
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 */
	@RequestMapping(value="/getOrigLine")
	public void getOrigPolygon(HttpServletRequest request,HttpServletResponse response,HttpSession session) throws IOException
	{
		List<OrigLine> origLineList=origLineService.getOrigLineList();
		List<Map<String, String>> result=new ArrayList<Map<String,String>>();
		int size=origLineList.size();
		for(int i=0;i<size;i++)
		{
			OrigLine origLine=origLineList.get(i);
			Map<String, String> tempMap=new HashMap<String, String>();
			tempMap.put("id",  origLine.getId().toString());
			tempMap.put("fname",  origLine.getFeatureName());
			tempMap.put("ftype",  origLine.getFeatureType());
			tempMap.put("fdes",  origLine.getFeatureDes());
			tempMap.put("geom",  origLine.getgeom().toString());
			result.add(tempMap);
			
		}
		ObjectMapper mapper = new ObjectMapper(); 
		String  data=new String(mapper.writeValueAsString(result).getBytes("UTF-8"),"iso-8859-1");
		logger.info("--------------获取原始线数据成功-------------");
		response.getWriter().println(data);
		
	}

}

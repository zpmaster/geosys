package com.csu.geo.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.csu.geo.service.HistoryTagService;
import com.csu.geo.service.TagUserService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *  编辑任务控制器
 * @author zhangpan
 *
 */
@Controller
public class TagHistoryController {

	private static final Logger logger=LoggerFactory.getLogger(TagHistoryController.class);
	
	@Autowired
	private TagUserService tagUserService;
	@Autowired
	private HistoryTagService historyTagService;
	
	@RequestMapping(value="/taghistory")
	public ModelAndView  taghistory(HttpSession session)
	{
		
		if(session.getAttribute("user")!=null)
		{
		ModelAndView view =new ModelAndView("taghistory");
		view.addObject("user",session.getAttribute("user"));
		logger.info("------------历史页面显示成功-------------");
		return  view;
		}
		else {
			ModelAndView view =new ModelAndView("login");
			logger.info("----------需要用户先登陆--------------");
			return view;
		}
	}
	/***
	 * 获取个人编辑历史数据
	 * @param request
	 * @param response
	 * @param session
	 * @throws IOException
	 */
	@RequestMapping(value="/getmyhistorytag")
	public void getTagHistory(HttpServletRequest request ,HttpServletResponse response,HttpSession session) throws IOException
	{
		if(session.getAttribute("user")==null)
		{
			response.sendRedirect("login");
		}
		else {
			String user=session.getAttribute("user").toString();
			String data=request.getParameter("data");
			ObjectMapper mapper = new ObjectMapper();  
			@SuppressWarnings("unchecked")
			Map<String, String> map=mapper.readValue(data, Map.class);
			int userID=tagUserService.findUserID(user);
			String ftype=map.get("ftype");
			Integer status=Integer.parseInt(map.get("status"));
			Map<String, List<Map<String, String>>> resultMap=historyTagService.getTagHistory(userID, ftype, status);
			ObjectMapper mapper1 = new ObjectMapper(); 
			String result=new String(mapper1.writeValueAsString(resultMap).getBytes("UTF-8"),"iso-8859-1");
			logger.info("----------获取个人历史编辑数据成功------------");
			response.getWriter().println(result);
		}
	}
}

package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.OrigPolygon;



public interface OrigPolygonDao  extends CrudRepository<OrigPolygon, Long>{

	@Override
	long count();

	@Override
	void delete(Iterable<? extends OrigPolygon> arg0);

	@Override
	void delete(Long arg0);

	@Override
	void delete(OrigPolygon arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<OrigPolygon> findAll();

	@Override
	Iterable<OrigPolygon> findAll(Iterable<Long> arg0);

	@Override
	OrigPolygon findOne(Long arg0);

	@Override
	<S extends OrigPolygon> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends OrigPolygon> S save(S arg0);
	
	@Query(value="SELECT * FROM \"orig_polygon\" " ,nativeQuery=true)
	List<OrigPolygon> getOrigPolygonList();

}

package com.csu.geo.repository;

import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.NgccTask;
import java.lang.String;
import java.util.List;
import java.lang.Integer;

public interface NgccTaskDao  extends CrudRepository<NgccTask, Long>{

	
	List<NgccTask> findByEditerName(String editername);
	
	List<NgccTask> findByTaskId(Integer taskid);
	
	List<NgccTask> findByEditStatus(Integer editstatus);
	List<NgccTask> findByAuditName(String auditname);
	
	List<NgccTask> findByEditerNameAndEditStatus(String editerName,Integer editStatus);
	
}

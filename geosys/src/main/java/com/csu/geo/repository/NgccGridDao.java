package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.NgccGrid;

public interface NgccGridDao extends CrudRepository<NgccGrid, Long>{
	
    @Query(value="SELECT * FROM  \"ngcc_grid\"",nativeQuery=true)
	public List<NgccGrid> getAllGrid();
	
    @Query(value="DELETE * FROM \"ngcc_grid\"",nativeQuery=true)
    public void deleteAll();
    
    public List<NgccGrid> getGridByUuid(int uuid);
}

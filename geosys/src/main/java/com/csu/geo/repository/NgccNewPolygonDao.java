package com.csu.geo.repository;

import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.NgccNewPolygon;

public interface NgccNewPolygonDao  extends CrudRepository<NgccNewPolygon, Long>{
   
}

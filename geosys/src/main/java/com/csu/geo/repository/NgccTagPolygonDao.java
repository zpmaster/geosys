package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.NgccTagPolygon;

public interface NgccTagPolygonDao extends CrudRepository<NgccTagPolygon, Long> {
	
	@Query(value="SELECT  * FROM ngcc_tag_polygon  WHERE taskid=?",nativeQuery=true)
	List<NgccTagPolygon> getByTaskId(Integer taskId);
	
	NgccTagPolygon findById(Integer id);
	
	List<NgccTagPolygon> findByTaskIdAndAuditStatus(Integer taskId,Integer status);
    
}

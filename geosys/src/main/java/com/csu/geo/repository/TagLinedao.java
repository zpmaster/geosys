package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.TagLine;



public interface TagLinedao  extends CrudRepository<TagLine, Long>{

	@Override
	long count();

	@Override
	void delete(Iterable<? extends TagLine> arg0);

	@Override
	void delete(Long arg0);

	@Override
	void delete(TagLine arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<TagLine> findAll();

	@Override
	Iterable<TagLine> findAll(Iterable<Long> arg0);

	@Override
	TagLine findOne(Long arg0);

	@Override
	<S extends TagLine> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends TagLine> S save(S arg0);
	
	 @Query(value="SELECT * FROM \"tag_line\" WHERE taguserid=?",nativeQuery=true)
	 public List<TagLine> findTagLinenByUserID(Integer tagUserID);
	 @Query(value="SELECT * FROM \"tag_line\" WHERE taguserid=? and status=?",nativeQuery=true)
	 public List<TagLine> findTagLineByStatus(Integer id,Integer status);
	 @Query(value="SELECT * FROM \"tag_line\" WHERE status=3",nativeQuery=true)
	 public List<TagLine> findTagLineByUnCheck();
}

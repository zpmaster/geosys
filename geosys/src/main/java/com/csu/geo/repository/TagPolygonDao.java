package com.csu.geo.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.TagPolygon;


public interface TagPolygonDao extends CrudRepository<TagPolygon, Long> {

	@Override
	long count();

	@Override
	void delete(Iterable<? extends TagPolygon> arg0);

	@Override
	void delete(Long arg0);

	@Override
	void delete(TagPolygon arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<TagPolygon> findAll();

	@Override
	Iterable<TagPolygon> findAll(Iterable<Long> arg0);

	@Override
	TagPolygon findOne(Long arg0);

	@Override
	<S extends TagPolygon> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends TagPolygon> S save(S arg0);
	 @Query(value="SELECT * FROM \"tag_polygon\" WHERE taguserid=?",nativeQuery=true)
	 public List<TagPolygon> findTagPolygonByUserID(Integer tagUserID);
	 @Query(value="SELECT * FROM \"tag_polygon\" WHERE taguserid=? and status=?",nativeQuery=true)
	 public List<TagPolygon> findTagPolygonByStatus(Integer id,Integer status);
	 @Query(value="SELECT * FROM \"tag_polygon\" WHERE status=3",nativeQuery=true)
	 public List<TagPolygon> findTagPolygonByUnCheck();
	

}

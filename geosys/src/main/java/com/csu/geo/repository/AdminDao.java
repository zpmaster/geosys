package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.AdminUser;


public interface AdminDao extends CrudRepository<AdminUser, Long>{

	@Override
	long count();

	@Override
	void delete(AdminUser arg0);

	@Override
	void delete(Iterable<? extends AdminUser> arg0);

	@Override
	void delete(Long arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<AdminUser> findAll();

	@Override
	Iterable<AdminUser> findAll(Iterable<Long> arg0);

	@Override
	AdminUser findOne(Long arg0);

	@Override
	<S extends AdminUser> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends AdminUser> S save(S arg0);
    @Query(value="SELECT * FROM \"admin_user\" WHERE username=? and password=?",nativeQuery=true)
	public AdminUser findByUserNameAndPassword(String name,String password);
    @Query(value="SELECT * FROM \"admin_user\"",nativeQuery=true)
    public List<AdminUser> getAllAdminUser();
    @Query(value="SELECT * FROM \"admin_user\" WHERE username=? ",nativeQuery=true)
    public List<AdminUser> getAdminUserByName(String name);
    @Query(value="SELECT * FROM \"admin_user\" WHERE authority=? ",nativeQuery=true)
    public List<AdminUser> getAdminUserByAuthority(Integer authority);
    @Query(value="SELECT * FROM \"admin_user\" WHERE authority=? and username=? ",nativeQuery=true)
    public List<AdminUser> getAdminUserByAuthorityAndUserName(Integer authority,String username);
    @Query(value="SELECT * FROM \"admin_user\" WHERE username=? ",nativeQuery=true)
    public AdminUser getAdminUser(String name);
}

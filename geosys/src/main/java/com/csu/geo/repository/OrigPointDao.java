package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.OrigPoint;


public interface OrigPointDao  extends CrudRepository<OrigPoint, Long>{

	@Override
	long count();

	@Override
	void delete(Iterable<? extends OrigPoint> arg0);

	@Override
	void delete(Long arg0);

	@Override
	void delete(OrigPoint arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<OrigPoint> findAll();

	@Override
	Iterable<OrigPoint> findAll(Iterable<Long> arg0);

	@Override
	OrigPoint findOne(Long arg0);

	@Override
	<S extends OrigPoint> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends OrigPoint> S save(S arg0);
	
	@Query(value="SELECT * FROM \"orig_point\" " ,nativeQuery=true)
	List<OrigPoint> getOrigPointnList();
     
	

}

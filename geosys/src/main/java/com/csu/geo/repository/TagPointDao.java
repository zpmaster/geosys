package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.TagPoint;


public interface TagPointDao extends CrudRepository<TagPoint, Long>{

	@Override
	long count();

	@Override
	void delete(Iterable<? extends TagPoint> arg0);

	@Override
	void delete(Long arg0);

	@Override
	void delete(TagPoint arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<TagPoint> findAll();

	@Override
	Iterable<TagPoint> findAll(Iterable<Long> arg0);

	@Override
	TagPoint findOne(Long arg0);

	@Override
	<S extends TagPoint> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends TagPoint> S save(S arg0);
	
	 @Query(value="SELECT * FROM \"tag_point\" WHERE taguserid=?",nativeQuery=true)
	 public List<TagPoint> findTagPointByUserID(Integer tagUserID);
	 @Query(value="SELECT * FROM \"tag_point\" WHERE taguserid=? and status=?",nativeQuery=true)
	 public List<TagPoint> findTagPointByStatus(Integer id,Integer status);
	 @Query(value="SELECT * FROM \"tag_point\" WHERE status=3",nativeQuery=true)
	 public List<TagPoint> findTagPointByUnCheck();

}

package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.csu.geo.entity.TagUser;



public interface TagUserDao extends CrudRepository<TagUser, Long> {

	@Override
	long count();

	@Override
	void delete(Iterable<? extends TagUser> arg0);

	@Override
	void delete(Long arg0);

	@Override
	void delete(TagUser arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<TagUser> findAll();

	@Override
	Iterable<TagUser> findAll(Iterable<Long> arg0);

	@Override
	TagUser findOne(Long arg0);

	@Override
	<S extends TagUser> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends TagUser> S save(S arg0);
	
	 @Query(value="SELECT * FROM \"tag_user\" WHERE username=? and password=?" ,nativeQuery=true)
		public TagUser findByNameAndPass(String name,String pass);
	 @Query(value="SELECT * FROM \"tag_user\" WHERE username=?" ,nativeQuery=true)
	 public TagUser findUserInfoByUserName(String username);
	 
	 @Query(value="UPDATE \"tag_user\" SET email=? ,tel=?,password=?  WHERE username=?",nativeQuery=true)
	 public Integer updateUserInfo(String email,String tel,String password,String username);
	 @Query(value="SELECT id FROM \"tag_user\" WHERE username=?",nativeQuery=true)
	 public Integer findUserID(String name);
	 @Query(value="SELECT * FROM \"tag_user\"",nativeQuery=true)
	 public List<TagUser> getAllTagUser();
	 @Query(value="SELECT * FROM \"tag_user\" WHERE username=?",nativeQuery=true)
	 public List<TagUser> geTagUsersByTagUserName(String tagUserName);
	 @Query(value="SELECT * FROM \"tag_user\" WHERE activestatus=?",nativeQuery=true)
	 public List<TagUser> getTagUsersByActiveStatus(Integer activeStatus);
	 @Query(value="SELECT * FROM \"tag_user\" WHERE activestatus=? and username=?",nativeQuery=true)
	 public List<TagUser> getTagUsersByActiveStatusAndName(Integer activeStatus,String userName);
	 @Query(value="SELECT * FROM \"tag_user\" WHERE id=?",nativeQuery=true)
	 public TagUser geTagUserById(Integer id);
}

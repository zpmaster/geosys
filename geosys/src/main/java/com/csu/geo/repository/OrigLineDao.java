package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.OrigLine;

public interface OrigLineDao extends CrudRepository<OrigLine, Long>{

	@Override
	long count();

	@Override
	void delete(Iterable<? extends OrigLine> arg0);

	@Override
	void delete(Long arg0);

	@Override
	void delete(OrigLine arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<OrigLine> findAll();

	@Override
	Iterable<OrigLine> findAll(Iterable<Long> arg0);

	@Override
	OrigLine findOne(Long arg0);

	@Override
	<S extends OrigLine> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends OrigLine> S save(S arg0);
	@Query(value="SELECT * FROM \"orig_line\"" ,nativeQuery=true)
	List<OrigLine> getOrigLineList();

}

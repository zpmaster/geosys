package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.Log;


public interface LogDao extends CrudRepository<Log, Long> {

	@Override
	long count();

	@Override
	void delete(Iterable<? extends Log> arg0);

	@Override
	void delete(Log arg0);

	@Override
	void delete(Long arg0);

	@Override
	void deleteAll();

	@Override
	boolean exists(Long arg0);

	@Override
	Iterable<Log> findAll();

	@Override
	Iterable<Log> findAll(Iterable<Long> arg0);

	@Override
	Log findOne(Long arg0);

	@Override
	<S extends Log> Iterable<S> save(Iterable<S> arg0);

	@Override
	<S extends Log> S save(S arg0);
	
	@Query(value="SELECT * FROM \"log\"",nativeQuery=true)
	public List<Log> getAllLog();

}

package com.csu.geo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.csu.geo.entity.NgccOrigPolygon;

public interface NgccOrigPolygonDao  extends CrudRepository<NgccOrigPolygon, Long>{

	@Query(value="select * from \"ngcc_orig_polygon\"",nativeQuery=true)
	public List<NgccOrigPolygon> findAll();
	
	/**
	 * 根据原始要素ID获取要素
	 * @return
	 */
	public NgccOrigPolygon findById(Integer id);
}

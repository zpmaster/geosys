 var layers = [     
        new ol.layer.Tile({
             	  source: new ol.source.BingMaps({
             	    imagerySet: 'Aerial',
             	    key: 'AkGbxXx6tDWf1swIhPJyoAVp06H0s0gDTYslNWWHZ6RoPqMpB9ld5FY1WutX8UoF'
             	  })
             	}),
        new ol.layer.Tile({
            source: new ol.source.TileWMS({
              url: 'http://localhost:8080/geoserver/changsha/wms',
              params: {'LAYERS': 'changsha:Area'},
              serverType: 'geoserver'
            })
          }),
          new ol.layer.Tile({
              source: new ol.source.TileWMS({
                url: 'http://localhost:8080/geoserver/changsha/wms',
                params: {'LAYERS': 'changsha:allpopint'},
                serverType: 'geoserver'
              })
            })
          
      ];
      var map = new ol.Map({
        layers: layers,
        target: 'map',
        view: new ol.View({
          center: [-7916041.528716288, 5228379.045749711],
          zoom: 19
        })
      });
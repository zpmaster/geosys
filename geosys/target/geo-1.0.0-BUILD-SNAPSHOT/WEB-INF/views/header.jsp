<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%
    String path=request.getContextPath();
    String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    String username="登陆";
    if(request.getSession().getAttribute("username")!=null)
    {
    	username=request.getSession().getAttribute("username").toString();
    }
 %>
<nav class="navbar navbar-default" role="navigation" >
   <div class="navbar-header">
      <a class="navbar-brand" href="#"><spring:message code="sysname"></spring:message></a>
   </div>
  <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-left">
          <li><a href="#home" class="smoothScroll"><spring:message code="nav.home"></spring:message> </a></li>
          <li> <a href="#about" class="smoothScroll"><spring:message code="nav.tag"></spring:message> </a></li>
           <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <spring:message code="nav.gps"></spring:message>  
               <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
               <li><a href="gpsup"><spring:message code="gps.up"></spring:message> </a></li>
               <li><a href="#"><spring:message code="gps.list"></spring:message> </a></li>
            </ul>
         </li>
          <li> <a href="#services" class="smoothScroll"><spring:message code="nav.task"></spring:message> </a></li>
          <li> <a href="qnaire" class="smoothScroll"><spring:message code="nav.questionnaire"></spring:message> </a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
               <spring:message code="nav.help"></spring:message>  
               <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
               <li><a href="#"><spring:message code="help.guide"></spring:message> </a></li>
               <li><a href="#"><spring:message code="help.safe"></spring:message> </a></li>
               <li><a href="contact"><spring:message code="help.contact"></spring:message> </a></li>
               <li><a href="#"><spring:message code="help.sys"></spring:message> </a></li>
            </ul>
         </li>
        </ul>
        <form class="navbar-form navbar-left" role="search">
         <div class="form-group">
            <input type="text" class="form-control" placeholder="Search">
         </div>
         <button type="submit" class="btn btn-default"><spring:message code="search"></spring:message> </button>
      </form>    
       <ul class="nav navbar-nav navbar-right">
         <li><a href="login" class="smoothScroll"><%=username %></a></a></li>
        </ul>
      </div>
      
       
</nav>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<%=path%>/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="<%=path%>/resources/bootstrap/css/bootstrap.css" rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title>建议</title>
</head>
<body>
	<jsp:include page="../header.jsp"></jsp:include>
	<form class="well span8">
		<table>
			<tr>
				<td>
			<tr>
				<td><label>First Name</label></td>
			</tr>
			<tr>
				<td><input type="text" class="span3"
					placeholder="Your First Name"></td>
			</tr>
			<tr>
				<td><label>Last Name</label></td>
			</tr>
			<tr>
				<td><input type="text" class="span3"
					placeholder="Your Last Name"></td>
			</tr>
			<tr>
				<td><label>Email Address</label></td>
			</tr>
			<tr>
				<td><input type="text" class="span3"
					placeholder="Your email address"></td>
			</tr>
			<tr>
				<td><label>Subject </label></td>
			</tr>
			<tr>
				<td><select id="subject" name="subject" class="span3">
						<option value="na" selected="">Choose One:</option>
						<option value="service">General Customer Service</option>
						<option value="suggestions">Suggestions</option>
						<option value="product">Product Support</option>
				</select></td>
			</tr>
			</td>
			<td>
				<tr>
				<td>
					<label>Message</label></td>
					</tr>
					<tr>
				<td>
					<textarea name="message" id="message" class="input-xlarge span5"
						rows="10"></textarea></td>
					</tr>
				
	<tr>
				<td>
				<button type="submit" class="btn btn-primary pull-right">Send</button></td>
					</tr>


			</td>
			</tr>
		</table>
	</form>
</body>
</html>
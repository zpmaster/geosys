<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%
    String path=request.getContextPath();
    String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
 %>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="<%=path%>/resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript"
	src="<%=path%>/resources/bootstrap/js/bootstrap.min.js"></script>
<link href="<%=path%>/resources/bootstrap/css/bootstrap.css"
	rel="stylesheet">

<meta name="viewport" content="width=device-width,initial-scale=1.0">
<title><spring:message code="register"></spring:message></title>
</head>
<body>
<jsp:include page="../views/header.jsp"></jsp:include>
 <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>欢迎注册，谢谢分享</strong></h1>
                            <div class="description">
                            	<p>
	                            	如果您已有本网站账号，请点击 <a href=""><strong>登陆</strong></a>, 加入我们的Family!
                            	</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Register to our site</h3>
                            		<p>请输入您的注册信息：</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form:form role="form"  modelAttribute="userModel" action="save" method="post" class="login-form">
			                    <form:errors path="*"></form:errors>
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>			                    		
			                        	<input  type="text"  name="username"  placeholder="Username..."class="form-username form-control" id="form-username" >
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Emial</label>
			                        	<input type="text" name="email" placeholder="Emial..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Tel</label>
			                        	<input type="text" name="tel" placeholder="Tel..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                    		<label class="sr-only" for="form-username">Password</label>
			                        	<input type="text" name="password" placeholder="Password..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Confirm Password</label>
			                        	<input type="password" name="cpassword" placeholder="Confirm Password..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn">注册</button>
			                    </form:form>
		                    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                        	<h3>...or login with:</h3>
                        	<div class="social-login-buttons">
	                        	<a class="btn btn-link-1 btn-link-1-facebook" href="#">
	                        		<i class="fa fa-facebook"></i> QQ
	                        	</a>
	                        	<a class="btn btn-link-1 btn-link-1-twitter" href="#">
	                        		<i class="fa fa-twitter"></i> WeChat
	                        	</a>
	                        	<a class="btn btn-link-1 btn-link-1-google-plus" href="#">
	                        		<i class="fa fa-google-plus"></i> WeiBo
	                        	</a>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
</body>
</html>